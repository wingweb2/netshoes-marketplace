<?php
use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\producttemplate as producttemplate;

if (!defined('_PS_VERSION_'))
	exit;

require_once dirname(dirname(dirname(__FILE__))).'/ApiMktpNetshoesV1.php';
require_once dirname(dirname(dirname(__FILE__))).'/includes/functions.php';

/**
 * Default Class
 */
class NetshoesGroupDefaultController extends ModuleAdminController {
	
	
	/** @var array Number of results in list per page (used in select field) */
    protected $_pagination = array(10, 20, 50);
    /** @var int Default number of results in list per page */
    protected $_default_pagination = 10;
	
	public $controllerName = null;
	public $prefix = null;
	public $controllerPrefix = null;
	
	function __construct() {
		$this->bootstrap = true;
		$this->moduleName = 'netshoesgroup';
		$this->prefix = $this->moduleName.'_';
		$this->controllerPrefix = $this->moduleName.'_'.($this->controllerName != null ? $this->controllerName.'_':null);
		$this->lang = true;
		$this->explicitSelect = true;

		parent::__construct();

	
		$this->netshoesGroup = new client\ApiClient((Configuration::get($this->prefix.'version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));

		if( Configuration::get($this->prefix.'client_id') != '' && Configuration::get($this->prefix.'access_token') != '' ) {
			client\Configuration::$apiKey['client_id'] = Configuration::get($this->prefix.'client_id');
			client\Configuration::$apiKey['access_token'] = Configuration::get($this->prefix.'access_token');
			client\Configuration::$apiClient = $this->netshoesGroup;
			client\Configuration::$debug = false;
			if($this->context->cookie->{$this->prefix.'error'} || $this->context->cookie->{$this->controllerPrefix.'error'}){
				$error = array_merge((array)json_decode($this->context->cookie->{$this->prefix.'error'},true),(array)json_decode($this->context->cookie->{$this->controllerPrefix.'error'},true));
				$this->context->cookie->{$this->prefix.'error'} = false;
				foreach ($error as $row) {
					$this->errors[] = $row;
				}
				$this->context->cookie->{$this->moduleName.'error'} = false;
			}
			if($this->context->cookie->{$this->prefix.'success'} || $this->context->cookie->{$this->controllerPrefix.'success'}){
				$success = array_merge((array)json_decode($this->context->cookie->{$this->prefix.'success'},true),(array)json_decode($this->context->cookie->{$this->controllerPrefix.'success'},true));
				$this->context->cookie->{$this->prefix.'success'} = false;
				foreach ($success as $row) {
					$this->confirmations[] = $row;
				}
				$this->context->cookie->{$this->moduleName.'success'} = false;
			}
		} else {
			$this->errors[] = 'Os Campos ID do cliente e/ou Token de acesso estão em branco';
		}
	}

	public function initPageHeaderToolbar()
    {
        $this->page_header_toolbar_btn['orders'] = array(
            'href' => 'index.php?controller=NetshoesGroupOrders&token='.Tools::getAdminTokenLite('NetshoesGroupOrders'),
            'desc' => $this->l('Pedidos'),
            'icon' => 'process-icon-cart'
        );
        $this->page_header_toolbar_btn['skus'] = array(
            'href' => 'index.php?controller=NetshoesGroupSKU&token='.Tools::getAdminTokenLite('NetshoesGroupSKU'),
            'desc' => $this->l('Anúncios'),
            'icon' => 'process-icon-archive icon-archive '
        );
        $this->page_header_toolbar_btn['products'] = array(
            'href' => 'index.php?controller=NetshoesGroupProducts&token='.Tools::getAdminTokenLite('NetshoesGroupProducts'),
            'desc' => $this->l('Produtos'),
            'icon' => 'process-icon-book icon-book'
        );
        $this->page_header_toolbar_btn['config'] = array(
            'href' => 'index.php?controller=AdminModules&token='.Tools::getAdminTokenLite('AdminModules').'&configure=netshoesgroup&module_name=netshoesgroup',
            'desc' => $this->l('Configuração'),
            'icon' => 'process-icon-cogs'
        );
		// $this->content .= debug($this->page_header_toolbar_btn);
        parent::initPageHeaderToolbar();
    }
}

