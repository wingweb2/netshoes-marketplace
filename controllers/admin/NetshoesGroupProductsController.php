<?php
use ApiMktpNetshoesV1\product as productapi;
use ApiMktpNetshoesV1\producttemplate as producttemplate;
use ApiMktpNetshoesV1\sku as sku;
require_once dirname(__FILE__).'/NetshoesGroupDefaultController.php';
require_once dirname(dirname(dirname(__FILE__))).'/classes/netshoesgroupOrder.php';
require_once dirname(dirname(dirname(__FILE__))).'/classes/netshoesgroupSku.php';

class NetshoesGroupProductsController extends NetshoesGroupDefaultController {

	public function __construct()
	{
		$this->controllerName = 'netshoesgroupProducts';
		$this->table = 'product';
		$this->className = 'Product';

		parent::__construct();
	}

	public function initProcess()
	{
	    $this->action = (Tools::getValue('action'))?Tools::getValue('action'):'list';
	    switch ($this->action) {
			case 'list':
				$this->displayList();
				break;
			case 'exportProduct':
				$this->exportProduct();
				break;
            case 'addVariation':
                $this->addVariation();
                break;
			default:
				$this->displayList();
				break;
		}
	    parent::initProcess();
	}


	public function displayList() 
	{
		// Will generate a list of products with id_netshoes, that is, associated products
		$success = Tools::getValue('success');
		$warning = Tools::getValue('warning');
		if($success && !is_null($success)){
			$this->confirmations[] = urldecode($success);
		} else if ($warning && !is_null($warning)) {
			$this->warnings[] = urldecode($warning);
		}

    	$this->table = 'product';

		$this->_join .= '
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = a.`id_product`)
		LEFT JOIN `'._DB_PREFIX_.'stock_available` sav ON (sav.`id_product` = a.`id_product` AND sav.`id_product_attribute` = 0
		'.StockAvailable::addSqlShopRestriction(null, null, 'sav').') ';

		$alias = 'sa';
		$alias_image = 'image_shop';

		$id_shop = Shop::isFeatureActive() && Shop::getContext() == Shop::CONTEXT_SHOP? (int)$this->context->shop->id : 'a.id_shop_default';
		$this->_join .= ' JOIN `'._DB_PREFIX_.'product_shop` sa ON (a.`id_product` = sa.`id_product` AND sa.id_shop = '.$id_shop.')
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON ('.$alias.'.`id_category_default` = cl.`id_category` AND b.`id_lang` = cl.`id_lang` AND cl.id_shop = '.$id_shop.')
				LEFT JOIN `'._DB_PREFIX_.'shop` shop ON (shop.id_shop = '.$id_shop.')
				LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop ON (image_shop.`id_image` = i.`id_image` AND image_shop.`cover` = 1 AND image_shop.id_shop = '.$id_shop.')
				LEFT JOIN `'._DB_PREFIX_.'product_download` pd ON (pd.`id_product` = a.`id_product`)';

		$this->_select .= 'shop.name as shopname, a.id_shop_default, ';
		$this->_select .= 'MAX('.$alias_image.'.id_image) id_image, cl.name `name_category`, '.$alias.'.`price`, 0 AS price_final, a.`is_virtual`, pd.`nb_downloadable`, sav.`quantity` as sav_quantity, '.$alias.'.`active`, IF(sav.`quantity`<=0, 1, 0) badge_danger';

		$this->_group = 'GROUP BY '.$alias.'.id_product';

		$this->fields_list = array();
		$this->fields_list['id_product'] = array(
			'title' => $this->l('ID'),
			'align' => 'center',
			'class' => 'fixed-width-xs',
			'type' => 'int'
		);
		$this->fields_list['image'] = array(
			'title' => $this->l('Image'),
			'align' => 'center',
			'image' => 'p',
			'orderby' => false,
			'filter' => false,
			'search' => false
		);
		$this->fields_list['name'] = array(
			'title' => $this->l('Name'),
			'filter_key' => 'b!name'
		);

		if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
			$this->fields_list['shopname'] = array(
				'title' => $this->l('Default shop'),
				'filter_key' => 'shop!name',
			);
		else
			$this->fields_list['name_category'] = array(
				'title' => $this->l('Category'),
				'filter_key' => 'cl!name',
			);
		$this->fields_list['price'] = array(
			'title' => $this->l('Base price'),
			'type' => 'price',
			'align' => 'text-right',
			'filter_key' => 'a!price'
		);
		$this->fields_list['price_final'] = array(
			'title' => $this->l('Final price'),
			'type' => 'price',
			'align' => 'text-right',
			'havingFilter' => true,
			'orderby' => false,
			'search' => false,
		);

		if (Configuration::get('PS_STOCK_MANAGEMENT'))
			$this->fields_list['sav_quantity'] = array(
				'title' => $this->l('Quantity'),
				'type' => 'int',
				'align' => 'text-right',
				'filter_key' => 'sav!quantity',
				'orderby' => true,
				'badge_danger' => true,
				//'hint' => $this->l('This is the quantity available in the current shop/group.'),
			);

		$this->fields_list['active'] = array(
			'title' => $this->l('Status'),
			'active' => 'status',
			'filter_key' => $alias.'!active',
			'align' => 'text-center',
			'type' => 'bool',
			'class' => 'fixed-width-sm',
			'orderby' => false
		);
		$this->no_link = true;
		
		$this->addRowAction('exportProduct'); //adicionao um botão na listagem
		$this->list_no_link = true; //retira o atributo onClick da td

	}

	private function checkNewVariation($sku)
    {
        $sql_netshoes = "SELECT id_sku, id_attribute FROM " . _DB_PREFIX_ . "netshoesgroup_sku WHERE id_product = " . (int)$sku->id_product;
        $result_ns = Db::getInstance()->executeS($sql_netshoes);
        $num_sku = count($result_ns);

        $sql_ps = "SELECT id_product_attribute FROM " . _DB_PREFIX_ . "product_attribute WHERE id_product = " . (int)$sku->id_product;
        $result_ps = Db::getInstance()->executeS($sql_ps);
        $num_ps = count($result_ps);
        echo '<pre style="display:none">';
        var_dump($result_ps);
//        var_dump(Db::getInstance()->getMsgError());
        var_dump($result_ns);
        echo '</pre>';

        if($num_ps > $num_sku)
        {
            return true;
        }
        else
            return false;
    }

    private function checkExportZT($id)
    {
        $sql = 'SELECT `id_sku` FROM `'._DB_PREFIX_.'netshoesgroup_sku` WHERE `business_id` = "ZT" AND `id_product` = '.(int)$id;

        $id_sku =  Db::getInstance()->getValue($sql);

        if($id_sku != false)
            return true;
        else
            return false;
    }

    private function checkExportNS($id)
    {
        $sql = 'SELECT `id_sku` FROM `'._DB_PREFIX_.'netshoesgroup_sku` WHERE `business_id` = "NS" AND `id_product` = '.(int)$id;

        $id_sku =  Db::getInstance()->getValue($sql);

        if($id_sku != false)
            return true;
        else
            return false;
    }

	public function displayExportProductLink($token, $id){
		$html = null;
		$sku = netshoesgroupSku::getSkuByProductObj($id);
		echo '<pre style="display:none;">';
		var_dump($sku);
		echo '</pre>';
		if(!$sku)
		{
			$html .= '	<a class="edit btn btn-default" title="Exportar Netshoes" href="'.self::$currentIndex.'&'.$this->identifier.'='.$id.'&business_id=NS&action=exportProduct&token='.$this->token.'">
							<i class="icon-share-sign"></i>
							'.$this->l('Export ( Netshoes )').'
						</a>';
			$html .= '	<a class="edit btn btn-default" title="Editar" href="'.self::$currentIndex.'&'.$this->identifier.'='.$id.'&business_id=ZT&action=exportProduct&token='.$this->token.'">
				<i class="icon-share-sign"></i>
				'.$this->l('Export ( Zattini )').'
			</a>';
		}
		else
		{
			if($this->checkExportNS($id))
			{
				$html .= '<p>'.$this->l('Produto Exportado ( Netshoes )').'</p>';
				if($this->checkNewVariation($sku))
                {
                    $html .= '	<a class="edit btn btn-default" title="Editar" href="'.self::$currentIndex.'&'.$this->identifier.'='.$id.'&business_id=NS&product_id='.$sku->id_netshoes.'&action=addVariation&token='.$this->token.'">
				    <i class="icon-share-sign"></i>
				    '.$this->l('Add Variations (Netshoes)').'
			            </a>';
                }
			 }
			 else
             {
                 $html .= '	<a class="edit btn btn-default" title="Exportar Netshoes" href="'.self::$currentIndex.'&'.$this->identifier.'='.$id.'&business_id=NS&action=exportProduct&token='.$this->token.'">
							<i class="icon-share-sign"></i>
							'.$this->l('Export ( Netshoes )').'
						</a>';
             }
			 if ($this->checkExportZT($id))
             {
			 	$html .= '<p>'.$this->l('Produto Exportado ( Zattini )').'</p>';
                 if($this->checkNewVariation($sku))
                 {
                     $html .= '	<a class="edit btn btn-default" title="Editar" href="'.self::$currentIndex.'&'.$this->identifier.'='.$id.'&business_id=ZT&product_id='.$sku->id_netshoes.'&action=addVariation&token='.$this->token.'">
				    <i class="icon-share-sign"></i>
				    '.$this->l('Add Variations (Zattini)').'
			            </a>';
                 }
			 }
			 else
             {
                 $html .= '	<a class="edit btn btn-default" title="Editar" href="'.self::$currentIndex.'&'.$this->identifier.'='.$id.'&business_id=ZT&action=exportProduct&token='.$this->token.'">
				<i class="icon-share-sign"></i>
				'.$this->l('Export ( Zattini )').'
			    </a>';
             }
//			 if(isset($sku->business_id) && ($sku->business_id == 'ZT' ))
//			 {
//			 	$html .= '<p>'.$this->l('Produto Exportado ( Netshoes )').'</p>';
//                 if($this->checkNewVariation($sku))
//                 {
//                     $html .= '	<a class="edit btn btn-default" title="Editar" href="'.self::$currentIndex.'&'.$this->identifier.'='.$id.'&business_id=NS&product_id='.$sku->id_netshoes.'&action=addVariation&token='.$this->token.'">
//				    <i class="icon-share-sign"></i>
//				    '.$this->l('Add Variations (Netshoes)').'
//			            </a>';
//                 }
//				$html .= '<p>'.$this->l('Produto Exportado ( Zattini )').'</p>';
//                 if($this->checkNewVariation($sku))
//                 {
//                     $html .= '	<a class="edit btn btn-default" title="Editar" href="'.self::$currentIndex.'&'.$this->identifier.'='.$id.'&business_id=ZT&product_id='.$sku->id_netshoes.'&action=addVariation&token='.$this->token.'">
//				    <i class="icon-share-sign"></i>
//				    '.$this->l('Add Variations (Zattini)').'
//			            </a>';
//                 }
//			 }
			
		}
	    return $html;
	}
	public function processFilter()
	{
		// Apply the filters in the list of associations
		if (isset($_GET[$this->table.'Filter_'.$this->identifier])){
			$_POST[$this->table.'Filter_'.$this->identifier] = (int)$_GET[$this->table.'Filter_'.$this->identifier];
		}
		parent::processFilter();
	}

	public function getList($id_lang, $orderBy = null, $orderWay = null, $start = 0, $limit = null, $id_lang_shop = null)
	{
		$orderByPriceFinal = (empty($orderBy) ? ($this->context->cookie->__get($this->table.'Orderby') ? $this->context->cookie->__get($this->table.'Orderby') : 'id_'.$this->table) : $orderBy);
		$orderWayPriceFinal = (empty($orderWay) ? ($this->context->cookie->__get($this->table.'Orderway') ? $this->context->cookie->__get($this->table.'Orderby') : 'ASC') : $orderWay);
		if ($orderByPriceFinal == 'price_final')
		{
			$orderBy = 'id_'.$this->table;
			$orderWay = 'ASC';
		}
		parent::getList($id_lang, $orderBy, $orderWay, $start, $limit, $this->context->shop->id);

		/* update product quantity with attributes ...*/
		$nb = count($this->_list);
		if ($this->_list)
		{
			$context = $this->context->cloneContext();
			$context->shop = clone($context->shop);
			/* update product final price */
			for ($i = 0; $i < $nb; $i++)
			{
				if (Context::getContext()->shop->getContext() != Shop::CONTEXT_SHOP)
					$context->shop = new Shop((int)$this->_list[$i]['id_shop_default']);

				// convert price with the currency from context
				$this->_list[$i]['price'] = Tools::convertPrice($this->_list[$i]['price'], $this->context->currency, true, $this->context);
				$this->_list[$i]['price_tmp'] = Product::getPriceStatic($this->_list[$i]['id_product'], true);
			}
		}

		if ($orderByPriceFinal == 'price_final')
		{
			if (strtolower($orderWayPriceFinal) == 'desc')
				uasort($this->_list, 'cmpPriceDesc');
			else
				uasort($this->_list, 'cmpPriceAsc');
		}
		for ($i = 0; $this->_list && $i < $nb; $i++)
		{
			$this->_list[$i]['price_final'] = $this->_list[$i]['price_tmp'];
			unset($this->_list[$i]['price_tmp']);
		}
	}

	private function filterAttribute($attribute)
    {
        $sql = "SELECT id_attribute, id_sku, id_product FROM " . _DB_PREFIX_ . "netshoesgroup_sku WHERE id_product = " . $attribute['id_product'] . " AND id_attribute = " . $attribute['id_product_attribute'];
        $sku = Db::getInstance()->getRow($sql);

        if($sku == false)
        {
            return true;
        }
        else
            return false;
    }

	public function addVariation()
    {
        $idProduct = (int)Tools::getValue($this->identifier);
        $bu_id = Tools::getValue('business_id');
        if (Tools::isSubmit('submitAddSku'))
        {

            $productsApi = new productapi\ProductsApi($this->netshoesGroup);
            $skusApi = new sku\skusApi($this->netshoesGroup);
            $product = new Product($idProduct,true,$this->context->language->id);
            $productId = Tools::getValue('productId');
            $product_id = Tools::getValue('product_id');
            $id_product_attribute = Tools::getValue('id_product_attribute');
            $name = Tools::getValue('name');
            $ean = Tools::getValue('ean');
            $sku = Tools::getValue('sku');
            $color = Tools::getValue('color');
            $flavor = Tools::getValue('flavor');
            $brand = Tools::getValue('brand');
            $size = Tools::getValue('size');
            $gender = Tools::getValue('gender');
            $product_type = Tools::getValue('product_type');
            $department = Tools::getValue('department');
            $stockQuantity = Tools::getValue('stockQuantity');
            $taxType = Tools::getValue('tax-type');
            $tax = Tools::getValue('tax');
            $listPrice = Tools::getValue('listPrice');
            $sellPrice = Tools::getValue('sellPrice');


            $error= array();

            $dataExport = array(
                "productId"=>$productId,
                "skus"=>array(),
                "department" => $department,
                "productType" => isset($product_type)?$product_type:"", //optional
                "brand" => $brand,
                "attributes" => array(),//optional
                "links" => array() //optional
            );
            $dataInsert = array();
            $skus = array();

            if(isset($id_product_attribute) && !isset($id_product_attribute[0]) )
            {
                foreach ($id_product_attribute as $key => $row)
                {
                    $skuInsert = new netshoesgroupSku();
                    $images = Image::getImages($this->context->language->id, $idProduct,$id_product_attribute[$key]);

                    if ($images == array())
                    {
                        $images = Image::getImages($this->context->language->id, $idProduct,null);
                    }
                    $imagesUrl = array();
                    if($images != array())
                    {
                        $_link = Context::getContext()->link;
                        foreach ($images as $image) {
                            $imagesUrl[]['url'] = $_link->getImageLink( $product->link_rewrite, $image['id_image'], 'large_default');
                        }
                    } else {
                        $errorImages = array();
                        $errorImages[] = $this->l('Product without image.');
                        $this->context->cookie->{$this->prefix.'error'} = json_encode($success);
                        Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$idProduct.'&action=addVariation&product_id='.$productId.'&business_id='.$bu_id.'&token='.$this->token);
                    }
                    $dataExport['skus'][] = array(
                        "sku"=>preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[$key])),
                        "name"=>trim($name[$key]),
                        "description"=>substr( trim($product->description)  ,0 ,1200 ),
                        "color" => ($flavor[$key] == '')?trim($color[$key]):"",
                        "flavor"=> ($color[$key] == '')?trim($flavor[$key]):"",
                        "size" => trim($size[$key]),
                        "gender" => trim($gender[$key]),
                        "eanIsbn" => trim($ean[$key]), //optional
                        "images" => $imagesUrl,
                        "height"=> intval($product->height),
                        "width"=> intval($product->width),
                        "depth"=> intval($product->depth),
                        "weight"=> $product->weight * 1000,
                    );
                    $dataInsert[]=array(
                        'id_product'=>$idProduct,
                        'id_attribute'=>$id_product_attribute[$key],
                        'id_sku'=>preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[$key])),
                        'variation_price'=> floatval($taxType[$key]== 'c'? 0 :$tax[$key]),
                        'variation_priority'=>($taxType[$key]== 'c'?'c':'p'),
                        'bu_id' => $bu_id,
                        'id_netshoes' => $productId,
                        'date_add'=>date('Y-m-d H:i:s'),
                        'date_upd'=>date('Y-m-d H:i:s'),

                    );
                    $productQuantity = StockAvailable::getQuantityAvailableByProduct($idProduct, $id_product_attribute[$key]);
                    $skuInsert->sku = preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[$key]));
                    $skuInsert->id_product = $idProduct;
                    $skuInsert->id_attribute = $id_product_attribute[$key];
                    $skuInsert->color = ($flavor[$key] == '')?trim($color[$key]):"";
                    $skuInsert->flavor = ($color[$key] == '')?trim($flavor[$key]):"";
                    $skuInsert->gender = trim($gender[$key]);
                    $skuInsert->brand = $brand;
                    $skuInsert->product_type = isset($product_type)?$product_type:"";
                    $skuInsert->department = $department;
                    $skuInsert->size = trim($size[$key]);
                    $skuInsert->name = trim($name[$key]);
                    $skuInsert->description = substr( trim($product->description)  ,0 ,1200 );
                    $skuInsert->eanIsbn = trim($ean[$key]);
                    $skuInsert->height = intval($product->height);
                    $skuInsert->width = intval($product->width);
                    $skuInsert->depth = intval($product->depth);
                    $skuInsert->weight = $product->weight * 1000;
                    $skuInsert->stockQuantity = $stockQuantity[$key];
                    $skuInsert->stockVariation = $stockQuantity[$key] - $productQuantity;
                    $skuInsert->enable = false;
                    $skuInsert->price = array(
                        "listPrice"=>tofloat(($listPrice[$key]>$sellPrice[$key])?$listPrice[$key]:$sellPrice[$key]),
                        "sellPrice"=>tofloat($sellPrice[$key])
                    );
                    $skuInsert->images = $imagesUrl;
                    $skuInsert->id_netshoes = $productId;
                    $skuInsert->link = array();
                    $skuInsert->situation = 'PENDING_MATCH';
                    $skuInsert->variation_price = floatval($taxType[$key]== 'c'? 0 :$tax[$key]);
                    $skuInsert->variation_priority = ($taxType[$key]== 'c'?'c':'p');
                    $skuInsert->price_impact = null;
                    $skuInsert->business_id = $bu_id;
                    $skuInsert->date_add = date('Y-m-d H:i:s');
                    $skuInsert->date_upd = date('Y-m-d H:i:s');
                    $skus[$skuInsert->sku] = $skuInsert;
                }
                logMessageNS($dataExport);
            }
            else
            {
                $skuInsert = new netshoesgroupSku();
                $images = Image::getImages($this->context->language->id, $idProduct);
                $imagesUrl = array();
                if($images){
                    $_link = Context::getContext()->link;
                    foreach ($images as $image) {
                        $imagesUrl[]['url'] = $_link->getImageLink( $product->link_rewrite, $image['id_image'], 'large_default');
                    }
                } else {
                    $errorImages = array();
                    $errorImages[] = $this->l('Product without image.');
                    $this->context->cookie->{$this->prefix.'error'} = json_encode($success);
                    Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$idProduct.'&action=addVariation&product_id='.$productId.'&business_id='.$bu_id.'&token='.$this->token);
                }

                $dataExport['skus'][] = array(
                    "sku"=>preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[0])),
                    "name"=>trim($name[0]),
                    "description"=>substr( trim($product->description)  ,0 ,1200 ),
                    "color" => ($flavor[0] == '')?trim($color[0]):"",
                    "flavor"=> ($color[0] == '')?trim($flavor[0]):"",
                    "size" => trim($size[0]),
                    "gender" => trim($gender[0]),
                    "eanIsbn" => trim($ean[0]), //optional
                    "images" => $imagesUrl,
                    "height"=> intval($product->height),
                    "width"=> intval($product->width),
                    "depth"=> intval($product->depth),
                    "weight"=> $product->weight * 1000
                );

                $dataInsert[]=array(
                    'id_product'=>$idProduct,
                    'id_attribute'=>NULL,
                    'id_sku'=>preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[0])),
                    'variation_price'=> floatval($taxType[0]== 'c'? 0 :$tax[0]),
                    'variation_priority'=>($taxType[0]== 'c'?'c':'p'),
                    'bu_id' => $bu_id,
                    'id_netshoes' => $productId,
                    'date_add'=>date('Y-m-d H:i:s'),
                    'date_upd'=>date('Y-m-d H:i:s'),

                );
                $productQuantity = StockAvailable::getQuantityAvailableByProduct($idProduct);
                $skuInsert->sku = preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[0]));
                $skuInsert->id_product = $idProduct;
                $skuInsert->color = ($flavor[0] == '')?trim($color[0]):"";
                $skuInsert->flavor = ($color[0] == '')?trim($flavor[0]):"";
                $skuInsert->gender = trim($gender[0]);
                $skuInsert->brand = $brand;
                $skuInsert->product_type = isset($product_type)?$product_type:"";
                $skuInsert->department = $department;
                $skuInsert->size = trim($size[0]);
                $skuInsert->name = trim($name[0]);
                $skuInsert->description = substr( trim($product->description)  ,0 ,1200 );
                $skuInsert->eanIsbn = trim($ean[0]);
                $skuInsert->height = intval($product->height);
                $skuInsert->width = intval($product->width);
                $skuInsert->depth = intval($product->depth);
                $skuInsert->weight = $product->weight * 1000;
                $skuInsert->stockQuantity = $stockQuantity[0];
                $skuInsert->stockVariation = $stockQuantity[0] - $productQuantity;
                $skuInsert->enable = false;
                $skuInsert->price = array(
                    "listPrice"=>tofloat(($listPrice[0]>$sellPrice[0])?$listPrice[0]:$sellPrice[0]),
                    "sellPrice"=>tofloat($sellPrice[0])
                );
                $skuInsert->images = $imagesUrl;
                $skuInsert->id_netshoes = $productId;
                $skuInsert->link = array();
                $skuInsert->situation = 'PENDING_MATCH';
                $skuInsert->variation_price = floatval($taxType[0]== 'c'? 0 :$tax[0]);
                $skuInsert->variation_priority = ($taxType[0]== 'c'?'c':'p');
                $skuInsert->price_impact = null;
                $skuInsert->business_id = $bu_id;
                $skuInsert->date_add = date('Y-m-d H:i:s');
                $skuInsert->date_upd = date('Y-m-d H:i:s');
                $skus[$skuInsert->sku] = $skuInsert;

            }
//            echo '<pre style="">';
//            var_dump($dataExport);
//            var_dump($dataInsert);
//            echo '</pre>';
//            sleep(10);
//            die();
            foreach ($dataExport['skus'] as $sku)
            {
                try {
                    $response = $skusApi->saveProductSku($product_id, $sku);
                    logMessageNS($response);
                } catch (ApiException $e) {
                    $error[] = $e->getMessage();
                    logMessageNS($e->getMessage());
                }
            }
            if( empty($error) )
            {
                foreach ($dataInsert as $row)
                {
                    $add = $skus[$row['id_sku']];
                    $add->add();
                    if(Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'netshoesgroup_relation WHERE id_sku ="'.$row['id_sku'].'" ') >0 ){
                        Db::getInstance()->update('netshoesgroup_relation', $row,'id_sku ="'.$row['id_sku'].'"');
                    }else{
                        Db::getInstance()->insert('netshoesgroup_relation', $row);
                    }
                }
                $success = array();
                $success[] = (count($dataExport['skus'])>1?$this->l('Successfully exported adverts.'):$this->l('Successfully exported advert.'));
                $this->context->cookie->{$this->prefix.'success'} = json_encode($success);
                Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
                die();
            }
            else
            {
                $error = (json_encode($error));
                $this->context->cookie->{$this->prefix.'error'} = $error;
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$idProduct.'&action=addVariation&product_id='.$productId.'&business_id='.$bu_id.'&token='.$this->token);
                die();
            }

        }
        else
        {
            $error = array();
            $productsTemplateApi = new producttemplate\ProductsTemplatesApi($this->netshoesGroup);
            try {
                $brands = $productsTemplateApi->listBrands();
            } catch (ApiException $e) {
                $error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
            }
            try {
                $departments = $productsTemplateApi->listDepartments($bu_id);
            } catch (ApiException $e) {
                $error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
            }
            try {
                $sizes = $productsTemplateApi->listSizes();

            } catch (ApiException $e) {
                $error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
            }
            try {
                $colors = $productsTemplateApi->listColors();
            } catch (ApiException $e) {
                $error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
            }
            try {
                $flavors = $productsTemplateApi->listFlavors();
            } catch (ApiException $e) {
                $error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
            }
            $genders = array();
            $genders[]['name'] = 'Homem';
            $genders[]['name'] = 'Mulher';
            $genders[]['name'] = 'Unissex';
            // department" => $department,
            //     product_type" => isset($product_type)?$product_type:NULL, //optional
            // Get Product
            $product = new Product($idProduct,true,$this->context->language->id);
            $attributes = $product->getAttributesResume($this->context->language->id);
            //var_dump($attributes);
            $categoryTax = floatval(Configuration::get($this->prefix.'variation_price'.$product->id_category_default));
            $productToExport = array(
                'getProductTypeUrl' => Tools::getHttpHost(true).__PS_BASE_URI__.'modules/netshoesgroup/getProductTypes.php',
                'id' => $product->id,
                'name' => $product->name,
                'description' => $product->description,
                'ean' => $product->ean13,
                'brand' => $product->manufacturer_name,
                'brands' => $brands['items'],
                'departments' => $departments['items'],
                'business_id' => $bu_id,
                'reference' => preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($product->reference)),
                'sellPrice'=>  number_format(Product::getPriceStatic($idProduct, true),2,'.',''),
                'listPrice' => number_format(($product->tax_rate > 0 && $product->unit_price >0)?$product->unit_price + ($product->unit_price * $product->tax_rate /100) : Product::getPriceStatic($idProduct, true),2,'.',''),
                'stockQuantity'=>StockAvailable::getQuantityAvailableByProduct($idProduct),
                'attributes' => array(),
                'categoryTax' =>$categoryTax,
            );
            // color" => trim($color[0]),
            //             "flavor"=> trim($flavor[0]),
            //             "size" => trim($size[0]),
            //             "gender" => trim($gender[0]),
            //             "ean_isbn" => trim($ean[0]), /
            $colorsExport = array();
            foreach($colors['items'] as $color) {
                $colorsExport[] = $color->name;
            }
            sort($colorsExport);
            $flavorsExport = array();
            foreach($flavors['items'] as $flavor) {
                $flavorsExport[] = $flavor->name;
            }
            sort($flavorsExport);
            $sizesExport = array();
            foreach($sizes['items'] as $size) {
                $sizesExport[] = $size->name;
            }
            $sizesExport[] = 'PP';
            sort($sizesExport);
            if(!$attributes){
                $productToExport['attributes'][] = array(
                    'id'=> 0,
                    'ean' => $product->ean13,
                    'flavors' => $flavors['items'],
                    'colors' => $colors['items'],
                    'genders' => $genders,
                    'sizes' => $sizes['items'],
                    'reference' => preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($product->reference)),
                    'name'=> $product->name,
                    'sellPrice'=>  number_format(Product::getPriceStatic($idProduct, true),2,'.',''),
                    'stockQuantity'=>StockAvailable::getQuantityAvailableByProduct($idProduct),
                );
            }else{
                foreach ($attributes as $key => $row) {

                    if(!$this->filterAttribute($row))
                    {
//                        echo 'ja existe';
                        continue;
                    }
                    $productToExport['attributes'][] = array(
                        'id'=> $row['id_product_attribute'],
                        'reference'=> preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($row['reference'])),
                        'ean'=> $row['ean13'],
                        'name'=> $product->name.' '.$row['attribute_designation'],
                        'flavors' => $flavorsExport,
                        'colors' => $colorsExport,
                        'genders' => $genders,
                        'sizes' => $sizesExport,
                        'sellPrice'=>  number_format(Product::getPriceStatic($idProduct, true,$row['id_product_attribute']),2,'.',''),
                        'stockQuantity'=>StockAvailable::getQuantityAvailableByProduct($idProduct,$row['id_product_attribute']),
                        'id'=> $row['id_product_attribute'],
                    );
                }
            }
            $productToExport['addVariation'] = true;
            $productToExport['product_id'] = $this->getProductId($idProduct);
            $tpl = $this->context->smarty->createTemplate($this->getTemplatePath().'product_export.tpl');
            $tpl->assign('product', $productToExport);
            $this->content .= $tpl->fetch();
//            $this->var_dump($productToExport);
//            die();
            return $this->content;
        }
    }

    private function getProductId($id_product)
    {
        $sql = "SELECT sku, business_id FROM " . _DB_PREFIX_ . "netshoesgroup_sku WHERE id_product = " . $id_product;
        $skus = Db::getInstance()->executeS($sql);

//        $this->var_dump($skus);

        $productsApi = new productapi\ProductsApi($this->netshoesGroup);
        $skusApi = new sku\skusApi($this->netshoesGroup);

        foreach ($skus as $sku)
        {
            $bus_id = $sku['business_id'];
            if($bus_id == "Netshoes" || $bus_id == "NS")
            {
                $bu_id = "NS";
                $sku['business_id'] = $bu_id;
            }
            if($bus_id == "Zattini" || $bus_id == "ZT")
            {
                $bu_id = "ZT";
                $sku['business_id'] = $bu_id;
            }
            $response = $skusApi->getStatus($sku['sku'], $bu_id);
            if($response != false)
            {
//                $this->var_dump($response);
                foreach ($response->links as $link)
                {
                    if($link->rel == "Product" || $link->rel == "product")
                    {
                        $href = explode('products/', $link->href);
                        $href = explode('/skus', $href[1]);
//                        $this->var_dump($href);
                        return $href[0];
                    }
                }
                return 1;
            }
            else
            {
                sleep(1);
                continue;
            }
        }
    }

    private function var_dump($content, $show = false)
    {
        $html = "<pre";
        if(!$show)
            $html .= " style='display: none;'>";
        else
            $html .= ">";
        echo $html;
        var_dump($content);
        echo '</pre>';
    }

	public function exportProduct()
	{
		
    	$idProduct = (int)Tools::getValue($this->identifier);
    	$bu_id = Tools::getValue('business_id');


		if (Tools::isSubmit('submitExport')){

	       		$product = new Product($idProduct,true,$this->context->language->id);
    			$productId = ($product->reference != '')?preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($product->reference)):$idProduct;
			$id_product_attribute = Tools::getValue('id_product_attribute');
			$name = Tools::getValue('name');
			$ean = Tools::getValue('ean');
			$sku = Tools::getValue('sku');
			$color = Tools::getValue('color');
			$flavor = Tools::getValue('flavor');
			$brand = Tools::getValue('brand');
			$size = Tools::getValue('size');
			$gender = Tools::getValue('gender');
			$product_type = Tools::getValue('product_type');
			$department = Tools::getValue('department');
			$stockQuantity = Tools::getValue('stockQuantity');
			$taxType = Tools::getValue('tax-type');
			$tax = Tools::getValue('tax');
			$listPrice = Tools::getValue('listPrice');
			$sellPrice = Tools::getValue('sellPrice');

			
			$error= array();

			$dataExport = array(
				"productId"=>$productId,
			    "skus"=>array(),
			    "department" => $department,
			    "productType" => isset($product_type)?$product_type:"", //optional
			    "brand" => $brand,
			    "attributes" => array(),//optional
			    "links" => array() //optional
			);
			$dataInsert = array();
			$skus = array();
			
			if(isset($id_product_attribute) && !isset($id_product_attribute[0]) ) {
				foreach ($id_product_attribute as $key => $row) {
					
					$skuInsert = new netshoesgroupSku();
					$images = Image::getImages($this->context->language->id, $idProduct,$id_product_attribute[$key]);
					
					if ($images == array()) {
						$images = Image::getImages($this->context->language->id, $idProduct,null);
					}
					$imagesUrl = array();
					if($images != array()){
						$_link = Context::getContext()->link;
						foreach ($images as $image) {
							$imagesUrl[]['url'] = $_link->getImageLink( $product->link_rewrite, $image['id_image'], 'large_default');
						}
					} else {
						$errorImages = array();
						$errorImages[] = $this->l('Product without image.');
						$this->context->cookie->{$this->prefix.'error'} = json_encode($success);
						Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$idProduct.'&action=exportProduct&business_id='.$bu_id.'&token='.$this->token);
					}
					$dataExport['skus'][] = array(
				            "sku"=>preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[$key])),
				            "name"=>trim($name[$key]),
				            "description"=>substr( trim($product->description)  ,0 ,1200 ),
				            "color" => ($flavor[$key] == '')?trim($color[$key]):"",
				            "flavor"=> ($color[$key] == '')?trim($flavor[$key]):"",
				            "size" => trim($size[$key]),
				            "gender" => trim($gender[$key]),
				            "eanIsbn" => trim($ean[$key]), //optional
				            "images" => $imagesUrl,
				            "height"=> intval($product->height), 
				            "width"=> intval($product->width),
				            "depth"=> intval($product->depth),
				            "weight"=> $product->weight * 1000,
				        );
					$dataInsert[]=array(
						'id_product'=>$idProduct,
						'id_attribute'=>$id_product_attribute[$key],
						'id_sku'=>preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[$key])),
						'variation_price'=> floatval($taxType[$key]== 'c'? 0 :$tax[$key]),
						'variation_priority'=>($taxType[$key]== 'c'?'c':'p'),
						'bu_id' => $bu_id,
						'id_netshoes' => $productId,
						'date_add'=>date('Y-m-d H:i:s'),
						'date_upd'=>date('Y-m-d H:i:s'),
						
					);
					$productQuantity = StockAvailable::getQuantityAvailableByProduct($idProduct, $id_product_attribute[$key]);
					$skuInsert->sku = preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[$key]));
					$skuInsert->id_product = $idProduct;
					$skuInsert->id_attribute = $id_product_attribute[$key];
					$skuInsert->color = ($flavor[$key] == '')?trim($color[$key]):"";
					$skuInsert->flavor = ($color[$key] == '')?trim($flavor[$key]):"";
					$skuInsert->gender = trim($gender[$key]);
					$skuInsert->brand = $brand;
					$skuInsert->product_type = isset($product_type)?$product_type:"";
					$skuInsert->department = $department;
					$skuInsert->size = trim($size[$key]);
					$skuInsert->name = trim($name[$key]);
					$skuInsert->description = substr( trim($product->description)  ,0 ,1200 );
					$skuInsert->eanIsbn = trim($ean[$key]);
					$skuInsert->height = intval($product->height);
					$skuInsert->width = intval($product->width); 
					$skuInsert->depth = intval($product->depth);
					$skuInsert->weight = $product->weight * 1000; 
					$skuInsert->stockQuantity = (($brand == 'Oakley' || strpos(trim($name[$key]), 'Oakley') !== false))?0:$stockQuantity[$key];
					$skuInsert->stockVariation = (($brand == 'Oakley' || strpos(trim($name[$key]), 'Oakley') !== false))?0:$stockQuantity[$key] - $productQuantity;
					$skuInsert->enable = false;
					$skuInsert->price = array(
							"listPrice"=>tofloat(($listPrice[$key]>$sellPrice[$key])?$listPrice[$key]:$sellPrice[$key]),
				             "sellPrice"=>tofloat($sellPrice[$key])
				         );
					$skuInsert->images = $imagesUrl;
					$skuInsert->id_netshoes = $productId;
					$skuInsert->link = array();
					$skuInsert->situation = 'PENDING_MATCH';
					$skuInsert->variation_price = floatval($taxType[$key]== 'c'? 0 :$tax[$key]);
					$skuInsert->variation_priority = ($taxType[$key]== 'c'?'c':'p');
					$skuInsert->price_impact = null;
					$skuInsert->business_id = $bu_id;
					$skuInsert->date_add = date('Y-m-d H:i:s');
					$skuInsert->date_upd = date('Y-m-d H:i:s');
					$skus[$skuInsert->sku] = $skuInsert;
					
				
					

				}
				logMessageNS($dataExport);
			} else {
				
				$skuInsert = new netshoesgroupSku();
				$images = Image::getImages($this->context->language->id, $idProduct);
				$imagesUrl = array();
				if($images){
					$_link = Context::getContext()->link;
					foreach ($images as $image) {
						$imagesUrl[]['url'] = $_link->getImageLink( $product->link_rewrite, $image['id_image'], 'large_default');
					}
				} else {
					$errorImages = array();
					$errorImages[] = $this->l('Product without image.');
					$this->context->cookie->{$this->prefix.'error'} = json_encode($success);
					Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$idProduct.'&action=exportProduct&business_id='.$bu_id.'&token='.$this->token);
				}
				
				$dataExport['skus'][] = array(
			            "sku"=>preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[0])),
			            "name"=>trim($name[0]),
			            "description"=>substr( trim($product->description)  ,0 ,1200 ),
			            "color" => ($flavor[0] == '')?trim($color[0]):"",
			            "flavor"=> ($color[0] == '')?trim($flavor[0]):"",
			            "size" => trim($size[0]),
			            "gender" => trim($gender[0]),
			            "eanIsbn" => trim($ean[0]), //optional
			            "images" => $imagesUrl,
			            "height"=> intval($product->height), 
			            "width"=> intval($product->width),
			            "depth"=> intval($product->depth),
			            "weight"=> $product->weight * 1000
			        );

				$dataInsert[]=array(
					'id_product'=>$idProduct,
					'id_attribute'=>NULL,
					'id_sku'=>preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[0])),
					'variation_price'=> floatval($taxType[0]== 'c'? 0 :$tax[0]),
					'variation_priority'=>($taxType[0]== 'c'?'c':'p'),
					'bu_id' => $bu_id,
					'id_netshoes' => $productId,
					'date_add'=>date('Y-m-d H:i:s'),
					'date_upd'=>date('Y-m-d H:i:s'),
					
				);
				$productQuantity = StockAvailable::getQuantityAvailableByProduct($idProduct);
				$skuInsert->sku = preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($sku[0]));
				$skuInsert->id_product = $idProduct;
				$skuInsert->color = ($flavor[0] == '')?trim($color[0]):"";
				$skuInsert->flavor = ($color[0] == '')?trim($flavor[0]):"";
				$skuInsert->gender = trim($gender[0]);
				$skuInsert->brand = $brand;
				$skuInsert->product_type = isset($product_type)?$product_type:"";
				$skuInsert->department = $department;
				$skuInsert->size = trim($size[0]);
				$skuInsert->name = trim($name[0]);
				$skuInsert->description = substr( trim($product->description)  ,0 ,1200 );
				$skuInsert->eanIsbn = trim($ean[0]);
				$skuInsert->height = intval($product->height);
				$skuInsert->width = intval($product->width); 
				$skuInsert->depth = intval($product->depth);
				$skuInsert->weight = $product->weight * 1000; 
				$skuInsert->stockQuantity = (($brand == 'Oakley' || strpos(trim($name[0]), 'Oakley') !== false))?0:$stockQuantity[0];
				$skuInsert->stockVariation = (($brand == 'Oakley' || strpos(trim($name[0]), 'Oakley') !== false))?0:$stockQuantity[0] - $productQuantity;
				$skuInsert->enable = false;
				$skuInsert->price = array(
						"listPrice"=>tofloat(($listPrice[0]>$sellPrice[0])?$listPrice[0]:$sellPrice[0]),
			             "sellPrice"=>tofloat($sellPrice[0])
			         );
				$skuInsert->images = $imagesUrl;
				$skuInsert->id_netshoes = $productId;
				$skuInsert->link = array();
				$skuInsert->situation = 'PENDING_MATCH';
				$skuInsert->variation_price = floatval($taxType[0]== 'c'? 0 :$tax[0]);
				$skuInsert->variation_priority = ($taxType[0]== 'c'?'c':'p');
				$skuInsert->price_impact = null;
				$skuInsert->business_id = $bu_id;
				$skuInsert->date_add = date('Y-m-d H:i:s');
				$skuInsert->date_upd = date('Y-m-d H:i:s');
				$skus[$skuInsert->sku] = $skuInsert;
		
			}
			$productsApi = new productapi\ProductsApi($this->netshoesGroup);
			$skusApi = new sku\skusApi($this->netshoesGroup);
			try {
				$response = $productsApi->saveProduct($dataExport);
				logMessageNS($response);
			} catch (ApiException $e) {
				$error[] = $e->getMessage();
				logMessageNS($e->getMessage());
			}
			if( empty($error) ) {
				foreach ($dataInsert as $row) {
					$add = $skus[$row['id_sku']];
					$add->add();
					if(Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'netshoesgroup_relation WHERE id_sku ="'.$row['id_sku'].'" ') >0 ){
						Db::getInstance()->update('netshoesgroup_relation', $row,'id_sku ="'.$row['id_sku'].'"');
					}else{
						Db::getInstance()->insert('netshoesgroup_relation', $row);
					}
				}
				$success = array();
				$success[] = (count($dataExport['skus'])>1?$this->l('Successfully exported adverts.'):$this->l('Successfully exported advert.'));
				$this->context->cookie->{$this->prefix.'success'} = json_encode($success);
				Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
				die();
			}else{
				$error = (json_encode($error));
				$this->context->cookie->{$this->prefix.'error'} = $error;
				Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$idProduct.'&action=exportProduct&business_id='.$bu_id.'&token='.$this->token);
		    	die();
			}
			
		}
		else
		{
	    	$error = array();
			$productsTemplateApi = new producttemplate\ProductsTemplatesApi($this->netshoesGroup);
			try {
				$brands = $productsTemplateApi->listBrands();
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			try {
				$departments = $productsTemplateApi->listDepartments($bu_id);
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			try {
				$sizes = $productsTemplateApi->listSizes();

			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			try {
				$colors = $productsTemplateApi->listColors();
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			try {
				$flavors = $productsTemplateApi->listFlavors();
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			$genders = array();
			$genders[]['name'] = 'Homem';
			$genders[]['name'] = 'Mulher';
			$genders[]['name'] = 'Unissex';
			// department" => $department,
			//     product_type" => isset($product_type)?$product_type:NULL, //optional
	    	// Get Product
	       	$product = new Product($idProduct,true,$this->context->language->id);
	       	$attributes = $product->getAttributesResume($this->context->language->id);
			$categoryTax = floatval(Configuration::get($this->prefix.'variation_price'.$product->id_category_default));
			$productToExport = array(
				'getProductTypeUrl' => Tools::getHttpHost(true).__PS_BASE_URI__.'modules/netshoesgroup/getProductTypes.php',
				'id' => $product->id,
				'name' => $product->name,
				'description' => $product->description,
				'ean' => $product->ean13,
				'brand' => $product->manufacturer_name,
				'brands' => $brands['items'],
				'departments' => $departments['items'],
				'business_id' => $bu_id,
				'reference' => preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($product->reference)),
				'sellPrice'=>  number_format(Product::getPriceStatic($idProduct, true),2,'.',''),
				'listPrice' => number_format(($product->tax_rate > 0 && $product->unit_price >0)?$product->unit_price + ($product->unit_price * $product->tax_rate /100) : Product::getPriceStatic($idProduct, true),2,'.',''),
				'stockQuantity'=>StockAvailable::getQuantityAvailableByProduct($idProduct),
				'attributes' => array(),
				'categoryTax' =>$categoryTax,
			);
			// color" => trim($color[0]),
			//             "flavor"=> trim($flavor[0]),
			//             "size" => trim($size[0]),
			//             "gender" => trim($gender[0]),
			//             "ean_isbn" => trim($ean[0]), /
			$colorsExport = array();
			foreach($colors['items'] as $color) {
				$colorsExport[] = $color->name;
			}
			sort($colorsExport);
			$flavorsExport = array();
			foreach($flavors['items'] as $flavor) {
				$flavorsExport[] = $flavor->name;
			}
			sort($flavorsExport);
			$sizesExport = array();
			foreach($sizes['items'] as $size) {
				$sizesExport[] = $size->name;
			}
			$sizesExport[] = 'PP';
			sort($sizesExport);
            $images = Image::getImages($this->context->language->id, $idProduct,$id_product_attribute[$key]);

            if ($images == array()) {
                $images = Image::getImages($this->context->language->id, $idProduct,null);
            }
            $imagesUrl = array();
            $_link = Context::getContext()->link;
            foreach ($images as $image) {
                $imagesUrl[]['url'] = $_link->getImageLink( $product->link_rewrite, $image['id_image'], 'large_default');
            }
			if(!$attributes){

				$productToExport['attributes'][] = array(
					'id'=> 0,
					'ean' => $product->ean13,
					'flavors' => $flavors['items'],
					'colors' => $colors['items'],
					'genders' => $genders,
					'sizes' => $sizes['items'],
					'reference' => preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($product->reference)),
					'name'=> $product->name,
					'sellPrice'=>  number_format(Product::getPriceStatic($idProduct, true),2,'.',''),
					'stockQuantity'=>StockAvailable::getQuantityAvailableByProduct($idProduct),
                    'image' => $imagesUrl[0]['url']
				);
			}else{
				foreach ($attributes as $key => $row) {
					
					
					$productToExport['attributes'][] = array(
						'id'=> $row['id_product_attribute'],
						'reference'=> preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($row['reference'])),
						'ean'=> $row['ean13'],
						'name'=> $product->name.' '.$row['attribute_designation'],
						'flavors' => $flavorsExport,
						'colors' => $colorsExport,
						'genders' => $genders,
						'sizes' => $sizesExport,
						'sellPrice'=>  number_format(Product::getPriceStatic($idProduct, true,$row['id_product_attribute']),2,'.',''),
						'stockQuantity'=>StockAvailable::getQuantityAvailableByProduct($idProduct,$row['id_product_attribute']),
						'id'=> $row['id_product_attribute'],
                        'image' => $imagesUrl[0]['url']
					);
				}
			}
			
			$tpl = $this->context->smarty->createTemplate($this->getTemplatePath().'product_export.tpl');
			$tpl->assign('product', $productToExport);
			$this->content .= $tpl->fetch();
	        return $this->content;
		}
	}
}
