<?php
use ApiMktpNetshoesV1\order as order;

if (!defined('_PS_VERSION_'))
	exit;

require_once dirname(__FILE__).'/NetshoesGroupDefaultController.php';

class NetshoesGroupOrdersController extends NetshoesGroupDefaultController {
	private $statusList = array(
		'Created' => 'Aguardando Pagamento',
		'Approved' => 'Pagamento Aprovado',
		// 'PROCESSING' => 'Confirmado sem Nota Fiscal',
		'Invoiced'=> 'Nota Fiscal Emitida',
		'Shipped' => 'Enviado',
		'Delivered' => 'Entregue',
		// 'SHIPMENT_EXCEPTION' => 'Exceção de Transporte',
		// 'UNAVAILABLE' => 'Indis. de Estoque',
		'Canceled' => 'Cancelado',
		// 'NOT_APPROVED' => 'Pagamento Vencido',
	);
	private $statusListColor = array(
		'Created' => '#F0AD4E',
		'Approved' => '#32CD32',
		// 'PROCESSING' => '#FFFF00',
		'Invoiced' => '#108510',
		'Shipped' => '#25b9d7',
		'Delivered' => '#B746F4',
		// 'SHIPMENT_EXCEPTION' => '#5BC0DE',
		// 'UNAVAILABLE' => '#777777',
		'Canceled' => '#F71827',
		// 'NOT_APPROVED' => '#F71827',
	);
	
	public function __construct()
	{
		$this->controllerName = 'NetshoesGroupOrders';
		$this->table = 'netshoesgroup_order';
		$this->className = 'netshoesgroupOrder';

		parent::__construct();
		$this->ordersApi = new order\OrdersApi($this->netshoesGroup);
		
	}
	
	public function initProcess()
	{
	    $this->action = (Tools::getValue('action'))?Tools::getValue('action'):'list';	
	    $this->displayList();	
	 //    switch ($this->action) {
		// 	case 'list':
		// 		$this->displayList();
		// 		break;
		// 	case 'updatedStatus':
		// 		$this->updatedStatus();
		// 		break;
		// 	case 'connect':
		// 		$this->connectSKU();
		// 		break;
		// 	case 'disconnect':
		// 		$this->disconnectSKU();
		// 		break;
		// 	default:
		// 		$this->errors[] = $this->l('Oops! Página não encontrada.');
		// 		$this->displayList();
		// 		break;
		// }
		
	    parent::initProcess();
	}
	
	public function getList($id_lang, $orderBy = null, $orderWay = null, $start = 0, $limit = 20, $id_lang_shop = null)
	{
		$this->content.= $this->viewCommissionBar();
		parent::getList($id_lang, $orderBy, $orderWay, $start, $limit, $this->context->shop->id);
		$this->orders = array();
		if($this->_list){
			foreach ($this->_list as $key => $row) {
				$this->_list[$key]['status'] =  isset($this->statusList[$row['status']])?$this->statusList[$row['status']]:$row['status'];
				$this->_list[$key]['statusColor'] =  isset($this->statusListColor[$row['status']])?$this->statusListColor[$row['status']]:NULL;
				$this->orders[$row['id_order']] = $row;
			}
		}
		// $this->content .= debug($this->_listsql);
	}
	
	private function viewCommissionBar()
	{
		$install_date = Configuration::get('netshoesgroup_install_date').' 00:00:00';
		$sql = "SELECT SUM(total_gross) AS total_gross FROM `"._DB_PREFIX_."netshoesgroup_order` WHERE 1 ";
		$sql.= " AND `order_date` >= '".date('Y-m')."-01 00:00:00' ";
		$sql.= " AND `status` != 'Created'";
		// $sql.= " AND `status` != 'UNAVAILABLE'";
		$sql.= " AND `status` != 'Canceled'";
		// $sql.= " AND `status` != 'NOT_APPROVED'";

		$sqlCommission = "SELECT SUM(total_commission) AS total_commission FROM `"._DB_PREFIX_."netshoesgroup_order` WHERE 1 ";
		$sqlCommission.= " AND `order_date` >= '".date('Y-m')."-01 00:00:00' ";
		$sql.= " AND `status` != 'Created'";
		// $sql.= " AND `status` != 'UNAVAILABLE'";
		$sqlCommission.= " AND `status` != 'Canceled'";
		// $sql.= " AND `status` != 'NOT_APPROVED'";
		
		$dateEstimate =  date('Y-m-d', strtotime("-".date('t')." days"));
		$sql2 = "SELECT SUM(total_gross) AS total_gross FROM `"._DB_PREFIX_."netshoesgroup_order` WHERE 1 ";
		$sql2.= " AND `order_date` >= '".$dateEstimate." 00:00:00' ";
		$sql2.= " AND `status` != 'Created'";
		// $sql2.= " AND `status` != 'UNAVAILABLE'";
		$sql2.= " AND `status` != 'Canceled'";
		// $sql2.= " AND `status` != 'NOT_APPROVED'";


		
		$total_amount = floatval(Db::getInstance()->getValue($sql));
		$total_estimate = floatval(Db::getInstance()->getValue($sql2));
		$commission = floatval(Db::getInstance()->getValue($sqlCommission));

		if($total_amount['total_gross'] != 0 && $total_estimate['total_gross'] != 00 && $total_commission['total_commission'] != 0) {
			$min_commission = (Configuration::get('netshoesgroup_min_commission')?Configuration::get('netshoesgroup_min_commission'):0.8);
			$tax_commission = (Configuration::get('netshoesgroup_tax_commission')?Configuration::get('netshoesgroup_tax_commission'):200);
			// $commission = $total_amount * $tax_commission / 100;
			$total_estimate = ($total_amount / date('d')) * date('t');
			$commission_estimate = $total_estimate * $tax_commission /100;
			$current_percent = $total_amount * 100 /$total_estimate;
			
			$tpl = $this->context->smarty->createTemplate($this->getTemplatePath().'commission_bar.tpl');
			$tpl->assign(array(
				'total_amount'=>$total_amount,
				'tax_commission'=>$tax_commission,
				'min_commission'=>$min_commission,
				'commission'=>$commission,
				'total_estimate'=>$total_estimate,
				'commission_estimate'=>$commission_estimate,
				'current_percent'=> $current_percent
			));
			return $tpl->fetch();
		}
		return false;
	}
	
	private function displayList(){
		$this->_pagination = array(20, 50,100,300);
	    $this->identifier = 'id_order';
        $this->lang = false;
		$this->addRowAction('view');
		$this->addRowAction('synchronize');
		$this->addRowAction('reversal');
		$this->_select .= 'a.id_order, a.status AS progress, a.reversal, a.shipping_info ';
        $this->_orderBy = 'a.order_date';
        $this->_orderWay = 'DESC';
		$this->fields_list = array();
		$this->tpl_list_vars['toolbar_btn'] = FALSE;
		
		$this->fields_list['id_netshoes'] = array(
			'title' => $this->l('ID'),
			'align' => 'center',
			'type' => 'text',
			'filter_key' => 'a!id_netshoes'
		);
		$this->fields_list['business_unit'] = array(
			'title' => $this->l('Loja'),
			'filter_key' => 'a!business_unit',
			'type' => 'select',
			'list'=> array(
				'ZT'=>'Zattini',
				'NS'=>'Netshoes'
			),
		);
		$this->fields_list['customer_name'] = array(
			'title' => $this->l('Cliente'),
			'havingFilter' => true,
		);
		$this->fields_list['total_gross'] = array(
			'title' => $this->l('Total'),
            'type' => 'price',
			'align' => 'text-right',
			'class' => 'fixed-width-md'
		);
		$this->fields_list['total_commission'] = array(
			'title' => $this->l('Commisão'),
            'type' => 'price',
			'align' => 'text-right',
			'class' => 'fixed-width-md'
		);
		$this->fields_list['order_date'] = array(
			'title' => $this->l('Data'),
            'align' => 'text-center',
            'type' => 'datetime',
			'filter_key' => 'a!order_date'
		);
		$this->fields_list['status'] = array(
            'title' => $this->l('Status'),
            'align' => 'text-center',
            'filter_key' => 'a!status',
            'type' => 'select',
            'color' => 'statusColor',
            'list'=> $this->statusList,
		);
		$this->fields_list['progress'] = array(
            'title' => $this->l('Andamento'),
            'align' => 'text-center',
            'callback' => 'printProgressIcons',
            'remove_onclick' => true,
			'orderby' => false,
			'search' => false,
		);
		$this->tpl_list_vars['toolbar_btn'] = FALSE;
	}
	
	public function displaySynchronizeLink($token, $id){
		$href = self::$currentIndex.'&token='.$this->token.'&id_order='.$id.'&synchronize=1';
	    $html = '<a href="'.$href.'" title="Sincronizar" class="delete">
					<i class="icon-refresh"></i>  Sincronizar
				</a>';
		return $html;
	}
	public function displayReversalLink($token, $id){
		$html = null;
		if((int) $this->orders[$id]['reversal'] == 0){
			$href = self::$currentIndex.'&token='.$this->token.'&id_order='.$id.'&reversal=1';
		    $html = '<a href="'.$href.'" title="Estornar" class="delete">
						<i class="icon-reply"></i>  Estornar
					</a>';
		}
		return $html;
	}
	
	public function renderView()
	{	
		$order = $this->loadObject(true);
		$id = $order->id_order;
		$this->context->controller->addJqueryUI('ui.datepicker');
		// $this->addJS(array(_PS_JS_DIR_.'jquery/plugins/timepicker/jquery-ui-timepicker-addon.js'));
		// $this->addCSS(array(_PS_JS_DIR_.'jquery/plugins/timepicker/jquery-ui-timepicker-addon.css'));
		
		$order->statusLabel =  isset($this->statusList[$order->status])?$this->statusList[$order->status]:$order->status;
		$order->statusLabelColor =  isset($this->statusListColor[$order->status])?$this->statusListColor[$order->status]:NULL;
		
		if(count($order->products) > 0){
			foreach ($order->products as $key => &$product) {
				$sku = netshoesgroupSku::getSkuByNetshoesId($product->sku);
				$product->name = ($sku)?$sku->name:$product->name;
				$product->img = NULL;
				if($sku && $sku->images[0]){
					$product->img = $sku->images[0];
				} elseif($sku && $sku->id_product){
					$imgs = Image::getImages(Context::getContext()->language->id, $sku->id_product, $sku->id_attribute);
					$product->img = isset($imgs[0])?Context::getContext()->link->getImageLink( Tools::str2url($sku->name), 26, 'cart_default'):NULL;
				}
				
			}
		}
//        $order->shipping->shipping_estimate = (int)$order->shipping->shipping_estimate;
        $order->shipping->shipping_estimate = substr($order->shipping->shipping_estimate, 0, -3);
//		var_dump($order->shipping->shipping_estimate);
        $order->shipping->shipping_estimate = date('Y-m-d', $order->shipping->shipping_estimate);
//        var_dump($order->shipping->shipping_estimate);
		$tpl = $this->context->smarty->createTemplate($this->getTemplatePath().'order_view.tpl');
		$tpl->assign(array(
			'id'=>$id,
			'order' => $order,
			'link'=> $this->context->link,
			'statusList' => $this->statusList,
			'statusListColor' => $this->statusListColor,
		));
		return $tpl->fetch();
	}

 	public function printProgressIcons($status, $tr){
 		// $this->content .= debug(array($status, $tr));
 		//$html = '<pre style="display: none">'.var_dump($tr).'</pre>';
 		$shipping_info = (array) json_decode($tr['shipping_info']);
		$paymentClass = '';
        $shippingClass = '';
        $homeClass = '';
        $paymentText ='';
        $shippingText ='';
        $homeText = '';
		$tracking_url = '';


		if($shipping_info && isset($shipping_info['number']) && $shipping_info['number']){
			if(isset($shipping_info['trackingUrl']) && $shipping_info['trackingUrl'] != '')
			{
				$tracking_url = $shipping_info['trackingUrl'].$shipping_info['number'];
			}
			elseif(strlen($shipping_info['number']) == 13)
			{
				$tracking_url = 'http://rastreie.me/'.$shipping_info['number'];
			}
			elseif(strlen($shipping_info['number']) == 14)
			{
				$tracking_url = 'http://www.jadlog.com.br/siteDpd/tracking.jad?cte='.$shipping_info['number'];
			}
				//'http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_TIPO=001&P_LINGUA=001&P_COD_UNI='.$shipping_info['number'];
		}
		// 'Created' => 'Aguardando Pagamento',
		// 'APPROVED' => 'Pagamento Aprovado',
		// 'INVOICED'=>'Nota Fiscal Emitida',
		// 'SHIPPED' => 'Enviado',
		// 'DELIVERED ' => 'Entregue',
		// 'SHIPMENT_EXCEPTION' => 'Exceção de Transporte',
		// 'UNAVAILABLE' => 'Indis. de Estoque',
		// 'CANCELED' => 'Cancelado',
		// 'NOT_APPROVED' => 'Pagamento Vencido',
		
	  	if($status == 'Canceled'){
            $paymentClass .= ' btn-danger';
            $paymentText = 'Pedido Cancelado';
        }
        // elseif($status == 'NOT_APPROVED'){
        //     $paymentClass .= ' btn-danger';
        //     $paymentText = 'Pagamento Vencido';
        // }
        elseif($status == 'Created'){
            $paymentClass .= ' btn-warning';
            $paymentText = 'Aguardando Pagamento';
        }
        // elseif($status == 'UNAVAILABLE'){
		// 	  $paymentClass .= ' btn-danger';
        //    $paymentText = 'Indis. de Estoque';
        // }
        // elseif($status == 'PROCESSING'){
		//	  $paymentClass .= ' btn-info';
        //    $shippingClass .= '  btn-warning';
        //    $paymentText = 'Pedido confirmado, Aguardando nota fiscal';
        //    $shippingText = 'Aguardando Envio';
        // }
        elseif($status == 'Approved' || $status == 'Invoiced'){
			$paymentClass .= ' btn-success';
            $shippingClass .= '  btn-warning';
            $paymentText = 'Pagamento Aprovado';
            $shippingText = 'Aguardando Envio';
        }
        // elseif($status == 'SHIPMENT_EXCEPTION'){
		//	  $paymentClass .= ' btn-success';
        //    $shippingClass .= '  btn-info';
        //    $paymentText = 'Pagamento Aprovado';
        //    $shippingText = 'Exceção de Transporte';
        // }
        elseif($status == 'Shipped'){
			$paymentClass .= ' btn-success';
            $shippingClass .= '  btn-success';
            $homeClass .= ' btn-warning';
            $paymentText = 'Pagamento Aprovado';
            $shippingText = 'Pedido Enviado';
            $homeText = 'Aguardando entrega';
        }elseif($status == 'Delivered'){
			$paymentClass .= ' btn-success';
            $shippingClass .= '  btn-success';
            $homeClass .= ' btn-success';
            $paymentText = 'Pagamento Aprovado';
            $shippingText = 'Pedido Enviado';
            $homeText = 'Entrega Realizada';
        }
		
 		$html = '<div class="btn-group">'.
    			'<button class="btn help-tooltip '.$paymentClass.'" type="button" data-toggle="tooltip" data-placement="top" title="'.$paymentText.'"> <i class="icon-credit-card"></i></button>'.
    			'<button '.($tracking_url != '' ? 'onclick="window.open(\''.$tracking_url.'\', \'_blank\');"': '').' class="btn help-tooltip '.$shippingClass.'" type="button"  data-toggle="tooltip" data-placement="top" title="'.$shippingText.'"><i class="icon-truck"></i></button>'.
    			'<button class="btn help-tooltip '.$homeClass.'" type="button"  data-toggle="tooltip" data-placement="top" title="'.$homeText.'"><i class="icon-home"></i></button>'.
			'</div>';
        return $html;
 	}
	
	public function postProcess()
    {
    	// var_dump($this->netshoesGroup);
    	$ordersApi = new order\OrdersApi($this->netshoesGroup);
    	if(Tools::getIsset('reversal')){
    		$order = $this->loadObject(true);
			$success = array();
			$error = array();
			if(!$order->reversal){
				if(is_array($order->products) && count($order->products)>0){
					foreach ($order->products as $row) {
						$sql = 'SELECT `id_product`,`id_attribute` FROM `'._DB_PREFIX_.'netshoesgroup_sku` WHERE `sku`= "'.$row->sku.'"';
						$product = Db::getInstance()->getRow($sql);
						if(is_array($product) && count($product)>0){
							if(StockAvailable::updateQuantity((int)$product['id_product'], (int)$product['id_attribute'], +(int)$row->quantity)){
								$success[] = 'Produto '.$row->sku.' do pedido '.$order->id_netshoes.' estornado com sucesso.';
							}else{
								$error[] = 'Erro ao estornar o produto '.$row->sku.' do pedido '.$order->id_netshoes.'.';
							}
						}else{
							$error[] = 'Produto '.$row->sku.' não está associado.';
						}
					}
				}else{
					$error[] = 'Nenhum produto foi encontrado no pedido '.$order->id_netshoes.'.';
				}
				try{
					$order->reversal = true;
					$result = $order->update();
				} catch (Exception $e) {
					$error[] = $e->getMessage();
				}
				if(!$result){
					$errorDb = Db::getInstance()->getMsgError();
					if($errorDb){
						$error[] = $errorDb;
					}
				}
			}
			if(count($success) > 0 )
				$this->context->cookie->{$this->prefix.'success'} = json_encode($success);
			if(count($error)>0){
				$error = (json_encode($error));
				logMessageNS($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
			}
		    ob_end_clean();
			Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
    		die();
		}
    	if(Tools::getIsset('synchronize')){
    		$order = $this->loadObject(true);
    		$idOrderNetshoes = (Tools::getValue('id_netshoes'))?Tools::getValue('id_netshoes'):$order->id_netshoes;
		
			$error = array();
			try {
				$orderApi = $this->ordersApi->getOrder($idOrderNetshoes, array("shippings", "items", "devolutionItems", "address"));
			} catch (ApiException $e) {
				logMessageNS($e);
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			logMessageNS($orderApi);
			if(empty($error) && !empty($order) ) {
				try{
					$order->id_site = isset($orderApi['origin_number'])?$orderApi['origin_number']:NULL;
					$order->business_unit = ($orderApi['business_unit'] == 1)?'NS':'ZT';
					$order->devolution_requested = isset($orderApi['devolution_requested'])?$orderApi['devolution_requested']:NULL;
					$order->exchange_requested = isset($orderApi['exchange_requested'])?$orderApi['exchange_requested']:NULL;
					$order->order_date = isset($orderApi['order_date'])?date("Y-m-d H:i:s", ($orderApi['order_date']/1000)):NULL;
					$order->order_type = isset($orderApi['order_type'])?$orderApi['order_type']:NULL;
					$order->status = ($orderApi['status']!= null)?$orderApi['status']:$orderApi['shippings']['0']['status'];
					$order->invoice_key = isset($orderApi['shippings']['invoice']['access_key'])?$orderApi['shippings']['0']['invoice']['access_key']:NULL;
					$order->customer_name = isset($orderApi['shippings']['0']['customer']['customer_name'])?$orderApi['shippings']['0']['customer']['customer_name']:NULL;
					$order->customer = isset($orderApi['shippings']['0']['customer'])?$orderApi['shippings']['0']['customer']:NULL;
					$order->devolution_items = isset($orderApi['shippings']['0']['devolution_items'])?$orderApi['shippings']['0']['devolution_items']:NULL;
					$order->total_gross = isset($orderApi['total_gross'])?$orderApi['total_gross']:NULL;
					$order->total_net = isset($orderApi['total_net'])?$orderApi['total_net']:NULL;
					$order->total_freight = isset($orderApi['total_freight'])?$orderApi['total_freight']:NULL;
					$order->total_commission = isset($orderApi['total_commission'])?$orderApi['total_commission']:NULL;
					$order->total_discount = isset($orderApi['total_discount'])?$orderApi['total_discount']:NULL;
					$order->products = isset($orderApi['shippings']['0']['items'])?$orderApi['shippings']['0']['items']:NULL;
					$order->shipping = isset($orderApi['shippings']['0'])?$orderApi['shippings']['0']:NULL;
					// order->payment_methods = isset($response['body']->paymentMethods)?$response['body']->paymentMethods:NULL;
					$result = $order->update();
				} catch (Exception $e) {
					logMessageNS($e);
					$error[] = $e->getMessage();
				}
				if($result){
					$success = array('Pedido '.$idOrderNetshoes.' atualizado com sucesso.');
					$this->context->cookie->{$this->prefix.'success'} = json_encode($success);
				}else{
					$errorDb = Db::getInstance()->getMsgError();
					if($errorDb){
						$error[] = $errorDb;
					}
				}
			}
			if(count($error)>0){
				$error = (json_encode($error));
				logMessageNS($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
			}
		    ob_end_clean();
			Tools::redirectAdmin(self::$currentIndex.'&id_order='.Tools::getValue('id_order').'&viewnetshoesgroup_order&token='.$this->token);
    		die();
		}
		if(Tools::getIsset('sendInvoice')){
    		$order = $this->loadObject(true);
    		$shipping_code = ($order->shipping->shipping_code != NULL)?$order->shipping->shipping_code:$order->shipping_code;
			$error =array();
			$issueDate = new DateTime(Tools::getValue('issueDate'));
			$issueDate = $issueDate->format(DateTime::ISO8601);

			$body = array(
				"danfeXml" => Tools::getValue('danfeXml'),
				"issueDate"=> $issueDate,
				"key" => Tools::getValue('key'),
				"line"=> Tools::getValue('line'),
		      		"number"=> Tools::getValue('number'),
		      		"status" => 'Invoiced'
			);

			logMessageNS($body);
			try {
				$response = $this->ordersApi->updateShippingStatus($order->id_netshoes, $shipping_code, $body, 'invoiced');
			} catch (ApiException $e) {
				logMessageNS($e);
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if(empty($error) && !empty($response)){
				try{
					$order->invoiced = new stdClass();
					$order->invoiced->number = $body['number'];
					$order->invoiced->line = $body['line'];
					$order->invoiced->issueDate = $body['issueDate'];
					$order->invoiced->key = $body['key'];
					$order->status = 'Invoiced';
					$result = $order->update();
				} catch (Exception $e) {
					logMessageNS($e);
					$error[] = $e->getMessage();
				}
				$dataInsert = array(
					"id_netshoes" => $order->id_netshoes,
					"date_invoiced" => Tools::getValue('issueDate'),
					"number" => $body['number'],
					"line" => $body['line'],
					"key" => $body['key'],
					"danfeXml" => "danfeXml"
				);
				
				if(Db::getInstance()->getValue('SELECT id_order_update FROM '._DB_PREFIX_.'netshoesgroup_order_updates WHERE id_netshoes = "'.$order->id_netshoes.'"')){
					$insert = Db::getInstance()->update('netshoesgroup_order_updates',$dataInsert,'id_netshoes = "'.$order->id_netshoes.'"');
					logMessageNS('Atualizar Status (Invoiced)'. $order->id_netshoes . ' (update):'. $insert);
				}else{
					$insert = Db::getInstance()->insert('netshoesgroup_order_updates',$dataInsert);
					logMessageNS('Inserir Status (Invoiced)'. $order->id_netshoes . ' (insert):' .$insert);		
				}
				if($result){
					$success = array('Pedido '.$order->id_netshoes.' atualizado com sucesso.');
					$this->confirmations = $success;
					$this->context->cookie->{$this->prefix.'success'} = json_encode($success);
				}else{
					$errorDb = Db::getInstance()->getMsgError();
					if($errorDb){
						$error[] = $errorDb;
					}
				}
			}
			if(count($error)>0){
				$error = (json_encode($error));
				logMessageNS($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
			}
		    ob_end_clean();
			Tools::redirectAdmin(self::$currentIndex.'&id_order='.Tools::getValue('id_order').'&viewnetshoesgroup_order&token='.$this->token);
    		die();
		}
		if(Tools::getIsset('sendShipped')){
			$order = $this->loadObject(true);
			$shipping_code = ($order->shipping->shipping_code != NULL)?$order->shipping->shipping_code:$order->shipping_code;		
			$error =array();
			$deliveredCarrierDate = new DateTime(Tools::getValue('deliveredCarrierDate'));
			$deliveredCarrierDate = $deliveredCarrierDate->format(DateTime::ISO8601);
			$estimatedDelivery = new DateTime(Tools::getValue('estimatedDelivery'));
			$estimatedDelivery = $estimatedDelivery->format(DateTime::ISO8601);			
			$body = array(
				"carrier" => Tools::getValue('carrierName'),
				"deliveredCarrierDate"=> $deliveredCarrierDate,
				"estimatedDelivery"=> $estimatedDelivery,
				"status" => 'Shipped',
				"trackingNumber"=> Tools::getValue('trackingProtocol'),
		      		"trackingLink" => Tools::getValue('trackingUrl'),
			);
			logMessageNS($body);
			try {
				$response = $this->ordersApi->updateShippingStatus($order->id_netshoes, $shipping_code, $body, 'shipped');
			} catch (ApiException $e) {
				logMessageNS($e);
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if(empty($error) && !empty($response)){
				try{
					$order->shipping_info = new stdClass();
					$order->shipping_info->number = $body['trackingNumber'];
					$order->shipping_info->estimatedDelivery = $body['estimatedDelivery'];
					$order->shipping_info->deliveredCarrierDate = $body['deliveredCarrierDate'];
					$order->shipping_info->carrierName = $body['carrier'];
					$order->shipping_info->trackingUrl = $body['trackingLink'];
					$order->status = 'Shipped';
					$result = $order->update();
				} catch (Exception $e) {
					logMessageNS($e);
					$error[] = $e->getMessage();
				}
				$dataInsert = array(
					"id_netshoes" => $order->id_netshoes,
					"date_shipped" => Tools::getValue('deliveredCarrierDate'),
					"date_estimatedDelivery" => Tools::getValue('estimatedDelivery'),
					"carrier" => $body['carrier'],
					"trackingLink" => $body['trackingLink'],
					"trackingNumber" => $body['trackingNumber']
				);
				
				if(Db::getInstance()->getValue('SELECT id_order_update FROM '._DB_PREFIX_.'netshoesgroup_order_updates WHERE id_netshoes = "'.$order->id_netshoes.'"')){
					$insert = Db::getInstance()->update('netshoesgroup_order_updates',$dataInsert,'id_netshoes = "'.$order->id_netshoes.'"');
					logMessageNS('Atualizar Status (Shipped)'. $order->id_netshoes . ' (update):'. $insert);
				}else{
					$insert = Db::getInstance()->insert('netshoesgroup_order_updates',$dataInsert);
					logMessageNS('Inserir Status (Shipped) '. $order->id_netshoes . ' (insert):' .$insert);		
				}
				if($result){
					$success = array('Pedido '.$order->id_netshoes.' atualizado com sucesso.');
					$this->confirmations = $success;
					$this->context->cookie->{$this->prefix.'success'} = json_encode($success);
				}else{
					$errorDb = Db::getInstance()->getMsgError();
					if($errorDb){
						$error[] = $errorDb;
					}
				}
			}
			if(count($error)>0){
				$error = (json_encode($error));
				logMessageNS($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
			}
		    ob_end_clean();
			Tools::redirectAdmin(self::$currentIndex.'&id_order='.Tools::getValue('id_order').'&viewnetshoesgroup_order&token='.$this->token);
    		die();
		}
		if(Tools::getIsset('Delivered')){
			$order = $this->loadObject(true);
//			$shipping_code = ($order->shipping_code)?$order->shipping_code:$order->shipping->transport->tracking_number;
            $shipping_code = ($order->shipping->shipping_code != NULL)?$order->shipping->shipping_code:$order->shipping_code;
            $error =array();
			$deliveryDate = new DateTime();
			$deliveryDate->modify('-5 minutes');
			$deliveryDate = $deliveryDate->format(DateTime::ISO8601);

			$body = array(
				"deliveryDate"=> $deliveryDate,
				"status" => 'Delivered',
			);
			logMessageNS($body);
			try {
				$response = $this->ordersApi->updateShippingStatus($order->id_netshoes, $shipping_code, $body, 'delivered');
				//logMessageNS(debug($response));
			} catch (ApiException $e) {
				logMessageNS($e);
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if(empty($error) && !empty($response)){
				try{
					$order->status = 'Delivered';
					$result = $order->update();
				} catch (Exception $e) {
					$error[] = $e->getMessage();
				}
				$dataInsert = array(
					"id_netshoes" => $order->id_netshoes,
					"date_delivered" => date('Y-m-d H:i:s')
				);
				
				if(Db::getInstance()->getValue('SELECT id_order_update FROM '._DB_PREFIX_.'netshoesgroup_order_updates WHERE id_netshoes = "'.$order->id_netshoes.'"')){
					$insert = Db::getInstance()->update('netshoesgroup_order_updates',$dataInsert,'id_netshoes = "'.$order->id_netshoes.'"');
					logMessageNS('Atualizar Status (Delivered)'. $order->id_netshoes . ' (update):'. $insert);
				}else{
					$insert = Db::getInstance()->insert('netshoesgroup_order_updates',$dataInsert);
					logMessageNS('Inserir Status (Delivered) '. $order->id_netshoes . ' (insert):' .$insert);		
				}
				if($result){
					$success = array('Pedido '.$order->id_netshoes.' atualizado com sucesso.');
					$this->confirmations = $success;
					$this->context->cookie->{$this->prefix.'success'} = json_encode($success);
				}else{
					$errorDb = Db::getInstance()->getMsgError();
					if($errorDb){
						$error[] = $errorDb;
					}
				}
			}
			if(count($error)>0){
				$error = (json_encode($error));
				logMessageNS($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
			}
		    ob_end_clean();
			Tools::redirectAdmin(self::$currentIndex.'&id_order='.Tools::getValue('id_order').'&viewnetshoesgroup_order&token='.$this->token);
    		die();
		}
		$this->content .= parent::postProcess();
    }

	
}
