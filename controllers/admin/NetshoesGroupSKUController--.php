<?php
use ApiMktpNetshoesV1\product as product;
use ApiMktpNetshoesV1\sku as sku;

if (!defined('_PS_VERSION_'))
	exit;

require_once dirname(__FILE__).'/NetshoesGroupDefaultController.php';
require_once dirname(dirname(dirname(__FILE__))).'/classes/netshoesgroupSku.php';

class NetshoesGroupSKUController extends NetshoesGroupDefaultController {
	private $sku_situation_label = array(
		// 'INCLUDED' => 'SKU incluido.',
		'PENDING_MATCH' => 'Pendente de match.',
		'RELEASED_MATCH' => 'Liberado de match.',
		// 'PENDING_FIRST_PRICE_BLOCK' => 'Trava de primeiro preço.',
		// 'PENDING_PRICE_BLOCK_ANALYSIS' => 'Trava de variação de preço.',
		// 'PENDING_PRICE_BLOCK' => 'Trava de variação de preço.',
		// 'RELEASED_PRICE_BLOCK' => 'Liberado de trava de preço.',
		// 'PENDING_EXCLUSIVITY_ANALYSIS' => 'Pendência de exclusividade.',
		// 'RELEASED_EXCLUSIVITY' => 'Liberação de pendência de exclusividade.',
		// 'INACTIVE' => 'Inativo.',
		// 'REACTIVATED' => 'Reativado.',
		// 'EXCLUDED' => 'Excluido',
	);
	private $sku_situation_label_color = array(
		// 'INCLUDED' => '#72c279',
		'PENDING_MATCH' => '#F0AD4E',
		'RELEASED_MATCH' => '#5BC0DE',
		// 'PENDING_FIRST_PRICE_BLOCK' => '#F0AD4E',
		// 'PENDING_PRICE_BLOCK_ANALYSIS' => '#F0AD4E',
		// 'PENDING_PRICE_BLOCK' => '#F0AD4E',
		// 'RELEASED_PRICE_BLOCK' => '#5BC0DE',
		// 'PENDING_EXCLUSIVITY_ANALYSIS' => '#777777',
		// 'RELEASED_EXCLUSIVITY' => '#5BC0DE',
		// 'INACTIVE' => '#e08f95',
		// 'REACTIVATED' => '#72c279',
		// 'EXCLUDED' => '#e08f95',
	);
	
	public function __construct()
	{
		$this->controllerName = 'NetshoesGroupSKU';
		$this->table = 'netshoesgroup_sku';
		$this->className = 'netshoesgroupSku';	
		parent::__construct();
		$this->skuApi = new sku\SkusApi($this->netshoesGroup);
		$this->productApi = new product\ProductsApi($this->netshoesGroup);
		// $this->confirmations[] = "Cadastrado com sucesso!";
		// $this->errors[] = "Oops! Algo deu errado. Por favor, tente novamente.";
		// $this->informations[] = "Oops! Algo deu errado. Por favor, tente novamente.";
		// $this->warnings[] = "Oops! Algo deu errado. Por favor, tente novamente.";
	}
	
	public function initProcess()
	{
	    $this->action = (Tools::getValue('action'))?Tools::getValue('action'):'list';
	    switch ($this->action) {
			case 'list':
				$this->displayList();
				break;
			case 'updatedStatus':
				$this->updatedStatus();
				break;
			case 'connect':
				$this->connectSKU();
				break;
			case 'disconnect':
				$this->disconnectSKU();
				break;
			default:
				$this->errors[] = $this->l('Oops! Página não encontrada.');
				$this->displayList();
				break;
		}
		
	    parent::initProcess();
	}
	
	 public function getList($id_lang, $orderBy = null, $orderWay = null, $start = 0, $limit = null, $id_lang_shop = null)
    {
        parent::getList($id_lang, $orderBy, $orderWay, $start, $limit, $id_lang_shop);
        /* update product quantity with attributes ...*/
        $nb = count($this->_list);
        if ($this->_list) {
        	for ($i = 0; $i < $nb; $i++) {
        		 $this->_list[$i]['situation_color'] = isset($this->sku_situation_label_color[$this->_list[$i]['situation']])?$this->sku_situation_label_color[$this->_list[$i]['situation']]:NULL;
        		 $this->_list[$i]['situation'] = isset($this->sku_situation_label[$this->_list[$i]['situation']])?$this->sku_situation_label[$this->_list[$i]['situation']]:NULL;
			}
		}
	}
	
	private function displayList(){
		$this->_pagination = array(20, 50,100,300);
	    $this->identifier = 'id_sku';
        $this->lang = false;
		$this->addRowAction('edit');
		$this->addRowAction('synchronize');
		$this->_select .= 'a.id_sku, a.id_product';
        $this->_orderBy = 'a.name';
        $this->_orderWay = 'ASC';
		$this->tpl_list_vars['toolbar_btn'] = FALSE;
		
		$this->fields_list = array();
		$this->fields_list['sku'] = array(
			'title' => $this->l('ID'),
			'align' => 'center',
			'width' => 'auto',
			'filter_key' => 'a!sku',
		);
		$this->fields_list['name'] = array(
			'title' => $this->l('Name'),
			'width' => 'auto',
			'filter_key' => 'a!name',
		);
		$this->fields_list['price'] = array(
			'title' => $this->l('Price'),
			'align' => 'text-right',
			'orderby' => false,
			'filter' => false,
			'search' => false,
			'filter_key' => 'a!price',
            'callback' => 'printPrice'
		);
		$this->fields_list['stockQuantity'] = array(
			'title' => $this->l('Stock'),
			'type' => 'int',
			'align' => 'text-right',
			'filter_key' => 'a!stockQuantity',
		);
		$this->fields_list['associate'] = array(
                'title' => $this->l('Associado'),
                'align' => 'text-center',
                'type' => 'bool',
                'orderby' => true,
				'filter' => false,
				'search' => false,
				'filter_key' => 'a!id_product',
                'callback' => 'printAssociate'
            );
		$this->fields_list['situation'] = array(
			'title' => $this->l('Status'),
			'type' => 'select',
            'color' => 'situation_color',
			'align' => 'text-right',
            'list' => $this->sku_situation_label,
			'filter_key' => 'a!situation',
			'orderby' => false,
			'filter' => false,
			'search' => false
		);
		$this->fields_list['business_id'] = array(
            'title' => $this->l('Matketplace'),
            'align' => 'text-center',
            'orderby' => true,
			'filter' => false,
			'search' => false,
			'filter_key' => 'a!business_id',
            'callback' => 'printBusiness'
        );
		$this->fields_list['enable'] = array(
			'title' => $this->l('Ativo'),
			'type' => 'bool',
			'active' => 'status',
			'align' => 'text-center',
			'type' => 'bool',
			'class' => 'fixed-width-sm',
			'filter_key' => 'a!enable',
		);
		return;
	}

	private function getParamsSearch(){
		if(Tools::isSubmit('submitReset'.$this->controllerName)){
			$this->context->cookie->{$this->controllerName.'_pagination'} = 20;
			Tools::redirectAdmin(AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite($this->controllerName));
			die();
		}else{
			if(Tools::getValue($this->controllerName.'_pagination')){
				$this->context->cookie->{$this->controllerName.'_pagination'} = Tools::getValue($this->controllerName.'_pagination');
			}
		}
		$maxResult = $this->context->cookie->{$this->controllerName.'_pagination'};
        $page = (int)Tools::getValue('submitFilter'.$this->controllerName);
		if (!$page)
			$page = 1;
		return array(
        	'limit'=>((int)$maxResult>0?(int)$maxResult:20),
			'page'=>$start,
    	);		
	}

	public function printAssociate($id_order, $tr)
    {
        return ($tr['associate'] ? $this->l('Sim') : $this->l('Não'));
    }

    public function printBusiness($id_order, $tr)
    {
    	if ($tr['business_id'] == 'NS') {
    		return $this->l('Netshoes');
    	} elseif ($tr['business_id'] == 'ZT') {
    		return $this->l('Zattini');
    	} else {
    		return $this->l('Inativo em ambos');
    	}
    }

 	public function printPrice($value, $tr)
    {
    	$value = Tools::jsonDecode($value);
    	return '<span style="text-decoration:line-through;color:#999">De: R$ '.Tools::displayPrice(isset($value->listPrice)?$value->listPrice:$value).' </span>
		<br>
		<span>Por: R$ '.Tools::displayPrice(isset($value->sellPrice)?$value->sellPrice:$value).' </span>';
    }
	public function displayEnableLink($token, $id, $value, $active, $id_category = null, $id_product = null, $ajax = false)
	{
		if ($value == '1' ){
			$onClick = "onclick=\"return confirm('Deseja realmente finalizar anúncio? O mesmo só poderá ser reativado pelo departamento comercial do Grupo Netshoes.')\" ";
			$html = '<div class="btn-group">
			<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false" data-value="'.$value.'"><i class="icon-check"></i> </button>
			<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><span class="icon-caret-down"></span></button>
			<ul class="dropdown-menu" role="menu">
			 	<li><a '.$onClick.' href="'. self::$currentIndex.'&token='.$this->token.'&action=updatedStatus&id='.$id.'&status=0">Finalizar</a></li>
			</ul>';
		}else{$html = '<button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false" data-value="'.$value.'" ><i class="icon-remove"></i> </button>';
		}
		return $html;
	}
	
	public function displaySynchronizeLink($token, $id){
		$href = self::$currentIndex.'&token='.$this->token.'&id_sku='.$id.'&synchronize=1';
	    $html = '<a href="'.$href.'" title="Sincronizar" class="delete">
					<i class="icon-update"></i>  Sincronizar
				</a>';
		return $html;
	}

	protected function updatedStatus()
	{
		$idSku = Tools::getValue('id');
		$sku = new netshoesgroupSku($idSku);
		$bu_id = $sku->business_id;
		$body = array("active" => (Tools::getValue('status')==1?true:false));
		$error = array();
		try {
			$response = $this->skuApi->updateStatus($sku->sku, $bu_id, $body);
		} catch (ApiException $e) {
			$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
		}

		if(empty($error) && !empty($response) ) {
			{$success = array('Status do anúncio '.$idSku.' ('.$bu_id.') atualizado com sucesso.');
			$this->context->cookie->{$this->prefix.'success'} = json_encode($success);}
		}else{
			logMessage($response);
			logMessage(array($idSku,$body));
			$error = (json_encode($error));
			logMessage($error);
			$this->context->cookie->{$this->prefix.'error'} = $error;
		}
		$sku->enable = (Tools::getValue('status')==1?true:false);
		$sku->update();
		Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
		die();
	}
	
	public function connectSKU()
	{
		$idSku = Tools::getValue('id_sku');
		if (Tools::isSubmit('submitConnect')){
			$idSku = preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($idSku));
			$sql = "SELECT COUNT(*) FROM "._DB_PREFIX_."netshoesgroup_relation WHERE id_sku = '".$idSku."'";
    		$select = Db::getInstance()->getValue($sql);
			$association = false;
			$data = array(
				'id_product'=>(int) Tools::getValue('id_product'),
				'id_attribute'=>(int) Tools::getValue('id_product'),
				'id_sku'=> $idSku,
				'variation_price'=> tofloat(Tools::getValue('tax-type') == 'c'? 0 :Tools::getValue('tax')),
				'variation_priority'=>(Tools::getValue('tax-type') == 'c'?'c':'p'),
				'date_upd'=>date('Y-m-d H:i:s'),
			);
			if($select > 0){
				$dbResult = Db::getInstance()->update('netshoesgroup_relation', $data,'id_sku ="'.$idSku.'"');
			}else{
				$data['date_add'] = date('Y-m-d H:i:s');
				$dbResult = Db::getInstance()->insert('netshoesgroup_relation', $data);
			}
			$error = array();
			if(!$dbResult){
				$error[] = 'Erro ao salvar associação.';
				$error = (json_encode($error));
				logMessage($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
				$href = self::$currentIndex.'&token='.$this->token.'&id_sku='.$id.'&action=disconnect';
				Tools::redirectAdmin($href);
				die();
			}
			$bodyPrice = array(
				"sellPrice"=> tofloat(Tools::getValue('sellPrice')),
				"listPrice"=> tofloat((Tools::getValue('sellPrice')>Tools::getValue('listPrice')?Tools::getValue('sellPrice'):Tools::getValue('listPrice'))),
			);
		 	try {
				$response = $this->skuApi->updatePrice($idSku, $bodyPrice);
			} catch (ApiException $e) {
				$error['price'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if ( empty($response) || !empty($error['price']) ){
				$error[] = 'Erro ao atualizar o preço, tente novamente mais tarde.';
				logMessage($response);
				logMessage(array($idSku,$bodyPrice));
			}
			$stockQuantity =  Tools::getValue('stockQuantity');
			$bodyStock = array("country"=> 'BR', "available"=>$stockQuantity);
			try {
				$response = $this->skuApi->updateStock($idSku, $bodyStock);
			} catch (ApiException $e) {
				$error['stock'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if ( empty($response) || !empty($error['stock']) ){
				$error[] = 'Erro ao atualizar o estoque, tente novamente mais tarde.';
				logMessage($response);
				logMessage(array($idSku,$bodyStock));
			}
			$sku = new netshoesgroupSku($idSku);
			$sku->stockVariation = $stockQuantity - StockAvailable::getQuantityAvailableByProduct(Tools::getValue('id_product'), Tools::getValue('id_product'));
			$sku->update();
			if(count($error)>0){
				$error = (json_encode($error));
				logMessage($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
			}
			Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
			die();
		}else{
			$id_netshoes = Tools::getValue('id_netshoes');
			try {
				$response = $this->skuApi->getProductSku($id_netshoes, $idSku, "");
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			
			if ( empty($response) || !empty($error) ){
				logMessage($response);
				logMessage(array($idSku,$id_netshoes));
				$error = (json_encode($error));
				logMessage($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
				Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
				die();
			}
			$this->addJqueryPlugin('autocomplete');
			$tpl = $this->context->smarty->createTemplate($this->getTemplatePath().'sku_connect.tpl');
			$tpl->assign('product', array(
				'id'=>$idSku,
				'name'=>$response['name'],
				'ean'=>isset($response['ean_isbn'])?$response['ean_isbn']:null)
			);
			$tpl->assign('urlAjaxGetProductPS', Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->moduleName.'/ajaxGetProductPS.php');
			$tpl->assign('urlAjaxGetProductDataPS', Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->moduleName.'/ajaxGetProductDataPS.php');
			$this->content .= $tpl->fetch();
		}
		return true;
	}

	public function disconnectSKU($value='')
	{
		$idSku = preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim(Tools::getValue('id_sku')));
		$delete = Db::getInstance()->delete('netshoesgroup_relation', 'id_sku ="'.$idSku.'"');
		if (Db::getInstance()->Affected_Rows()){
			$success = array('Associação do anúncio '.$idSku.' removida com sucesso.');
			$this->context->cookie->{$this->prefix.'success'} = json_encode($success);
		}else{
			$error = array('Erro ao remover associação.');
			$error = (json_encode($error));
			logMessage($error);
			$this->context->cookie->{$this->prefix.'error'} = $error;
		}
		Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);
		die();
	}

	public function renderForm()
    {
    	$sku = $this->loadObject(true);
        $readonly = ($sku->id_product > 0);
		$this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Anúncio'),
                'icon' => 'icon-tags'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Nome'),
                    'name' => 'name',
                    'col'=> 5,
                    'lang' => false,
                    'readonly' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Referência'),
                    'desc' => '<div class="alert alert-warning">'.$this->l('Somente altere se ouve uma alteração da referência dentro do sistema da Netshoes, a API não libera a alteração dessa referencia.').'</div>',
                    'name' => 'sku',
                    'col'=> 5,
                    'lang' => false,
                    'required' => true,
                    'readonly' => $readonly,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Preço de'),
                    'name' => 'price[listPrice]',
                    'col'=> 5,
                    'lang' => false,
                    'required' => true,
                    'prefix'=>'R$',
                    'readonly' => $readonly,
				),
                array(
                    'type' => 'text',
                    'label' => $this->l('Preço por'),
                    'name' => 'price[sellPrice]',
                    'id' => 'price-sell-price',
                    'col'=> 5,
                    'lang' => false,
                    'required' => true,
                    'prefix'=>'R$',
                    'readonly' => $readonly,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Quantidade'),
                    'name' => 'stockQuantity',
                    'col'=> 5,
                    'lang' => false,
                    'required' => true,
                    'readonly' => $readonly,
                ),
                array(
					 'type' => 'text',
                    'label' => $this->l('Marketplace'),
                    'name' => 'business_id',
                    'col'=> 5,
                    'lang' => false,
                    'required' => true,
                    'readonly' => $readonly,
				),
				array(
					'type' => 'switch',
					'label' => $this->l('Ativo'),
					'name' => 'enable',
					'desc' => $this->l('O anúncio só poderá ser reativado pelo departamento comercial da Netshoes.'),
					'is_bool' => true,
					'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
						),
						array(
							'id' => 'active_off',
							'value' => 0,
						)
					),
				),
            ),
			'submit' => array(
				'title' => $this->l('Save'),
				'name' => 'sendSku',
			),
			'buttons'=>array(
				array(
					'type' => 'submit',
					'name' => 'synchronize',
					'title' => $this->l('Sincronizar'),
					'icon' => 'process-icon-update',
					'class' => 'pull-right',
					'value' => 1
				)
			)
        );
		
		$this->fields_value['price[listPrice]'] = number_format(isset($sku->price->listPrice)?$sku->price->listPrice:$sku->price,2,'.',',');
		$this->fields_value['price[sellPrice]'] = number_format(isset($sku->price->sellPrice)?$sku->price->sellPrice:$sku->price,2,'.',',');
		$this->fields_value['business_id'] = ($sku->business_id == 'NS')?'Netshoes':'Zattini';
        $this->content .= parent::renderForm();

		$this->addJqueryPlugin('autocomplete');
		$tpl = $this->context->smarty->createTemplate($this->getTemplatePath().'sku_form_connect.tpl');
		$tpl->assign('product', array(
			'id'=>$sku->id,
			'name'=>$sku->name,
			'ean'=>((is_array($sku->eanIsbn) && count($sku->eanIsbn) > 0 ) ? $sku->eanIsbn[0] : $sku->eanIsbn),
			'id_product' => (int)$sku->id_product,
			'id_attribute' => (int)$sku->id_attribute,
			'variation_price' => $sku->variation_price,
			'variation_priority' => $sku->variation_priority,
			'price_impact' => $sku->price_impact,
			
		));
		$tpl->assign('urlAjaxGetProductPS', __PS_BASE_URI__.'modules/'.$this->moduleName.'/ajaxGetProductPS.php');
		$tpl->assign('urlAjaxGetProductDataPS', __PS_BASE_URI__.'modules/'.$this->moduleName.'/ajaxGetProductDataPS.php');
		$this->content .= $tpl->fetch();
		
		return;
	}

    public function postProcess()
    {
    	if(Tools::getIsset('synchronize')){
			$sku = $this->loadObject(true);
    		$idSku = (Tools::getValue('sku'))?Tools::getValue('sku'):$sku->sku;
    		$bu_id = (Tools::getValue('business_id'))?Tools::getValue('business_id'):$sku->business_id;
    		$id_netshoes = (Tools::getValue('id_netshoes'))?Tools::getValue('id_netshoes'):$sku->id_netshoes;
    		$error = array();

			try {
				$product = $this->productApi->getProduct($id_netshoes, NULL);
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}

			try {
				$response = $this->skuApi->getProductSku($id_netshoes, $idSku, NULL);
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}

			try {
				$stock = $this->skuApi->getStock($idSku);
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}

			try {
				$prices = $this->skuApi->listPrices($idSku);
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			try {
				$status = $this->skuApi->getStatus($idSku, $bu_id);
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			
			if (  !empty($error)  ){
				logMessage($error);
			}else{
				try{
					$sku->sku = isset($response['sku'])?$response['sku']:NULL;
					$sku->color = ( isset($response['color']) && $response['flavor'] == "")?$response['color']:NULL;
					$sku->flavor = ( isset($response['flavor']) && $response['color'] == "")?$response['flavor']:NULL;
					$sku->gender = isset($response['gender'])?$response['gender']:NULL;
					$sku->brand = isset($product['brand'])?$product['brand']:NULL;
					$sku->product_type = isset($product['product_type'])?$product['product_type']:NULL;
					$sku->department = isset($product['department'])?$product['department']:NULL;
					$sku->size = isset($response['size'])?$response['size']:NULL;
					$sku->name = isset($response['name'])?$response['name']:NULL;
					$sku->description = isset($response['description'])?$response['description']:NULL;
					$sku->eanIsbn = isset($response['ean_isbn'])?$response['ean_isbn']:NULL;
					$sku->height = isset($response['height'])?$response['height']:NULL;
					$sku->width = isset($response['width'])?$response['width']:NULL;
					$sku->depth = isset($response['depth'])?$response['depth']:NULL;
					$sku->weight = isset($response['weight'])?$response['weight']:NULL;
					$sku->stockQuantity = isset($stock['available'])?$stock['available']:NULL;
					$sku->enable = isset($status['active'])?$status['active']:NULL;
					$sku->price->sellPrice = isset($prices['price'])?$prices['price']:NULL;
					$sku->images = isset($response['images'])?$response['images']:NULL;
					$sku->id_netshoes = $id_netshoes;
					$sku->link = isset($response['links']['href'])?$response['links']['href']:NULL;
					//$sku->situation = isset($response['body']->situation)?$response['body']->situation:NULL;
					$result = $sku->update();
				} catch (Exception $e) {
					$error[] = $e->getMessage();
				}
				if($result){
					$success = array('Anúncio '.$idSku.' ('.$bu_id.') atualizado com sucesso.');
					$this->context->cookie->{$this->prefix.'success'} = json_encode($success);
				}else{
					$errorDb = Db::getInstance()->getMsgError();
					if($errorDb){
						$error[] = $errorDb;
					}
				}
			}
			if(count($error)>0){
				$error = (json_encode($error));
				logMessage($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
			}
		    ob_end_clean();
			Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&id_sku='.Tools::getValue('id_sku').'&updatenetshoesgroup_sku');
    		die();
		}
			
		if(Tools::isSubmit('sendSku')){
			$sku = $this->loadObject(true);
			$pricePost = Tools::getValue('price');
			$pricePost['listPrice'] = tofloat(($pricePost['sellPrice'] > $pricePost['listPrice'])?$pricePost['sellPrice']:$pricePost['listPrice']);
			$pricePost['sellPrice'] = tofloat($pricePost['sellPrice']);
			$idSku = preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim(Tools::getValue('sku')));
			$bu_id = Tools::getValue('business_id', $sku->business_id);
			
			$error = array();
			
			$bodyPrice = array(
				"sellPrice"=> $pricePost['sellPrice'],
				"listPrice"=> $pricePost['listPrice'],
			);
			//logMessage($bodyPrice);
			try {
				$response = $this->skuApi->updatePrice($idSku, $bodyPrice);
			} catch (ApiException $e) {
				$error['price'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			
			if ( empty($response) || !empty($error['price']) ){
				$error[] = 'Erro ao atualizar o preço, tente novamente mais tarde.';
				logMessage($response);
				logMessage(array($idSku,$bodyPrice));
			}
			$stockQuantity =  Tools::getValue('stockQuantity');
			$bodyStock = array("country"=> 'BR', "available"=>$stockQuantity);
			//logMessage($stockQuantity);
			try {
				$response = $this->skuApi->updateStock($idSku, $bodyStock);
			} catch (ApiException $e) {
				$error['stock'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if ( empty($response) || !empty($error['stock']) ){
				$error[] = 'Erro ao atualizar o estoque, tente novamente mais tarde.';
				logMessage($response);
				logMessage(array($idSku,$bodyStock));
			}
		
			$bodyStatus = array("active" => (Tools::getValue('enable',$sku->enable)?true:false));
			$error = array();
			try {
				$response = $this->skuApi->updateStatus($idSku, $bu_id, $bodyStatus);
			} catch (ApiException $e) {
				$error['status'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if ( empty($response) || !empty($error['stock']) ){
				$error['status'] = 'Erro ao atualizar o anúncio, tente novamente mais tarde.';
				logMessage($response);
				logMessage(array($idSku,$bu_id,$bodyStatus));
			}
			try{
				$sku->sku = Tools::getValue('sku',$sku->sku);
				$sku->business_id = Tools::getValue('business_id', $sku->business_id);
				$sku->price->listPrice = $pricePost['listPrice'];
				$sku->price->sellPrice = $pricePost['sellPrice'];
				$sku->stockQuantity = Tools::getValue('stockQuantity',$sku->stockQuantity);
				if($sku->id_product != 0 && $sku->id_attribute != 0) {
					$sku->stockVariation = $sku->stockQuantity - StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute);
				} elseif($sku->id_product != 0) {
					$sku->stockVariation = $sku->stockQuantity - StockAvailable::getQuantityAvailableByProduct($sku->id_product);
				}
				$sku->enable = Tools::getValue('enable',$sku->enable);
				$result = $sku->update();
				//logMessage($sku);
			} catch (Exception $e) {
				$error[] = $e->getMessage();
			}
			if($result){
				$success = array('Anúncio '.$idSku.' ('.$bu_id.') atualizado com sucesso.');
				$this->context->cookie->{$this->prefix.'success'} = json_encode($success);
			}else{
				$errorDb = Db::getInstance()->getMsgError();
				if($errorDb){
					$error[] = $errorDb;
				}
			}
			
			if(count($error)>0){
				$error = (json_encode($error));
				logMessage($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
			}
		    ob_end_clean();
			Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&id_sku='.Tools::getValue('id_sku').'&updatenetshoesgroup_sku');
    		die();
		}
		
		if (Tools::isSubmit('submitConnectSKU')){
			$sku = $this->loadObject(true);
			$idSku = $sku->sku;
			$pricePost = array();
			$pricePost['listPrice'] = tofloat((Tools::getValue('sellPrice') > Tools::getValue('listPrice'))?Tools::getValue('sellPrice'):Tools::getValue('listPrice'));
			$pricePost['sellPrice'] = tofloat(Tools::getValue('sellPrice'));
			
			$error = array();
			
			$dataPrice = array(
				"sellPrice"=> $pricePost['sellPrice'],
				"listPrice"=> $pricePost['listPrice'],
			);
			try {
				$response = $this->skuApi->updatePrice($idSku, $dataPrice);
			} catch (ApiException $e) {
				$error['price'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if ( empty($response) || !empty($error['price']) ){
				$error[] = 'Erro ao atualizar o preço, tente novamente mais tarde.';
				logMessage($response);
				logMessage(array($idSku,$bodyPrice));
			}

			$bodyStock = array("country"=> 'BR', "available"=> Tools::getValue('stockquantity'));
			try {
				$response = $this->skuApi->updateStock($idSku, $bodyStock);
			} catch (ApiException $e) {
				$error['stock'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if ( empty($response) || !empty($error['stock']) ){
				$error[] = 'Erro ao atualizar o estoque, tente novamente mais tarde.';
				logMessage($response);
				logMessage(array($idSku,$bodyStock));
			}
			
			try{
				$sku->price->listPrice = $pricePost['listPrice'];
				$sku->price->sellPrice = $pricePost['sellPrice'];
				$sku->stockQuantity = Tools::getValue('stockquantity',$sku->stockQuantity);
				
				$sku->id_product = Tools::getValue('id_product',$sku->id_product);
				$sku->id_attribute = Tools::getValue('id_attribute',$sku->id_attribute);
				if($sku->id_product != 0 && $sku->id_attribute != 0) {
					$sku->stockVariation = $sku->stockQuantity - StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute);
				} elseif($sku->id_product != 0) {
					$sku->stockVariation = $sku->stockQuantity - StockAvailable::getQuantityAvailableByProduct($sku->id_product);
				}
				$sku->variation_priority = Tools::getValue('tax-type');
				$sku->variation_price = (Tools::getValue('tax-type')== 'c')?0:Tools::getValue('tax');
				$sku->price_impact = Tools::getValue('price-impact');
				$result = $sku->update();
			} catch (Exception $e) {
				$error[] = $e->getMessage();
			}
			if($result){
				$success = array('Anúncio '.$idSku.' atualizado com sucesso.');
				$this->context->cookie->{$this->prefix.'success'} = json_encode($success);
			}else{
				$errorDb = Db::getInstance()->getMsgError();
				if($errorDb){
					$error[] = $errorDb;
				}
			}
			
			if(count($error)>0){
				$error = (json_encode($error));
				logMessage($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
			}
		    ob_end_clean();
			Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&id_sku='.Tools::getValue('id_sku').'&updatenetshoesgroup_sku');
    		die();
		}

		if(Tools::isSubmit('submitRemoveRelation')){
			$sku = $this->loadObject(true);
			$error = array();
			try{
				$sku->id_product = NULL;
				$sku->id_attribute = NULL;
				$sku->variation_priority = NULL;
				$sku->variation_price = NULL;
				$sku->stockVatiation = 0;
				$result = $sku->update();
			} catch (Exception $e) {
				$error[] = $e->getMessage();
			}
			if($result){
				$success = array('Associação do anúncio '.$idSku.' removido com sucesso.');
				$this->context->cookie->{$this->prefix.'success'} = json_encode($success);
			}else{
				$errorDb = Db::getInstance()->getMsgError();
				if($errorDb){
					$error[] = $errorDb;
				}
			}
			
			if(count($error)>0){
				$error = (json_encode($error));
				logMessage($error);
				$this->context->cookie->{$this->prefix.'error'} = $error;
			}
		    ob_end_clean();
			Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token.'&id_sku='.Tools::getValue('id_sku').'&updatenetshoesgroup_sku');
    		die();
		}
		return parent::postProcess();
    }

}
