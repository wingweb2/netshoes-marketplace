<?php
/*
 * Cron para verificar e excluir entradas duplicadas de pedido
 * dev-init: 18-07-17
 * Wing Agency
 */
require_once ('../../config/config.inc.php');
require_once ('../../init.php');
if (!defined('_PS_VERSION_')) {
    exit();
}
$sql =  "SELECT id_netshoes, COUNT(*) FROM " . _DB_PREFIX_ . "netshoesgroup_order GROUP BY id_netshoes HAVING COUNT(*) > 1 ORDER BY COUNT(*), id_netshoes DESC";

$orders_duplicates = Db::getInstance()->executeS($sql);

if(count($orders_duplicates) == 0)
{
    echo 'Nenhum pedido duplicado encontrado';
    die();
}

$all_ids_orders_duplicates = array();

foreach ($orders_duplicates as $key => $orders_duplicate)
{
    //var_dump($orders_duplicates);

    $sql = "SELECT id_order FROM " . _DB_PREFIX_ . "netshoesgroup_order WHERE id_netshoes = '" . $orders_duplicate['id_netshoes'] . "'";

    $orders = Db::getInstance()->executeS($sql);

    $all_ids_orders_duplicates[$orders_duplicate['id_netshoes']] = $orders;
}

foreach ($all_ids_orders_duplicates as $id_netshoes => &$orders)
{
    foreach ($orders as &$order)
    {
        $order['id_order'] = (int)$order['id_order'];
    }

}


//echo '<pre>';
//var_dump($all_ids_orders_duplicates);
//echo '</pre><br><br>';

foreach ($all_ids_orders_duplicates as $id_netshoes => $orders)
{
    echo '<pre>';
    var_dump($orders);
    echo '</pre>';

    if($orders[0]['id_order'] < $orders[1]['id_order'])
    {
        echo 'id_order ' . $orders[1]['id_order'];
        $order = new netshoesgroupOrder($orders[1]['id_order']);
        if($order->delete())
            echo ' excluido';
        else
            echo ' nao excluido';
        $order = null;
    }
    echo '<br>';
}
