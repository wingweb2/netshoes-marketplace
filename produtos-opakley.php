<?php

date_default_timezone_set('America/Sao_Paulo');

// The custom error handlers that ship with PHP Simple Daemon respect all PHP INI error settings.
ini_set('error_log', __DIR__.'/error_log');
ini_set('display_errors', E_ALL);
error_reporting(E_ALL);


use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\sku as sku;
use ApiMktpNetshoesV1\model\BusinessUnitResource as BusinessUnitResource;


require(dirname(__FILE__).'/../../config/config.inc.php');
require(dirname(__FILE__).'/../../init.php');
require dirname(__FILE__).'/includes/functions.php';
require dirname(__FILE__).'/classes/netshoesgroupOrder.php';
require dirname(__FILE__).'/classes/netshoesgroupSku.php';
require_once dirname(__FILE__).'/ApiMktpNetshoesV1.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');


$DB = Db::getInstance();

$sql = "SELECT ns.id_sku, ns.sku, ns.enable, p.id_product,  p.id_manufacturer FROM ps_netshoesgroup_sku AS ns
	JOIN ps_product AS p ON (ns.id_product = p.id_product)
	WHERE p.id_manufacturer = 15 ";

$results = $DB->ExecuteS($sql);

echo $sql."<br>\n";

if($results && is_array($results))
{

	echo "count: ". count($results)."<br>\n";


	$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
	$useSSL = true;
	client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
	client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
	client\Configuration::$apiClient = $netshoesGroup;
	client\Configuration::$debug = false;

	$skusApi = new sku\SkusApi($netshoesGroup);

	$business_unit_resource = new BusinessUnitResource();
	$business_unit_resource->active = FALSE;

	foreach ($results as &$row)
    {

        $sku = new netshoesgroupSku($row['id_sku']);
        if($sku->business_id == "Netshoes" || $sku->business_id == "NS")
        {
            $bu_id = "NS";
            $sku->business_id = $bu_id;
        }
        if($sku->business_id == "Zattini" || $sku->business_id == "ZT")
        {
            $bu_id = "ZT";
            $sku->business_id = $bu_id;
        }

        $row['getStatus'] = $skusApi->getStatus($sku->sku, $sku->business_id);

        // $row['update'] = $skusApi->updateStatus($row['sku'], $sku->business_id, $business_unit_resource);

        // if($row['update']) {
        	// $sku->enable = false;
        	// $sku->update();
        // }
    }
}
echo json_encode($results)."<br>\n";
echo debug($results)."<br>\n";