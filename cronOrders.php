<?php
use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\order as order;
require(dirname(__FILE__).'/../../config/config.inc.php');
require(dirname(__FILE__).'/../../init.php');
require dirname(__FILE__).'/includes/functions.php';
require dirname(__FILE__).'/classes/netshoesgroupOrder.php';
require dirname(__FILE__).'/classes/netshoesgroupSku.php';
require_once dirname(__FILE__).'/ApiMktpNetshoesV1.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
$useSSL = true;
client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
client\Configuration::$apiClient = $netshoesGroup;
client\Configuration::$debug = false;
$apiOrder = new order\OrdersApi($netshoesGroup);
$error = array();
$page = isset($_GET['page']) ? $_GET['page'] : 0;
$start_date = date('Y-m-d H:i:s', strtotime("-2 days", strtotime(date('Y-m-d H:i:s')) ));
$end_date = date('Y-m-d H:i:s');
$start_date = new Datetime($start_date);
$start_date = $start_date->format(DateTime::ISO8601);
$end_date = new DateTime($end_date);
$end_date = $end_date->format(DateTime::ISO8601);
$last = isset($_GET['last']) ? $_GET['last'] : '';
$orders = null;


echo $end_date;
echo $start_date;
if($page <= $last)
{
    try
    {
        $orders = $apiOrder->listOrders($page, 50, array("shippings", "items", "devolutionItems"), null, null, null, null);
    }
    catch (ApiException $e)
    {
        $return['error'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
    }
    if($page == 0)
    {
        $last = $orders->links[3]->href;
        $last = explode("?", $last);
        $last = explode("&", $last[1]);
        $last = explode("=", $last[0]);
        $last = (int)$last[1];
    }
//    logMessageNS($orders);
    foreach ($orders['items'] as $key => $order)
    {
        $dataInsert = array(
            'id_netshoes'=> $order['number'],
            'id_site'=> pSQL($order['origin_number']),
            'business_unit'=> pSQL(($order['business_unit']==1)?'NS':'ZT'),
            'devolution_requested'=> pSQL($order['devolution_requested']),
            'exchange_requested'=> pSQL($order['exchange_requested']),
            'order_date'=> pSQL(date("Y-m-d H:i:s", $order['order_date']/1000)),
            'order_type'=> pSQL($order['order_type']),
            'status'=> pSQL(($order['status']!= null)?$order['status']:$order['shippings']['0']['status']),
            'invoiced'=> NULL,
            'invoice_key'=> pSQL($order['shippings']['0']['invoice']['access_key']),
            'customer_name'=> pSQL((isset($order['shippings']['0']['customer']['customer_name'])?$order['shippings']['0']['customer']['customer_name']:NULL)),
            'customer'=> pSQL(Tools::jsonEncode($order['shippings']['0']['customer'],JSON_UNESCAPED_UNICODE)),
            'devolution_items'=>pSQL( Tools::jsonEncode($order['shippings']['0']['devolution_items'],JSON_UNESCAPED_UNICODE)) ,
            'total_gross'=> pSQL($order['total_gross']),
            'total_net'=> pSQL($order['total_net']),
            'total_freight'=> pSQL($order['total_freight']),
            'total_commission'=> pSQL($order['total_commission']),
            'total_discount'=> pSQL($order['total_discount']),
            'products'=> !isset($order['shippings']['0']['items'])?NULL:pSQL(Tools::jsonEncode($order['shippings']['0']['items'],JSON_UNESCAPED_UNICODE)),
            'shipping'=> !isset($order['shippings']['0'])?NULL:pSQL(Tools::jsonEncode($order['shippings']['0'],JSON_UNESCAPED_UNICODE)) ,
            //'payment_methods'=> !isset($order->paymentMethods)?NULL:pSQL(Tools::jsonEncode($order->paymentMethods,JSON_UNESCAPED_UNICODE)) ,
            'date_add'=> date('Y-m-d H:i:s'),
            'date_upd'=> date('Y-m-d H:i:s'),
        );
//        logMessageNS($order['shippings']['0']);
        $orderNS = netshoesgroupOrder::getOrderByNGId((int)$order['number']);
        //var_dump($orderNS);
        if( $order['shippings']['0']['status'] == 'Approved' && $orderNS != false && $orderNS->status != $order['shippings']['0']['status'] )
        {
            foreach( $order['shippings']['0']['items'] as $item )
            {
                $sku = netshoesgroupSku::getSkuByNetshoesId($item['sku']);
                if($sku->id_sku != null)
                {
                    $sku->stockQuantity = $sku->stockQuantity - $item['quantity'];
                    if($sku->id_product != null)
                    {
                        if($sku->stockQuantity <= 0 || ($sku->stockQuantity + $sku->stockVariation) <= 0)
                        {
                            $productStock = intval( StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute) );
                            StockAvailable::setQuantity($sku->id_product, $sku->id_attribute, 0);
                            logMessageNS('Stock Sku update to 0 ( Approved Order ) '.$order['number']);
                            logMessageNS($sku);
                        }
                        else
                        {
                            StockAvailable::updateQuantity($sku->id_product, $sku->id_attribute, -$item['quantity']);
                            logMessageNS('Stock Sku Update for less ( Approved Order ) '.$order['number'] );
                            logMessageNS($sku);
                        }
                    }
                }
            }

        }
        elseif(!$orderNS->reserval && $order['shippings']['0']['status'] == 'Canceled' &&  $orderNS->status != $order['shippings']['0']['status'])
        {
            foreach( $order['shippings']['0']['items'] as $item )
            {
                $sku = netshoesgroupSku::getSkuByNetshoesId($item['sku']);
                if($sku != false && $sku->id_sku != null)
                {
                    $sku->stockQuantity = $sku->stockQuantity + $item['quantity'];
                    if($sku->id_product != null)
                    {
                        if($sku->stockQuantity <= 0 || ($sku->stockQuantity + $sku->stockVariation) <= 0)
                        {
                            $productStock = intval( StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute) );
                            StockAvailable::setQuantity($sku->id_product, $sku->id_attribute, 0);
                            logMessageNS('Stock Sku update to 0 ( Canceled Order ) '.$order['number']);
                            logMessageNS($sku);
                        }
                        else
                        {
                            StockAvailable::updateQuantity($sku->id_product, $sku->id_attribute, +$item['quantity']);
                            logMessageNS('Stock Sku Update for more ( Canceled Order ) '.$order['number'] );
                            logMessageNS($sku);
                        }
                    }
                }
            }
            $dataInsert['reserval'] = true;
        }
        if(Db::getInstance()->getValue('SELECT id_netshoes FROM '._DB_PREFIX_.'netshoesgroup_order WHERE id_netshoes = "'.$order['number'].'"'))
        {
            $insert = Db::getInstance()->update('netshoesgroup_order',$dataInsert,'id_netshoes = "'.$order['number'].'"');
            logMessageNS('atualizar '. $order['number'] . ' (update):'. $insert);
//            logMessageNS($dataInsert);
            var_dump($insert);
        }
        else
        {
            $insert = Db::getInstance()->insert('netshoesgroup_order',$dataInsert);
            logMessageNS('Inserir '. $order['number'] . ' (insert):' .$insert);
//            ($dataInsert);
            var_dump($insert);
        }
    }
   sleep('5');
    if( count($orders['items']) > 0 )
    {
        $page += 1;
        // header("Refresh:0; url=cronOrders.php?page=".$page.'&last='.$last);
        header("Location: cronOrders.php?page=".$page.'&last='.$last);
    }
}
else
{
    echo 'matar';
    die();
}
//do
//{
//	try
//    {
//		$orders = $apiOrder->listOrders($page, 25, array("shippings", "items", "devolutionItems"), null, null, null, null);
//	}
//	catch (ApiException $e)
//    {
//		$return['error'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
//	}
//    if($page == 0)
//    {
//        $last = $orders->links[3]->href;
//        $last = explode("?", $last);
//        $last = explode("&", $last[1]);
//        $last = explode("=", $last[0]);
//        $last = (int)$last[1];
//    }
//
//	foreach ($orders['items'] as $key => $order)
//	{
//		$dataInsert = array(
//			'id_netshoes'=> $order['number'],
//		  	'id_site'=> pSQL($order['origin_number']),
//		  	'business_unit'=> pSQL(($order['business_unit']==1)?'NS':'ZT'),
//			'devolution_requested'=> pSQL($order['devolution_requested']),
//			'exchange_requested'=> pSQL($order['exchange_requested']),
//			'order_date'=> pSQL(date("Y-m-d H:i:s", $order['order_date']/1000)),
//			'order_type'=> pSQL($order['order_type']),
//		  	'status'=> pSQL(($order['status']!= null)?$order['status']:$order['shippings']['0']['status']),
//		  	'invoiced'=> NULL,
//		  	'invoice_key'=> pSQL($order['shippings']['0']['invoice']['access_key']),
//		  	'customer_name'=> pSQL((isset($order['shippings']['0']['customer']['customer_name'])?$order['shippings']['0']['customer']['customer_name']:NULL)),
//		  	'customer'=> pSQL(Tools::jsonEncode($order['shippings']['0']['customer'],JSON_UNESCAPED_UNICODE)),
//		  	'devolution_items'=>pSQL( Tools::jsonEncode($order['shippings']['0']['devolution_items'],JSON_UNESCAPED_UNICODE)) ,
//		  	'total_gross'=> pSQL($order['total_gross']),
//		  	'total_net'=> pSQL($order['total_net']),
//		  	'total_freight'=> pSQL($order['total_freight']),
//		  	'total_commission'=> pSQL($order['total_commission']),
//		  	'total_discount'=> pSQL($order['total_discount']),
//		  	'products'=> !isset($order['shippings']['0']['items'])?NULL:pSQL(Tools::jsonEncode($order['shippings']['0']['items'],JSON_UNESCAPED_UNICODE)),
//		  	'shipping'=> !isset($order['shippings']['0'])?NULL:pSQL(Tools::jsonEncode($order['shippings']['0'],JSON_UNESCAPED_UNICODE)) ,
//		  	//'payment_methods'=> !isset($order->paymentMethods)?NULL:pSQL(Tools::jsonEncode($order->paymentMethods,JSON_UNESCAPED_UNICODE)) ,
//		  	'date_add'=> date('Y-m-d H:i:s'),
//		  	'date_upd'=> date('Y-m-d H:i:s'),
//		);
//		$orderNS = netshoesgroupOrder::getOrderByNGId($order['number']);
//		if( $order['shippings']['0']['status'] == 'Approved' && $orderNS->status != $order['shippings']['0']['status'] )
//		{
//			foreach( $order['shippings']['0']['items'] as $item )
//			{
//				$sku = netshoesgroupSku::getSkuByNetshoesId($item['sku']);
//				if($sku->id_sku != null)
//				{
//					$sku->stockQuantity = $sku->stockQuantity - $item['quantity'];
//					if($sku->id_product != null)
//					{
//						if($sku->stockQuantity <= 0 || ($sku->stockQuantity + $sku->stockVariation) <= 0)
//						{
//							$productStock = intval( StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute) );
//							StockAvailable::setQuantity($sku->id_product, $sku->id_attribute, 0);
//							logMessageNS('Stock Sku update to 0 ( Approved Order ) '.$order['number']);
//							logMessageNS($sku);
//						}
//						else
//						{
//							StockAvailable::updateQuantity($sku->id_product, $sku->id_attribute, -$item['quantity']);
//							logMessageNS('Stock Sku Update for less ( Approved Order ) '.$order['number'] );
//							logMessageNS($sku);
//						}
//					}
//				}
//			}
//
//		}
//		elseif( $order['shippings']['0']['status'] == 'Canceled' &&  $orderNS->status != $order['shippings']['0']['status'])
//		{
//			foreach( $order['shippings']['0']['items'] as $item )
//			{
//				$sku = netshoesgroupSku::getSkuByNetshoesId($item['sku']);
//				if($sku->id_sku != null)
//				{
//					$sku->stockQuantity = $sku->stockQuantity + $item['quantity'];
//					if($sku->id_product != null)
//					{
//						if($sku->stockQuantity <= 0 || ($sku->stockQuantity + $sku->stockVariation) <= 0)
//						{
//							$productStock = intval( StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute) );
//							StockAvailable::setQuantity($sku->id_product, $sku->id_attribute, 0);
//							logMessageNS('Stock Sku update to 0 ( Canceled Order ) '.$order['number']);
//							logMessageNS($sku);
//						}
//						else
//						{
//							StockAvailable::updateQuantity($sku->id_product, $sku->id_attribute, +$item['quantity']);
//							logMessageNS('Stock Sku Update for more ( Canceled Order ) '.$order['number'] );
//							logMessageNS($sku);
//						}
//					}
//				}
//			}
//
//		}
//		if(Db::getInstance()->getValue('SELECT id_netshoes FROM '._DB_PREFIX_.'netshoesgroup_order WHERE id_netshoes = "'.$order['number'].'"'))
//		{
//			$insert = Db::getInstance()->update('netshoesgroup_order',$dataInsert,'id_netshoes = "'.$order['number'].'"');
//			logMessageNS('atualizar '. $order['number'] . ' (update):'. $insert);
//			//logMessageNS($order);
//			var_dump($inser);
//		}
//		else
//		{
//			$insert = Db::getInstance()->insert('netshoesgroup_order',$dataInsert);
//			logMessageNS('Inserir '. $order['number'] . ' (insert):' .$insert);
//			//logMessageNS($order);
//			var_dump($insert);
//		}
//	}
//    var_dump($page);
//    var_dump($last);
//    $page += 1;
//	if($page > $last)
//    {
//        $page = 100;
//    }
//
//}while($page < 100);
/*$ordersIds = Db::getInstance()->executeS('SELECT id_netshoes FROM '._DB_PREFIX_.'netshoesgroup_order WHERE status != "Canceled" && status != "Delivered"');
foreach($ordersIds as $key => $orderId) {
	$error = array();
	try {
		$orderApi = $apiOrder->getOrder($orderId['id_netshoes'], array("shippings", "items", "devolutionItems", "address"));
	} catch (ApiException $e) {
		$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
	}
	logMessageNS($orderApi);
	if($orderApi != array()) {
		$order = netshoesgroupOrder::getOrderByNGId($orderId['id_netshoes']);
		try{
			$order->id_site = isset($orderApi['origin_number'])?$orderApi['origin_number']:NULL;
			$order->business_unit = ($orderApi['business_unit'] == 1)?'NS':'ZT';
			$order->devolution_requested = isset($orderApi['devolution_requested'])?$orderApi['devolution_requested']:NULL;
			$order->exchange_requested = isset($orderApi['exchange_requested'])?$orderApi['exchange_requested']:NULL;
			$order->order_date = isset($orderApi['order_date'])?date("Y-m-d H:i:s", ($orderApi['order_date']/1000)):NULL;
			$order->order_type = isset($orderApi['order_type'])?$orderApi['order_type']:NULL;
			$order->status = ($orderApi['status']!= null)?$orderApi['status']:$orderApi['shippings']['0']['status'];
			$order->invoice_key = isset($orderApi['shippings']['invoice']['access_key'])?$orderApi['shippings']['0']['invoice']['access_key']:NULL;
			$order->customer_name = isset($orderApi['shippings']['0']['customer']['customer_name'])?$orderApi['shippings']['0']['customer']['customer_name']:NULL;
			$order->customer = isset($orderApi['shippings']['0']['customer'])?$orderApi['shippings']['0']['customer']:NULL;
			$order->devolution_items = isset($orderApi['shippings']['0']['devolution_items'])?$orderApi['shippings']['0']['devolution_items']:NULL;
			$order->total_gross = isset($orderApi['total_gross'])?$orderApi['total_gross']:NULL;
			$order->total_net = isset($orderApi['total_net'])?$orderApi['total_net']:NULL;
			$order->total_freight = isset($orderApi['total_freight'])?$orderApi['total_freight']:NULL;
			$order->total_commission = isset($orderApi['total_commission'])?$orderApi['total_commission']:NULL;
			$order->total_discount = isset($orderApi['total_discount'])?$orderApi['total_discount']:NULL;
			$order->products = isset($orderApi['shippings']['0']['items'])?$orderApi['shippings']['0']['items']:NULL;
			$order->shipping = isset($orderApi['shippings']['0'])?$orderApi['shippings']['0']:NULL;
			// order->payment_methods = isset($response['body']->paymentMethods)?$response['body']->paymentMethods:NULL;
			$result = $order->update();
			echo "Atualizado ".$orderId['id_netshoes'];
		} catch (Exception $e) {
			$error[] = $e->getMessage();
			logMessageNS($error);
		}
	}
}*/






