<?php
use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\sku as sku;
use ApiMktpNetshoesV1\product as product;
require(dirname(__FILE__).'/../../config/config.inc.php');
require(dirname(__FILE__).'/../../init.php');
require dirname(__FILE__).'/includes/functions.php';
require dirname(__FILE__).'/classes/netshoesgroupSku.php';
require_once dirname(__FILE__).'/ApiMktpNetshoesV1.php';

$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
$useSSL = true;
client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
client\Configuration::$apiClient = $netshoesGroup;
client\Configuration::$debug = false;

$skusApi = new sku\SkusApi($netshoesGroup);
$productsApi = new product\ProductsApi($netshoesGroup);
/*$sql = 'SELECT id_sku FROM ps_netshoesgroup_sku WHERE id_sku >= 1100 AND id_sku < 1200';
$i=0;
$products = @$productsApi->listProducts(0, 1500, array());
if ($skusSql = Db::getInstance()->ExecuteS($sql)) {

    foreach ($skusSql as $skuSql) {
	$sku = new netshoesgroupSku($skuSql['id_sku']);
	$api = @$skusApi->getProductSku($sku->id_netshoes, $sku->sku, array('images'));
	if($api == null) {
		echo 'Id do produto errado : '.$sku->id_netshoes .' </br>';
		foreach($products['items'] as $product) {
			foreach ($product['skus'] as $skuApi) {				
				//die();	
				if($sku->sku == $skuApi['sku']) {
					$sku->id_netshoes = $product['product_id'];
					echo 'SKU : '.$sku->sku.', New Netshoes id : '. $sku->id_netshoes. '</br>';
					$sku->update();
				}
			}
			//die();
		}
	}
	sleep(3);
   }
}

*/

$sql = 'SELECT  relation.id_sku, relation.id_product, relation.id_attribute
FROM ps_netshoesgroup_relation as relation
LEFT JOIN ps_netshoesgroup_sku as sku
ON relation.id_sku = sku.sku
WHERE sku.sku IS NULL LIMIT 6000';
$alreadyUpdate = array();
if ($skusSql = Db::getInstance()->ExecuteS($sql)) {
	//var_dump($skusSql);
    foreach ($skusSql as $skuSql) {
	
	echo count($skusSql);
	$sql = 'SELECT  id_sku, sku, COUNT(*) as count  FROM ps_netshoesgroup_sku WHERE id_product = '.$skuSql['id_product'] .' GROUP BY
    sku HAVING count > 1';
	if ($skusWithError = Db::getInstance()->ExecuteS($sql)) {
        	foreach ($skusWithError as $skuWithError) {
			
			if(intval($skuWithError['count']) > 1) {
				//var_dump($skuWithError).'</br></br>';
				var_dump($alreadyUpdate);
				if( ! in_array ($skuWithError['id_sku'], $alreadyUpdate)) { 
					$sku = new netshoesgroupSku($skuWithError['id_sku']);
					$stock = StockAvailable::getQuantityAvailableByProduct($skuSql['id_product'], $skuSql['id_attribute']);
					echo $sku->sku . ' : ANTIGO : '.$skuWithError['id_sku'] .' </br>';
					$sku->sku = $skuSql['id_sku'];
					$sku->id_attribute = $skuSql['id_attribute'];
					$sku->stockQuantity = $stock;
				
					
					$api = @$skusApi->getProductSku($sku->id_netshoes, $sku->sku, array('images'));
					if($api != null) { 
						$sku->name = $api['name'];
						$sku->description = $api['description'];
						$sku->color = $api['color'];
						$sku->flavor = $api['flavor'];
						$sku->size = $api['size'];
						$sku->gender = $api['gender'];
						$sku->eanIsbn = $api['ean_isbn'];
						$sku->images = $api['images'];
						$sku->height = $api['height'];
						$sku->width = $api['width'];
						$sku->depth = $api['depth'];
						$sku->weight = $api['weight'];
						if( $sku->update() ) {	
							echo $sku->sku . ' : SUCESSO</br>';
							$alreadyUpdate[] = $skuWithError['id_sku'];	
							break;						
						} else {
							echo $sku->sku . ' : FALHA</br>';	
						}
						
					}
					sleep(2);
					
				}
			}
		}
	}
    }
}
