<?php
class netshoesgroupOrder extends ObjectModel
{
    public $id_order;	
    public $id_netshoes;
    public $id_site;
    public $business_unit; // NS (netshoes) ou ZT (zattini)
    public $devolution_requested;
    public $exchange_requested;
    public $order_date;
    public $order_type;
    public $status;
    public $invoice_key;
    public $invoiced;
    public $customer_name;
    public $customer;
    public $devolution_items;
    public $total_gross;
    public $total_net;
    public $total_freight;
    public $total_commission;
    public $total_discount;
    public $products;
    public $shipping;
    public $shipping_code;
    public $shipping_info;
    // public $payment_methods;
    public $reversal;
    public $date_add;
    public $date_upd;
        
	public static $definition = array(
		'table' => 'netshoesgroup_order',
		'primary' => 'id_order',
		'multilang'=>false,
		'fields' => array(
 			'id_netshoes' => array('type' => self::TYPE_STRING,  'validate' => 'isString','required' => true, 'size' => 50),
 			'id_site' => array('type' => self::TYPE_STRING,  'validate' => 'isString','required' => false, 'size' => 20),
 			'business_unit' => array('type' => self::TYPE_STRING,  'validate' => 'isString','required' => true, 'size' => 20),
 			'devolution_requested' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
 			'exchange_requested' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
 			'order_date' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
 			'order_type' => array('type' => self::TYPE_STRING,  'validate' => 'isString','required' => true, 'size' => 50),
			'status' => array('type' => self::TYPE_STRING,  'validate' => 'isString','required' => true, 'size' => 20),
			'invoice_key' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'required' => false, 'size' => 150),
			'invoiced' => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
			'customer_name' => array('type' => self::TYPE_STRING,  'validate' => 'isString','required' => true, 'size' => 100),
			'customer' => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
			'devolution_items' => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
			'total_gross'=>array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_net'=>array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_freight'=>array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_commission'=>array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_discount'=>array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'products' => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
			'shipping' => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
			'shipping_code' => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
			'shipping_info' => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
			// 'payment_methods' => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
			'reversal' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
		
		),
	);
        
 	public function __construct($id = null, $id_lang = null, $id_shop = null)
	{
		parent::__construct($id, $id_lang, $id_shop);
		
		$this->customer = ($this->customer)?Tools::jsonDecode($this->customer):null;
		$this->devolution_items = ($this->devolution_items)?Tools::jsonDecode($this->devolution_items):null;
		$this->products = ($this->products)?Tools::jsonDecode($this->products):null;
		$this->shipping = ($this->shipping)?Tools::jsonDecode($this->shipping):null;
		$this->shipping_info = ($this->shipping_info)?Tools::jsonDecode($this->shipping_info):null;
		$this->invoiced = ($this->invoiced)?Tools::jsonDecode($this->invoiced):null;
		// $this->payment_methods = ($this->payment_methods)?Tools::jsonDecode($this->payment_methods):null;
	}

	public function update($null_values = false)
    {
		$this->customer = (is_array($this->customer) || is_object($this->customer))?Tools::jsonEncode($this->customer,JSON_UNESCAPED_UNICODE):$this->customer;
		$this->devolution_items = (is_array($this->devolution_items) || is_object($this->devolution_items))?Tools::jsonEncode($this->devolution_items,JSON_UNESCAPED_UNICODE):$this->devolution_items;
		$this->products = (is_array($this->products) || is_object($this->products))?Tools::jsonEncode($this->products,JSON_UNESCAPED_UNICODE):$this->products;
		$this->shipping = (is_array($this->shipping) || is_object($this->shipping))?Tools::jsonEncode($this->shipping,JSON_UNESCAPED_UNICODE):$this->shipping;
		$this->shipping_info = (is_array($this->shipping_info) || is_object($this->shipping_info))?Tools::jsonEncode($this->shipping_info,JSON_UNESCAPED_UNICODE):$this->shipping_info;
		$this->invoiced = (is_array($this->invoiced) || is_object($this->invoiced))?Tools::jsonEncode($this->invoiced,JSON_UNESCAPED_UNICODE):$this->invoiced;
		// $this->payment_methods = (is_array($this->payment_methods) || is_object($this->payment_methods))?Tools::jsonEncode($this->payment_methods,JSON_UNESCAPED_UNICODE):$this->payment_methods;
		
		$dateInstall = (Configuration::get('netshoesgroup_install_date')?Configuration::get('netshoesgroup_install_date'):date('Y-m-d')).' 00:00:00';
		
		//verifica se a data que o pedido foi criado é menor que a data de instalação
		//também verifica se o status for igual a CANCELED e se o produto já não foi estornado.

		// if(strtotime($dateInstall) < strtotime($this->order_date) && $this->status == 'CANCELED' && $this->reversal == 0){
		// 	$sql = 'SELECT `products` FROM `'._DB_PREFIX_.'netshoesgroup_order` WHERE `id_order`= "'.$this->id_order.'"';
        // 	$this->products =  Db::getInstance()->getValue($sql);
		// 	$products = Tools::jsonDecode($this->products);
		// 	if(is_array($products)){
		// 		foreach ($products as $key => $product) {
		// 			$sql = 'SELECT `id_product`,`id_attribute` FROM `'._DB_PREFIX_.'netshoesgroup_sku` WHERE `sku`= "'.$product->sku.'" AND `id_product` > 0';
        // 			$sku =  Db::getInstance()->getRow($sql);
		// 			if($sku){
		// 				StockAvailable::updateQuantity((int)$sku['id_product'], (int)$sku['id_attribute'], +(int)$product->quantity);
		// 				$this->reversal = true;
		// 			}
		// 		}
		// 	}
		// }
		
        return parent::update($null_values);
    }
    public function add($autodate = true, $null_values = false)
    {
    	if (/*$this->status == 'NEW' ||*/ $this->status == 'APPROVED'){
	    	$products = $this->products;
			if($products){
				if(!is_array($products)){
					$products = Tools::jsonDecode($this->products);
				}
				if(is_array($products) && count($products)>0){
					foreach ($products as $key => $product) {
						$sql = 'SELECT `id_product`,`id_attribute` FROM `'._DB_PREFIX_.'netshoesgroup_sku` WHERE `sku`= "'.$product->sku.'" AND `id_product` > 0';
	        			$sku =  Db::getInstance()->getRow($sql);
						if($sku){
							StockAvailable::updateQuantity((int)$sku['id_product'], (int)$sku['id_attribute'], -(int)$product->quantity);
						}
					}
				}
			}
		}
		$this->customer = (is_array($this->customer) || is_object($this->customer))?Tools::jsonEncode($this->customer,JSON_UNESCAPED_UNICODE):$this->customer;
		$this->devolution_items = (is_array($this->devolution_items) || is_object($this->devolution_items))?Tools::jsonEncode($this->devolution_items,JSON_UNESCAPED_UNICODE):$this->devolution_items;
		$this->products = (is_array($this->products) || is_object($this->products))?Tools::jsonEncode($this->products,JSON_UNESCAPED_UNICODE):$this->products;
		$this->shipping = (is_array($this->shipping) || is_object($this->shipping))?Tools::jsonEncode($this->shipping,JSON_UNESCAPED_UNICODE):$this->shipping;
		$this->shipping_info = (is_array($this->shipping_info) || is_object($this->shipping_info))?Tools::jsonEncode($this->shipping_info,JSON_UNESCAPED_UNICODE):$this->shipping_info;
		$this->invoiced = (is_array($this->invoiced) || is_object($this->invoiced))?Tools::jsonEncode($this->invoiced,JSON_UNESCAPED_UNICODE):$this->invoiced;
		// $this->payment_methods = (is_array($this->payment_methods) || is_object($this->payment_methods))?Tools::jsonEncode($this->payment_methods,JSON_UNESCAPED_UNICODE):$this->payment_methods;
        
        return parent::add($autodate, $null_values);
    }
	
	public function validateFields($die = true, $error_return = false)
    {
		$this->customer = (is_array($this->customer) || is_object($this->customer))?Tools::jsonEncode($this->customer,JSON_UNESCAPED_UNICODE):$this->customer;
		$this->devolution_items = (is_array($this->devolution_items) || is_object($this->devolution_items))?Tools::jsonEncode($this->devolution_items,JSON_UNESCAPED_UNICODE):$this->devolution_items;
		$this->products = (is_array($this->products) || is_object($this->products))?Tools::jsonEncode($this->products,JSON_UNESCAPED_UNICODE):$this->products;
		$this->shipping = (is_array($this->shipping) || is_object($this->shipping))?Tools::jsonEncode($this->shipping,JSON_UNESCAPED_UNICODE):$this->shipping;
		$this->shipping_info = (is_array($this->shipping_info) || is_object($this->shipping_info))?Tools::jsonEncode($this->shipping_info,JSON_UNESCAPED_UNICODE):$this->shipping_info;
		$this->invoiced = (is_array($this->invoiced) || is_object($this->invoiced))?Tools::jsonEncode($this->invoiced,JSON_UNESCAPED_UNICODE):$this->invoiced;
		// $this->payment_methods = (is_array($this->payment_methods) || is_object($this->payment_methods))?Tools::jsonEncode($this->payment_methods,JSON_UNESCAPED_UNICODE):$this->payment_methods;
		
        parent::validateFields($die, $error_return);
    }
	
	
	/**
     * Get an order by its netshoes id
     *
     * @param int idNG Order id
     * @return array Order details
     */
    public static function getOrderByNGId($idNG)
    {
        $sql = 'SELECT `id_order` FROM `'._DB_PREFIX_.'netshoesgroup_order` WHERE `id_netshoes`= "'.$idNG.'"';
        $id_order =  Db::getInstance()->getValue($sql);
        return ($id_order)? new netshoesgroupOrder($id_order) : false;
    }
}
