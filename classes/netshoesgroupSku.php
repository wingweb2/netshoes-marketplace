<?php
class netshoesgroupSku extends ObjectModel
{
	public $id_sku;
	public $sku;
	public $id_product;
	public $id_attribute;
	public $color;
	public $flavor;
	public $gender;
	public $brand;
	public $product_type;
	public $department;
	public $size;
	public $name;
	public $description;
	public $eanIsbn; 
	public $height; 
	public $width; 
	public $depth; 
	public $weight; 
	public $stockQuantity; 	
	public $stockVariation; 
	public $enable;
	public $price;
	public $images; 
	public $id_netshoes;
	public $link;
	public $situation;
	public $variation_price;
	public $variation_priority;
	public $price_impact;
	public $business_id;
    public $date_add;
    public $date_upd;
        

	public static $definition = array(
		'table' => 'netshoesgroup_sku',
		'primary' => 'id_sku',
		'multilang'=>false,
		'fields' => array(
 			'sku' => array('type' => self::TYPE_STRING,  'validate' => 'isString','required' => true, 'size' => 50),
 			'id_product' => array('type' => self::TYPE_INT,  'validate' => 'isUnsignedInt','required' => false, 'size' => 20),
 			'id_attribute' => array('type' => self::TYPE_INT,  'validate' => 'isUnsignedInt','required' => false, 'size' => 20),
 			'color' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'size' => 50),
 			'flavor' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'size' => 50),
 			'product_type' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'size' => 50),
 			'department' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'size' => 50),
			'size' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'size' => 500),
			'name' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'size' => 500),
 			'description' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml'),
			'eanIsbn' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'size' => 255),
 			'width' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'height' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'depth' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'weight' => array('type' => self::TYPE_FLOAT, 'validate' => 'isUnsignedFloat'),
            'stockQuantity' => array('type' => self::TYPE_INT,  'validate' => 'isInt','required' => false, 'size' => 20),
	    'stockVariation' => array('type' => self::TYPE_INT,  'validate' => 'isInt','required' => false, 'size' => 11),
            'enable' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'price' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'size' => 255),
			'images' => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
			'id_netshoes' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'size' => 20),
			'link' => array('type' => self::TYPE_STRING,  'validate' => 'isString'),
			'situation' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'size' => 50),
            'variation_price' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
			'variation_priority' => array('type' => self::TYPE_STRING,  'validate' => 'isString', 'size' => 1),
            'price_impact' => array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'business_id' => array('type' => self::TYPE_STRING,  'validate' => 'isString','required' => true, 'size' => 50),
			'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'date_upd' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
		),
	);
        
 	public function __construct($id = null, $id_lang = null, $id_shop = null)
	{
		parent::__construct($id, $id_lang, $id_shop);
		
		//$this->eanIsbn = ($this->eanIsbn)?Tools::jsonDecode($this->eanIsbn):null;
		$this->price = ($this->price)?Tools::jsonDecode($this->price):null;
		$this->images = ($this->images)?Tools::jsonDecode($this->images):null;
		//$this->link = ($this->link)?Tools::jsonDecode($this->link):null;
		if($this->business_id == "Netshoes" || $this->business_id == "NS")
		{
			$bu_id = "NS";
			$this->business_id = $bu_id;
		}
		if($this->business_id == "Zattini" || $this->business_id == "ZT")
		{
			$bu_id = "ZT";
			$this->business_id = $bu_id;
		}
	}
	
	public function update($null_values = false)
    {
    	//$this->eanIsbn = (is_array($this->eanIsbn) || is_object($this->eanIsbn))?Tools::jsonEncode($this->eanIsbn,JSON_UNESCAPED_UNICODE):$this->eanIsbn;
		$this->price = (is_array($this->price) || is_object($this->price))?Tools::jsonEncode($this->price,JSON_UNESCAPED_UNICODE):$this->price;
		$this->images = (is_array($this->images) || is_object($this->images))?Tools::jsonEncode($this->images,JSON_UNESCAPED_UNICODE):$this->images;
		$this->link = (is_array($this->link) || is_object($this->link))?Tools::jsonEncode($this->link,JSON_UNESCAPED_UNICODE):$this->link;
		if($this->business_id == "Netshoes" || $this->business_id == "NS")
		{
			$bu_id = "NS";
			$this->business_id = $bu_id;
		}
		if($this->business_id == "Zattini" || $this->business_id == "ZT")
		{
			$bu_id = "ZT";
			$this->business_id = $bu_id;
		}
		
        return parent::update($null_values);
    }
    public function add($autodate = true, $null_values = false)
    {
    	//$this->eanIsbn = (is_array($this->eanIsbn) || is_object($this->eanIsbn))?Tools::jsonEncode($this->eanIsbn,JSON_UNESCAPED_UNICODE):$this->eanIsbn;
		$this->price = (is_array($this->price) || is_object($this->price))?Tools::jsonEncode($this->price,JSON_UNESCAPED_UNICODE):$this->price;
		$this->images = (is_array($this->images) || is_object($this->images))?Tools::jsonEncode($this->images,JSON_UNESCAPED_UNICODE):$this->images;
		$this->link = (is_array($this->link) || is_object($this->link))?Tools::jsonEncode($this->link,JSON_UNESCAPED_UNICODE):$this->link;
		if($this->business_id == "Netshoes" || $this->business_id == "NS")
		{
			$bu_id = "NS";
			$this->business_id = $bu_id;
		}
		if($this->business_id == "Zattini" || $this->business_id == "ZT")
		{
			$bu_id = "ZT";
			$this->business_id = $bu_id;
		}
		
        return parent::add($autodate, $null_values);
    }
	public function validateFields($die = true, $error_return = false)
    {
    	//$this->eanIsbn = (is_array($this->eanIsbn) || is_object($this->eanIsbn))?Tools::jsonEncode($this->eanIsbn,JSON_UNESCAPED_UNICODE):$this->eanIsbn;
		$this->price = (is_array($this->price) || is_object($this->price))?Tools::jsonEncode($this->price,JSON_UNESCAPED_UNICODE):$this->price;
		$this->images = (is_array($this->images) || is_object($this->images))?Tools::jsonEncode($this->images,JSON_UNESCAPED_UNICODE):$this->images;
		$this->link = (is_array($this->link) || is_object($this->link))?Tools::jsonEncode($this->link,JSON_UNESCAPED_UNICODE):$this->link;
		
        parent::validateFields($die, $error_return);
    }
	
	
	/**
     * Get sku by netshoes id
     *
     * @param string netshoes id
     * @return array SKU details
     */
    public static function getSkuByNetshoesId($id_netshoes)
    {
        $sql = 'SELECT `id_sku` FROM `'._DB_PREFIX_.'netshoesgroup_sku` WHERE `sku`= "'.$id_netshoes.'"';
        $id_sku =  Db::getInstance()->getValue($sql);
        return ($id_sku)? new netshoesgroupSku($id_sku) : false;
    }
	
	/**
     * Get sku by id product an id attribute
     *
     * @param int id product
     * @param int id attribute
     * @return int|netshoesgroupSku|false Sku details
     */
    public static function getSkuByProduct($idProduct, $idAttribute = 0, $style = 'get')
    {
        $sql = 'SELECT `id_sku` FROM `'._DB_PREFIX_.'netshoesgroup_sku` WHERE `id_product` = '.(int)$idProduct;
        if($idAttribute == 0)
        {
            $sql.=' AND `id_attribute` = '.(int)$idAttribute;
        }
        elseif($idAttribute != 0 && $style == 'get')
		 {
			$sql.=' AND `id_attribute` = '.(int)$idAttribute;
		 }
		
        $id_sku =  Db::getInstance()->getValue($sql);
        if (count($id_sku) > 1) {
        	return $id_sku;
        } elseif ($id_sku) {
        	return new netshoesgroupSku($id_sku);
        }
        else
        {
        	return false;
        }
    }
    public static function getSkuByProductObj($idProduct, $idAttribute = 0)
    {
        $sql = 'SELECT `id_sku` FROM `'._DB_PREFIX_.'netshoesgroup_sku` WHERE `id_product` = '.(int)$idProduct;

        $id_sku =  Db::getInstance()->getValue($sql);
//        echo '<pre style="display: none;">';
//        echo 'obj: ';
//        var_dump($id_sku);
//        echo '</pre>';
        if($id_sku == false)
            return false;
        else
            return new self((int)$id_sku);
    }
}
