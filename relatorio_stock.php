<?php
/**
 * Created by PhpStorm.
 * User: Backend Wing
 * Date: 02/10/2017
 * Time: 14:35
 */

use ApiMktpNetshoesV1\sku as sku;
use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\product as product;

require(dirname(__FILE__).'/../../config/config.inc.php');
require(dirname(__FILE__).'/../../init.php');
require dirname(__FILE__).'/includes/functions.php';
require dirname(__FILE__).'/classes/netshoesgroupOrder.php';
require dirname(__FILE__).'/classes/netshoesgroupSku.php';
require_once dirname(__FILE__).'/ApiMktpNetshoesV1.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');

//ini_set('zlib.output_compression', 0);
//ini_set('implicit_flush', 1);
//ob_start();
//ob_end_clean();

$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
$useSSL = true;
client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
client\Configuration::$apiClient = $netshoesGroup;
client\Configuration::$debug = false;

$skusApi = new sku\SkusApi($netshoesGroup);
$error = array();
$export = array();
//$tabela = json_decode(file_get_contents('tabela.json'));
$tabela1 = json_decode(file_get_contents('tabela_error.json'));
foreach ($tabela1 as $sku)
{
    $error[] = $sku;
}
//foreach ($tabela as $sku)
//{
//    $export[] = $sku;
//}

if($_GET['filter']=="1")
{
    $sql =  "SELECT id_product, id_sku, sku, id_attribute FROM " . _DB_PREFIX_ . "netshoesgroup_sku WHERE id_product != 0 AND id_sku > " . $_GET['id_sku_min'] . " AND id_sku < " . $_GET['id_sku_max'] . " GROUP BY id_product ORDER BY id_sku";
}
else
{
    $sql =  "SELECT id_product, id_sku, sku, id_attribute FROM " . _DB_PREFIX_ . "netshoesgroup_sku WHERE id_product != 0 GROUP BY id_product ORDER BY id_sku";
}

$skus = Db::getInstance()->executeS($sql);

//ob_implicit_flush(true);
////echo Tools::jsonEncode($skus);
////die();
//echo 'Iniciando geracao de relatorio, total sku('.count($skus).'), isso pode demorar varios minutos';
//flush();
//ob_flush();
$i = 0;

foreach ($skus as $sku)
{
    $row_table = new stdClass();
    $sku_api = $skusApi->getStock($sku['sku']);


    if(!isset($sku_api))
    {
        $row_table->sku = "SKU NOT FOUND - " . $sku['sku'];
        $error[] = $row_table;
    }
    else
    {
        $row_table->sku = $sku['sku'];
        $id_product = $sku['id_product'];
        $id_attribute = (isset($sku['id_attribute'])) ? $sku['id_attribute'] : 0;
        $row_table->ps_stock = StockAvailableCore::getQuantityAvailableByProduct($sku['id_product'], $sku['id_attribute']);
        $row_table->api_stock_available = (int)$sku_api->available;
        $row_table->api_stock_total = (int)$sku_api->total;
        $row_table->api_stock_reserved = (int)$sku_api->reserved;
        if($row_table->ps_stock != $row_table->api_stock_total)
        {
            $error[] = $row_table;
        }
        $export[] = $row_table;
    }
    sleep(1);
}

//$arquive = fopen('tabela.json', 'w');
//fwrite($arquive, json_encode($export));
//fclose($arquive);
$arquive = fopen('tabela_error.json', 'w');
fwrite($arquive, json_encode($error));
fclose($arquive);
$id_sku_min=$skus[count($skus)-1]['id_sku'];
$age=$_GET['age'];
$id_sku_max=$id_sku_min+$age;
sleep(20);
header("Refresh:0; url=relatorio_stock.php?filter=1&id_sku_min=".$id_sku_min.'&id_sku_max='.$id_sku_max.'&age='.$age);