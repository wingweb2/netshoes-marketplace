<?php
use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\sku as sku;
use ApiMktpNetshoesV1\product as product;
require(dirname(__FILE__).'/../../config/config.inc.php');
require(dirname(__FILE__).'/../../init.php');
require dirname(__FILE__).'/includes/functions.php';
require dirname(__FILE__).'/classes/netshoesgroupSku.php';
require_once dirname(__FILE__).'/ApiMktpNetshoesV1.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
$useSSL = true;
client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
client\Configuration::$apiClient = $netshoesGroup;
client\Configuration::$debug = false;

$skusApi = new sku\SkusApi($netshoesGroup);
$productsApi = new product\ProductsApi($netshoesGroup);

$debug = isset($_GET['debug']) ? (int)$_GET['debug'] : 0;

// 1794
// $productApi = $productsApi->getProduct("1794", array('images', 'attributes'));
// echo  "Produto 1794: ".debug($productApi)."\r\n";

// // 1794
// $productApi = $productsApi->getProduct("013519", array('images', 'attributes'));
// echo  "Produto 013519: ".debug($productApi)."\r\n";

// 013519-41

$DB = Db::getInstance();
$page = isset($_GET['page']) && (int)$_GET['page'] > 0 ? (int)$_GET['page']:1;
$perPage = isset($_GET['perPage'])  && (int)$_GET['perPage'] > 0 ? (int)$_GET['perPage']:10;
$start = $page * $perPage - $perPage;
// $sql = 'SELECT sku, id_sku FROM '._DB_PREFIX_.'netshoesgroup_sku where id_sku >= '.($page*100).' AND id_sku <= '.($page*100+100);
$sql = "SELECT DISTINCT ns_sku.id_product, p.reference FROM `" . _DB_PREFIX_ . "netshoesgroup_sku` AS ns_sku
    LEFT JOIN `" . _DB_PREFIX_ . "product` AS p ON (p.`id_product` = ns_sku.`id_product`)
    WHERE ns_sku.`id_product` > 0 ORDER BY id_product ASC LIMIT $start, $perPage";
$sqlCount = "SELECT COUNT(DISTINCT id_product) as count FROM `" . _DB_PREFIX_ . "netshoesgroup_sku` WHERE 1";

$countResults = $DB->getValue($sqlCount, 1);
$results = $DB->ExecuteS($sql);

$content = null;
$content .= debug($sql)."\r\n";
$content .= debug($sqlCount)."\r\n";
$content .= json_encode($results)."\r\n";

if($results){
    foreach ($results as $row) {
        $productId = ($row['reference'] != '')?preg_replace("/[^a-zA-Z0-9-_.]/", "-", trim($row['reference'])):$row['id_product'];
        $productApi = $productsApi->getProduct($productId, array('images', 'attributes'));
        $content .= "Produto ".$row['id_product'].": ".debug($productApi)."\r\n";
    }
}

if( $countResults > $start + $perPage )
{
    $page += 1;
    // header("Refresh:0; url=cronSkus.php?page=".$page);
    // sleep(2);
    header("Location: cronSkusTeste.php?page=$page&perPage=$perPage&debug=$debug");
}

if($debug){
    echo $content;
}


// if ($results = Db::getInstance()->ExecuteS($sql))
// {
//     $i = 0;
//     foreach ($results as $row)
//     {
//         $sku = new netshoesgroupSku($row['id_sku']);
//         if($sku->business_id == "Netshoes" || $sku->business_id == "NS")
//         {
//             $bu_id = "NS";
//             $sku->business_id = $bu_id;
//         }
//         if($sku->business_id == "Zattini" || $sku->business_id == "ZT")
//         {
//             $bu_id = "ZT";
//             $sku->business_id = $bu_id;
//         }

//         if( $skuStatus = @$skusApi->getStatus($sku->sku, $sku->business_id) )
//         {
//             if($skuStatus == null)
//             {
//                 logMessageNS('Erro consultar status, aguardar 20s e tentar novamente: ' .$sku->sku);
//                 sleep(20);
//                 $skuStatus = $skusApi->getStatus($sku->sku, $sku->business_id);
//             }
//             if($skuStatus->statusMatch != null)
//             {
//                 $sku->situation = $skuStatus->statusMatch;
//                 $sku->update();
//             }
//             if($sku->situation == 'PENDING_MATCH')
//             {

//                 $body = array('country' => 'BR', 'listPrice' => floatval( ($sku->price->listPrice)?$sku->price->listPrice:$sku->price ), 'sellPrice' => floatval( ($sku->price->sellPrice)?$sku->price->sellPrice:$sku->price) );
//                 $skusApi->updatePrice($sku->sku, $body);
//                 logMessageNS('preço do sku '.$sku->sku.' foi atualizado');
//                 if($skuStatus['active'] == true)
//                 {
//                     if($sku->stockQuantity < 0)
//                         $sku->stockQuantity = 0;
//                     $bodyStock = array('country' => 'BR', 'available' => floatval($sku->stockQuantity));
//                     $skusApi->updateStock($sku->sku, $bodyStock);
//                     logMessageNS('quantidade do sku '.$sku->sku.' foi atualizada');
//                     $sku->enable = true;
//                 }
//                 $sku->situation = 'RELEASED_MATCH';
//                 $sku->update();
//                 /*
//                else {
//                    $body = array('country' => 'BR', 'listPrice' => floatval( ($sku->price->listPrice)?$sku->price->listPrice:$sku->price ), 'sellPrice' => floatval( ($sku->price->sellPrice)?$sku->price->sellPrice:$sku->price) );
//                    $skusApi->updatePrice($sku->sku, $body);
//                    logMessageNS('preço do sku '.$sku->sku.' foi atualizado');
//                }
//                */
//             }
//             elseif($sku->situation == 'RELEASED_MATCH')
//             {

//                 if($skuStatus['active'] == true)
//                 {
//                     $sku->enable = true;
//                     $sku->stockQuantity = intval( StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute) );
//                     if($sku->stockQuantity < 0)
//                         $sku->stockQuantity = 0;
//                     $bodyStock = array('country' => 'BR', 'available' => floatval($sku->stockQuantity));
//                     $resp = $skusApi->updateStock($sku->sku, $bodyStock);
//                     logMessageNS('quantidade do sku '.$sku->sku.' foi atualizada: ' . debug($bodyStock) . ' resposta: ' . debug($resp));
// //                    if($skuStock = @$skusApi->getStock($sku->sku))
// //                    {
// //                        if($sku->id_product)
// //                        {
// //                            $productStock = intval( StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute) );
// //                            $sku->stockVariation = $skuStock['available'] - $productStock;
// //                        }
// //                        $sku->stockQuantity = intval($skuStock['available']);
// //                    }
// //                    $body = array('country' => 'BR', 'listPrice' => floatval( ($sku->price->listPrice)?$sku->price->listPrice:$sku->price ), 'sellPrice' => floatval( ($sku->price->sellPrice)?$sku->price->sellPrice:$sku->price) );
// //                    $skusApi->updatePrice($sku->sku, $body);
// //                    logMessageNS('preço do sku '.$sku->sku.' foi atualizado');
// //                    if($skuPrice = @$skusApi->listPrices($sku->sku))
// //                    {
// //                        echo '<pre>';
// //                        var_dump($sku->sku);
// //                        var_dump($skuPrice);
// //                        echo '</pre>';
// //                        $sku->price->sellPrice = $skuPrice->price;
// //
// //                    }
//                     if(!$sku->update())
//                     {
//                         var_dump($sku);
//                         die();
//                     }
//                     else
//                     {
//                         var_dump($sku->stockQuantity);
//                         var_dump($sku->stockVariation);
//                     }


//                 }

//             }

//         }
//     }
//     logMessage(json_encode(array('page'=>$page, 'sql'=>$sql, 'count_results'=>count($results))), 0, dirname(__FILE__).'/logs/cron-sku-'.date('Y-m-d').'.log');
//     if( count($results) > 0 )
//     {
//         $page += 1;
//         // header("Refresh:0; url=cronSkus.php?page=".$page);
// //        sleep(20);
//         header("Location: cronSkus.php?page=".$page);
//     }
// }


