
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Estrutura da tabela `{prefix}netshoesgroup_order`
--

CREATE TABLE IF NOT EXISTS `{prefix}netshoesgroup_order` (
  `id_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_netshoes` bigint(20) NOT NULL,
  `id_site` varchar(20) DEFAULT NULL,
  `business_unit` varchar(20) DEFAULT NULL,
  `devolution_requested` BOOLEAN NOT NULL DEFAULT FALSE,
  `exchange_requested` BOOLEAN NOT NULL DEFAULT FALSE,
  `order_date` datetime DEFAULT NULL,
  `order_type` varchar(50) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `invoice_key` varchar(150) DEFAULT NULL,
  `invoiced` longtext,
  `customer_name` varchar(100) DEFAULT NULL,
  `customer` longtext,
  `devolution_items` longtext,
  -- `payer` longtext,
  `total_gross` decimal(10,2) DEFAULT '0.00',
  `total_net` decimal(10,2) DEFAULT '0.00',
  `total_freight` decimal(7,2) DEFAULT '0.00',
  `total_commission` decimal(20,2) DEFAULT '0.00',
  `total_discount` decimal(20,2) DEFAULT '0.00',
  `products` longtext,
  `shipping` longtext,
  `shipping_code` int,
  `shipping_info` longtext,
  -- `payment_methods` longtext,
  `reversal` BOOLEAN NOT NULL DEFAULT FALSE,
  `id_invoice` INT UNSIGNED NULL DEFAULT '0',
  `date_add` datetime DEFAULT CURRENT_TIMESTAMP,
  `date_upd` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


--
-- Estrutura da tabela `{prefix}netshoesgroup_invoice`
--

CREATE TABLE IF NOT EXISTS `{prefix}netshoesgroup_invoice` (
  `id_invoice` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sales` decimal(10,3) NOT NULL DEFAULT '0.000',
  `tax` decimal(10,3) NOT NULL DEFAULT '0.000',
  `date_end` date DEFAULT NULL,
  `date_add` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_invoice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Estrutura da tabela `{prefix}netshoesgroup_sku`
--

CREATE TABLE IF NOT EXISTS `{prefix}netshoesgroup_sku` (
  `id_sku` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(50) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `id_attribute` int(11) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `flavor` varchar(50) DEFAULT NULL,
  `product_type` varchar(50) DEFAULT NULL,
  `department` varchar(50) DEFAULT NULL,
  `size` varchar(500) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `name` varchar(500) DEFAULT NULL,
  `description` text,
  `eanIsbn` varchar(255) DEFAULT NULL,
  `height` decimal(7,3) DEFAULT NULL,
  `width` decimal(7,3) DEFAULT NULL,
  `depth` decimal(7,3) DEFAULT NULL,
  `weight` decimal(7,3) DEFAULT NULL,
  `stockQuantity` int(11) DEFAULT NULL,
  `enable` BOOLEAN NOT NULL DEFAULT TRUE,
  `price` varchar(255) DEFAULT NULL,
  `images` text,
  `id_netshoes` varchar(20) DEFAULT NULL,
  `link` text DEFAULT NULL,
  `situation` varchar(50) DEFAULT NULL,
  `variation_price`  decimal(7,3) DEFAULT NULL,
  `variation_priority` varchar(1) DEFAULT NULL,
  `price_impact` DECIMAL(7,3) NOT NULL DEFAULT '0',
  `date_add` datetime DEFAULT CURRENT_TIMESTAMP,
  `date_upd` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_sku`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `{prefix}netshoesgroup_relation` (
  `id_relation` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_sku` varchar(50) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL,
  `id_attribute` int(11) DEFAULT NULL,
  `bu_id` varchar(50) DEFAULT NULL,
  `id_netshoes` varchar(50) DEFAULT NULL,
  `variation_price`  decimal(7,3) DEFAULT NULL,
  `variation_priority` varchar(1) DEFAULT NULL,
  `price_impact` DECIMAL(7,3) NOT NULL DEFAULT '0',
  `date_add` datetime,
  `date_upd` datetime,
  PRIMARY KEY (`id_relation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
