<?php
use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\producttemplate as producttemplate;


require(dirname(__FILE__).'/../../config/config.inc.php');
$useSSL = true;
require(dirname(__FILE__).'/../../init.php');
require dirname(__FILE__).'/includes/functions.php';
require_once dirname(__FILE__).'/ApiMktpNetshoesV1.php';

$department_code = $_GET['department_code'];
$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
client\Configuration::$apiClient = $netshoesGroup;
try {
	$productTemplateApi = new producttemplate\ProductsTemplatesApi(); 
} catch (ApiException $e) {
	echo preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
}
try {
	$productTypes = $productTemplateApi->listProductTypes($department_code);
} catch (ApiException $e) {
	echo preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
}
echo json_encode($productTypes['items']);
