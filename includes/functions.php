<?php

if (!function_exists( 'debug' ) ) {
	function debug($params){
		$content = '&nbsp<pre>';
		$content.= print_r($params,true);
		$content.= '</pre>';
		return $content;
	}
}


if (!function_exists( 'logMessageNS' ) ) {
	function logMessageNS($message, $level = 1, $file = null){
		$file = ($file != null)? $file : dirname(dirname(__FILE__)).'/logs/log-'.date('Y-m-d').'.log';
		return logMessage($message, $level, $file);
	}
}


if (!function_exists( 'logMessage' ) ) {
	/**
	* Write the message in the log file
	*
	* @param string message
	* @param level  0 => 'DEBUG',1 => 'INFO',2 => 'WARNING',3 => 'ERROR',
	*/
	function logMessage($message, $level = 1, $file = null){

		$file = ($file != null)? $file : dirname(dirname(__FILE__)).'/logs/log-'.date('Y-m-d').'.log';

		if(!is_string($message)){
			$message = print_r($message, true);
		}
		if($level > 3 || $level < 0){
			$level = 0;
		}

		$level_value = array(
			0 => 'DEBUG',
			1 => 'INFO',
			2 => 'WARNING',
			3 => 'ERROR',
		);

		$formatted_message = '*'.$level_value[$level].'* '."\t".date('Y/m/d - H:i:s').': '.$message."\r\n";
		return @file_put_contents($file, $formatted_message, FILE_APPEND);
	}
    
}

if (!function_exists( 'tofloat' ) ) {
	/**
	* Conver to float number
	*
	* @param string num
	* @return float
	*/
	function tofloat($num) {
	        return floatval($num);
	}
}
if (!function_exists( 'deserializeError' ) ) {
    function deserializeError($errorJson, $apiClient)
    {

        $error = null;

        try {
            $error = $apiClient->deserialize(json_decode($errorJson), 'ErrorResource');
        } catch (\Exception $e) {
        }

        return $error;

    }
}

// if (!function_exists( 'createInvoice' ) ) {
// 	/**
// 	* Check and create invoice
// 	*
// 	* @return BOOLEAN
// 	*/
// 	function createInvoice() {
// 		$dateTime = new DateTime( date('Y-m-05'), new DateTimeZone('UTC'));
// 		$dateTime->setTime(0, 0, 0);
// 		$dateTime->modify( '-1 month' );
// 		$DB = Db::getInstance();
// 		$dateInstall = (Configuration::get('b2wmarketplace_install_date')?Configuration::get('b2wmarketplace_install_date'):false);
//     	if(date('d') < 5 || !$dateInstall)
// 			return false;
// 		$count = (int)$DB->getValue('SELECT count(*) FROM `ps_1_6_1b2wmarketplace_invoice` WHERE `date_end` = "'.$dateTime->format('Y-m-t').'"');
// 		if($count == 0){
// 			$sqlOrders = "SELECT id_order, total_amount
// 						FROM `"._DB_PREFIX_."b2wmarketplace_order` 
// 						WHERE `purchase` BETWEEN '".$dateInstall." 0:0:0' AND '".$dateTime->format('Y-m-t')." 23:59:59' 
// 						AND `status` != 'NEW' 
// 						AND `status` != 'UNAVAILABLE' 
// 						AND `status` != 'CANCELED' 
// 						AND `status` != 'NOT_APPROVED' 
// 						AND (`id_invoice` = 0 OR `id_invoice` IS NULL )";
// 			$orders = $DB->executeS($sqlOrders);
// 			if(is_array($orders) && count($orders)>0){
// 				$totalSales = 0;
// 				$ordersId = array();
// 				foreach ($orders as $key => $order) {
// 					$ordersId[] = $order['id_order'];
// 					$totalSales += $order['total_amount'];
// 				}
// 				$min_comission = floatval((Configuration::get('b2wmarketplace_min_comission')?Configuration::get('b2wmarketplace_min_comission'):0.8));
// 				$tax_comission = floatval((Configuration::get('b2wmarketplace_tax_comission')?Configuration::get('b2wmarketplace_tax_comission'):200));
				
// 				$totalInvoice = $totalSales * $tax_comission /100;
// 				$totalInvoice = ($totalInvoice > $min_comission)?$totalInvoice:$min_comission;
// 				$dataInsert = array(
// 					'sales'=>$totalSales,
// 					'tax'=>$totalInvoice,
// 					'date_end'=>$dateTime->format('Y-m-t'),
// 					'date_add'=>date('Y-m-d H:i:s'),
// 				);
// 				if($DB->insert('b2wmarketplace_invoice',$dataInsert)){
// 					$invoiceID = $DB->Insert_ID();
// 					$dataUpdateOrders = array('id_invoice' => $invoiceID,);
// 					$where = 'id_order IN('.implode($ordersId, ',').')';
// 					if(!$DB->update('b2wmarketplace_order',$dataUpdateOrders,$where)){
// 						logMessageNS(array('Erro ao atualizar pedidos com id da ordem.',$dataUpdateOrders,$where));
// 					}
// 				}else{
// 					logMessageNS(array('Erro ao criar fatura.',$dataInsert,$orders));
// 				}
// 			}
// 		}
// 	}
// 	createInvoice();
// }
