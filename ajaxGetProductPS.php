<?php
require(dirname(__FILE__).'/../../config/config.inc.php');
$useSSL = true;
require(dirname(__FILE__).'/../../init.php');
error_reporting(E_ALL);
ini_set('display_errors', '1');
$context = Context::getContext();
$id_lang = (int)$context->language->id;
$query = isset($_GET['q'])?urldecode($_GET['q']):'';

$sql = 'SELECT pl.`name`, pl.`id_product` FROM '._DB_PREFIX_.'product_lang AS pl
	JOIN '._DB_PREFIX_.'product as p ON (p.id_product = pl.id_product)
	LEFT JOIN '._DB_PREFIX_.'product_attribute as pa ON (pa.id_product = pl.id_product)
	WHERE pl.`name` LIKE "%'.$query.'%"
	OR p.`ean13` LIKE "%'.$query.'%"
	OR pa.`ean13` LIKE "%'.$query.'%"
	GROUP BY  pl.`id_product`
	ORDER BY pl.`name` ASC LIMIT 10';
$products = Db::getInstance()->ExecuteS($sql);
$return = array();
if(is_array($products) && count($products) >0){
	foreach ($products as $product) {
		$product = array(
			'product_id' => $product['id_product'],
			'name' => $product['name']
			);
		$return[] = $product;
	}
}
echo json_encode($return);