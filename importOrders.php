<?php
use ApiMktpNetshoesV1\order as order;
use ApiMktpNetshoesV1\client as client;
error_reporting(E_ALL);
ini_set('display_errors', 1);
require(dirname(__FILE__).'/../../config/config.inc.php');
$useSSL = true;
require(dirname(__FILE__).'/../../init.php');
require dirname(__FILE__).'/includes/functions.php';
require_once dirname(__FILE__).'/ApiMktpNetshoesV1.php';
$return = array(
	'error'=>false,
	'html'=>'',
);

$dateInstall = (Configuration::get('netshoesgroup_install_date')?Configuration::get('netshoesgroup_install_date'):date('Y-m-d'));

$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
client\Configuration::$apiClient = $netshoesGroup;

$page =   ((int)Tools::getValue('page') >0 )?(int)Tools::getValue('page'):0;
$start_date = date('Y-m-d H:i:s', strtotime("-30 days", strtotime($dateInstall)));
$end_date = date('Y-m-d H:i:s');
$start_date = new Datetime($start_date);
$end_date = new DateTime($end_date);
$start_date = $start_date->format(DateTime::ISO8601);
$ordersApi = new order\OrdersApi($netshoesGroup);
try {
	$orders = $ordersApi->listOrders($page, 50, array("shippings", "items", "devolutionItems"), $start_date, $end_date, null, null);
} catch (ApiException $e) {
	$return['error'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
}
if(!isset($orders)){
	$return['error'] = "Oops! Algo deu errado. Por favor, tente novamente.";
	$return['response'] = $orders;

}elseif(count($orders['items'])<1){
	$return['error'] = "Nenhum pedido foi encontrado.";
}else{
	$return['totalResults'] = count($orders['items']);
	$return['totalPages'] = ceil( count($orders['items']) / 50);
	$return['nextPage'] = ( count($orders['items']) == 50 )?($page+1): false ;
	//logMessageNS($orders['items']);
	foreach ($orders['items'] as $key => $order) {
		$dataInsert = array(
			'id_netshoes'=> $order['number'],
		  	'id_site'=> pSQL($order['origin_number']),
		  	'business_unit'=> pSQL(($order['business_unit']==1)?'NS':'ZT'),
			'devolution_requested'=> pSQL($order['devolution_requested']),
			'exchange_requested'=> pSQL($order['exchange_requested']),
			'order_date'=> pSQL(date("Y-m-d H:i:s", $order['order_date']/1000)),
			'order_type'=> pSQL($order['order_type']),
		  	'status'=> pSQL(($order['status']!= null)?$order['status']:$order['shippings']['0']['status']),
		  	'invoiced'=> NULL,
		  	'invoice_key'=> pSQL($order['shippings']['0']['invoice']['access_key']),
		  	'customer_name'=> pSQL((isset($order['shippings']['0']['customer']['customer_name'])?$order['shippings']['0']['customer']['customer_name']:NULL)),
		  	'customer'=> pSQL(Tools::jsonEncode($order['shippings']['0']['customer'],JSON_UNESCAPED_UNICODE)),
		  	'devolution_items'=>pSQL( Tools::jsonEncode($order['shippings']['0']['devolution_items'],JSON_UNESCAPED_UNICODE)) ,
		  	'total_gross'=> pSQL($order['total_gross']),
		  	'total_net'=> pSQL($order['total_net']),
		  	'total_freight'=> pSQL($order['total_freight']),
		  	'total_commission'=> pSQL($order['total_commission']),
		  	'total_discount'=> pSQL($order['total_discount']),
		  	'products'=> !isset($order['shippings']['0']['items'])?NULL:pSQL(Tools::jsonEncode($order['shippings']['0']['items'],JSON_UNESCAPED_UNICODE)),
		  	'shipping'=> !isset($order['shippings']['0'])?NULL:pSQL(Tools::jsonEncode($order['shippings']['0'],JSON_UNESCAPED_UNICODE)) ,
		  	//'payment_methods'=> !isset($order->paymentMethods)?NULL:pSQL(Tools::jsonEncode($order->paymentMethods,JSON_UNESCAPED_UNICODE)) ,
		  	'date_add'=> date('Y-m-d H:i:s'),
		  	'date_upd'=> date('Y-m-d H:i:s'),
		);
		if(Db::getInstance()->getValue('SELECT id_netshoes FROM '._DB_PREFIX_.'netshoesgroup_order WHERE id_netshoes = "'.$order['number'].'"')){
			$insert = Db::getInstance()->update('netshoesgroup_order',$dataInsert,'id_netshooes = "'.$order['number'].'"');
		}else{
			$insert = Db::getInstance()->insert('netshoesgroup_order',$dataInsert);
		}
		$insert = true;
		$return['order'][$order['number']] = array(
			'insert'=>$insert,
			'data'=>$dataInsert,
			'error'=>($insert?null:Db::getInstance()->getMsgError())
		);
	}
}
echo Tools::jsonEncode($return);
