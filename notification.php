<?php
use ApiMktpNetshoesV1\order as order;
use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\sku as sku;
ob_start();
require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once dirname(__FILE__).'/includes/functions.php';
require_once(dirname(__FILE__).'/ApiMktpNetshoesV1.php');
require_once (dirname(__FILE__).'/classes/netshoesgroupOrder.php');
require_once (dirname(__FILE__).'/classes/netshoesgroupSku.php');

$json = file_get_contents('php://input');
$json = json_decode($json,true);
$input =  $_POST + $_GET;
$error = false;
logMessageNS($json);
if(isset($json['eventAction']) && $json['eventAction'] == 'PEDIDO') {
	if($json['enventCode'] == 'PEDIDO_ALTERADO') {
		$order = FALSE;
		$idOrderNetshoes = false;
		if(isset($json['parameters']['orderNumber'])){
			$idOrderNetshoes = $json['parameters']['orderNumber'];
			$order = netshoesgroupOrder::getOrderByNGId($json['parameters']['orderNumber']);
		}
		if (!$order){
			$order = new netshoesgroupOrder();
		}
		$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
		client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
		client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
		client\Configuration::$apiClient = $netshoesGroup;
		$ordersApi = new order\OrdersApi($netshoesGroup);						
		try {
			$response = $this->ordersApi->getOrder($idOrderNetshoes, '');
		} catch (ApiException $e) {
			$error = true;
			logMessageNS(array('Erro ao buscar informações sobre o pedido.',$response));
		}
		//logMessageNS($response);
		try{
			    $order->id_site = isset($response['origin_number'])?$response['origin_number']:NULL;
			    $order->business_unit = ($response['business_unit'] == 1)?'NS':'ZT';
			    $order->devolution_requested = isset($response['devolution_requested'])?$response['devolution_requested']:NULL;
			    $order->exchange_requested = isset($response['exchange_requested'])?$response['exchange_requested']:NULL;
			    $order->order_date = isset($response['order_date'])?date("Y-m-d H:i:s", ($response['order_date']/1000)):NULL;
			    $order->order_type = isset($response['order_type'])?$response['order_type']:NULL;
			    $order->status = ($response['status']!= null)?$response['status']:$response['shippings']['0']['status'];
			    $order->invoice_key = isset($response['shippings']['invoice']['access_key'])?$response['shippings']['0']['invoice']['access_key']:NULL;
			    $order->customer_name = isset($response['shippings']['0']['customer']['customer_name'])?$response['shippings']['0']['customer']['customer_name']:NULL;
			    $order->customer = isset($response['shippings']['0']['customer'])?$response['shippings']['0']['customer']:NULL;
			    $order->devolution_items = isset($response['shippings']['0']['devolution_items'])?$response['shippings']['0']['devolution_items']:NULL;
			    $order->total_gross = isset($response['total_gross'])?$response['total_gross']:NULL;
			    $order->total_net = isset($response['total_net'])?$response['total_net']:NULL;
			    $order->total_freight = isset($response['total_freight'])?$response['total_freight']:NULL;
			    $order->total_commission = isset($response['total_commission'])?$response['total_commission']:NULL;
			    $order->total_discount = isset($response['total_discount'])?$response['total_discount']:NULL;
			    $order->products = isset($response['shippings']['0']['items'])?$response['shippings']['0']['items']:NULL;
			    $order->shipping = isset($response['shippings']['0'])?$response['shippings']['0']:NULL;
			// order->payment_methods = isset($response['body']->paymentMethods)?$response['body']->paymentMethods:NULL;
			$result = $order->update();
		} catch (Exception $e) {
			 logMessageNS($e->getMessage());
		}
		if (!$result) {
			$error = true;
			logMessageNS(array('Erro ao salvar dados do pedido no banco.',$e->getMessage(),$order));
		}
	}
} elseif(isset($json['eventAction']) && $json['eventAction'] == 'PRODUTO') {
	if ($json['eventCode'] == 'PRODUTO_LIBERADO') {
			$sku = FALSE;
			$idSku = false;
			if(isset($json['sku'])){
				$idSku = $json['sku'];
				$sku = netshoesgroupSku::getSkuByNetshoesId($json['sku']);
			}
			//logMessageNS($sku);
			$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
			client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
			client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
			client\Configuration::$apiClient = $netshoesGroup;
			client\Configuration::$debug = false;
			$skusApi = new sku\SkusApi($netshoesGroup);	
			$body = array('country' => 'BR', 'price' => $sku->price->listPrice, 'price' => $sku->price->sellPrice);
			logMessageNS($body);
			try {
				$skusApi->updatePrice($sku->sku, $body);
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			$bodyStock = array('country' => 'BR', 'available' => $sku->stockQuantity);
			try {
				$skusApi->updateStock($sku->sku, $bodyStock);
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			$sku->situation = 'RELEASED_MATCH';
			$sku->update(); 	
	} else {
		$error = true;
		logMessageNS(array('Erro na notificação. PRODUTO_LIBERADO','input' =>$json));
	}
} else {
	$error = true;
	logMessageNS(array('Erro na notificação','input' =>$input));
}
$resultado = trim(ob_get_contents());
ob_end_clean();
http_response_code(($error? 422: 200));
echo $resultado;
