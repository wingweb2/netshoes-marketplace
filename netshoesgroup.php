<?php
use ApiMktpNetshoesV1\product as productapi;
use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\sku as sku;
use ApiMktpNetshoesV1\order as order;
require_once dirname(__FILE__).'/includes/functions.php';

if (!defined('_PS_VERSION_'))
	exit;

// Loading API Netshoes
require_once(dirname(__FILE__).'/ApiMktpNetshoesV1.php');

// Loading module Classes
require_once(dirname(__FILE__).'/classes/netshoesgroupSku.php');
require_once(dirname(__FILE__).'/classes/netshoesgroupOrder.php');

/**
  * Netshoes Group
  * @category marketplace
  *
  * @author Wing Agency - wingagency.com.br
  * @copyright Wing Agency / PrestaShop
  * @license 
  * @version 1.0
  */
class netshoesgroup  extends Module
{
    private $skuApi;
    private $netshoesGroup;
	public function __construct()
	{
		$this->name = 'netshoesgroup';
		$this->tab = 'marketplace';
		$this->version = '1.0';
		$this->displayName = $this->l('Netshoes group Marketplace');
		$this->author = 'Wing Agency';
		$this->description = $this->l('Integration module with the marketplace Netshoes Group.');
	
		$this->bootstrap = true;
		parent::__construct();
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->prefix = $this->name.'_';
		$this->html = null;
	}

	public function install()
	{
		if (!parent::install()
			|| !$this->installDB()
			|| !$this->installTabs()
			|| !$this->registerHook('displayBackOfficeHeader')
			|| !$this->registerHook('updateproduct')
			|| !$this->registerHook('actionUpdateQuantity')
			){
			return false;
		}
		return true;
	}


	public function uninstall()
	{
		if (!parent::uninstall()
			 || !$this->unregisterHook('displayBackOfficeHeader')
			 || !$this->unregisterHook('updateproduct')
			 || !$this->unregisterHook('actionUpdateQuantity')
			)
			return false;
		return true;
	}
	
	private function installDB() {
		$sql = file_get_contents(dirname(__FILE__).'/install.sql');
		$sql = str_replace('{prefix}', _DB_PREFIX_, $sql);
		return Db::getInstance()->execute($sql);
	}
	
	private function uninstallDb()
	{
		return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.$this->name.'_relation`');
	}
	
	
	private function installTabs(){
		$languages = Language::getLanguages();
		$id_parent = Tab::getIdFromClassName('AdminMarketplace');
		if (!$id_parent){
			$tab = new Tab();
			$tab->class_name = 'AdminMarketplace';
			$tab->id_parent = 0;
			$tab->module = '';
			foreach ($languages as $language)
			        $tab->name[$language['id_lang']] = 'Marketplace';
			$tab->add();
			$id_parent = $tab->id;
		}
		$moduleName = 'NetshoesGroup';
		if(!Tab::getIdFromClassName(moduleName.'SKU')){
			$tab = new Tab();
			$tab->class_name = $moduleName.'SKU';
			$tab->id_parent = $id_parent;
			$tab->module = $this->name;
			foreach ($languages as $language)
			        $tab->name[$language['id_lang']] = $this->displayName;
			$tab->add();
		}
		$tabs = $arrayName = array(
			$moduleName.'SKU'=> 'SKU - '.$this->displayName,
			$moduleName.'Products'=> 'Produtos - '.$this->displayName,
			$moduleName.'Orders'=> 'Pedidos - '.$this->displayName,
		);
		foreach ($tabs as $key => $value) {
			if(!Tab::getIdFromClassName(key)){
				$tab = new Tab();
				$tab->class_name = $key;
				$tab->id_parent = '-1';
				$tab->module = $this->name;
				foreach ($languages as $language)
				        $tab->name[$language['id_lang']] = $value;
				$tab->add();
			}
		}
		return true;
	}
	
	
	public function getContent()
	{
		$this->html = '';
		if(Tools::getIsset('truncateTable')){
			Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'netshoesgroup_order;');
		}
		if(Tools::getIsset('getOrders')){
			$this->context->smarty->assign('importOrdersUrl', Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/importOrders.php'); 
			$this->html = $this->display(__FILE__, 'views/templates/admin/importOrders.tpl');
			return $this->html;
		}
		if(Tools::getIsset('truncateTableSKU')){
			Db::getInstance()->execute('TRUNCATE TABLE '._DB_PREFIX_.'netshoesgroup_sku;');
		}
		if(Tools::getIsset('getSkus')){
			$this->context->smarty->assign('importSkusUrl', Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/importSkus.php'); 
			$this->html = $this->display(__FILE__, 'views/templates/admin/importSkus.tpl');
			return $this->html;
		}
		if(Tools::getIsset('getInvoice')){
			$this->getInvoice();
			
			// $this->context->smarty->assign('importSkusUrl', Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/importSkus.php'); 
			// $this->html = $this->display(__FILE__, 'views/templates/admin/importSkus.tpl');
			return $this->html;
			
		}
		
		// If we try to update the settings
		if (Tools::isSubmit('submitModule'))
		{
			Configuration::updateValue($this->prefix.'min_comission', Tools::getValue($this->prefix.'min_comission'));
			Configuration::updateValue($this->prefix.'tax_comission', Tools::getValue($this->prefix.'tax_comission'));
			Configuration::updateValue($this->prefix.'install_date', Tools::getValue($this->prefix.'install_date'));
			Configuration::updateValue($this->prefix.'access_token', Tools::getValue($this->prefix.'access_token'));
			Configuration::updateValue($this->prefix.'client_id', Tools::getValue($this->prefix.'client_id'));
			//Configuration::updateValue($this->prefix.'seller', Tools::getValue($this->prefix.'seller'));
			Configuration::updateValue($this->prefix.'version', Tools::getValue($this->prefix.'version'));
		}
		if(Tools::isSubmit('submitCategory')){
			Configuration::updateValue($this->prefix.'variation_price'. Tools::getValue('id_category'), str_replace(',','.', Tools::getValue('variation_price')));
		}
		$this->netshoesGroup = new client\ApiClient((Configuration::get($this->prefix.'version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
					client\Configuration::$apiKey['client_id'] = Configuration::get($this->prefix.'client_id');
					client\Configuration::$apiKey['access_token'] = Configuration::get($this->prefix.'access_token');
					client\Configuration::$apiClient = $this->netshoesGroup;
					$dateInstall = (Configuration::get('netshoesgroup_install_date')?Configuration::get('netshoesgroup_install_date'):date('Y-m-d'));
		$this->html .= $this->renderStats();
		$this->html .= $this->renderForm();
		$this->html .= $this->initList();
		return $this->html;

	}


	private function renderForm()
	{

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'hidden',
						'name' => $this->prefix.'tax_comission',
						'required' => true,
					),
					array(
						'type' => 'hidden',
						'name' => $this->prefix.'min_comission',
						'required' => true,
					),
					// array(
					// 	'type' => 'hidden',
					// 	'name' => $this->prefix.'install_date',
					// 	'required' => true,
					// ),
					// array(
						// 'type' => 'date',
						// 'label' => $this->l('Data instalação:'),
						// 'name' => $this->prefix.'install_date',
						// 'size' => 10,
						// 'required' => true,
						// 'desc' => $this->l('Data de para base da busca e cobrança da comissão mensal.'),
					// ),
					array(
						'type' => 'text',
						'label' => $this->l('Token de acesso:'),
						'name' => $this->prefix.'access_token',
						'required' => true,
					),
					array(
						'type' => 'text',
						'label' => $this->l('ID do cliente:'),
						'name' => $this->prefix.'client_id',
						'required' => true,
					),
					// array(
					// 	'type' => 'text',
					// 	'label' => $this->l('Vendedor:'),
					// 	'name' => $this->prefix.'seller',
					// 	'required' => true,
					// ),
					array(
						'type' => 'switch',
						'label' => $this->l('Production version'),
						'name' => $this->prefix.'version',
						'desc' => $this->l('Select yes to go into production mode (MARKETPLACE) or not to test mode (sandbox).'),
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
							),
							array(
								'id' => 'active_off',
								'value' => 0,
							)
						),
					),
					array(
						'type'=>'free',
						'name' => 'advanced_help'
					),
					array(
						'type'=>'free',
						'name' => 'notification_helper'
					),
				),
				'submit' => array(
					'title' => $this->l('Save')
				)
			),
		);
		

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table =  $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitModule';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	private function getConfigFieldsValues()
	{
		$advanced_help = null;
		$notification_helper = null;
		if(Configuration::get($this->prefix.'access_token') != ''){
			$urlNotification = Tools::getHttpHost(true).__PS_BASE_URI__.'modules/'.$this->name.'/notification.php';
			$totalOrders = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'netshoesgroup_order');
			$currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&token='.Tools::getAdminTokenLite('AdminModules').'&configure='.$this->name.'&module_name='.$this->name;
			if($totalOrders > 0 ){
				$advanced_help .= '<div class="alert alert-info col-lg-9 ">
					<p>Encontramos  '.$totalOrders.' pedidos sincronizados.</p>
					<br />
					<ul class="list-unstyled">
						<li><a href="'.$currentIndex.'&truncateTable" class="btn btn-danger">Excuir Pedidos</a></li>
					</ul>
				</div>';
			}else{
				$advanced_help .= '<div class="alert alert-danger col-lg-9 ">
					<p>Não encontrados nenhum pedido sincronizado.</p>
					<br />
					<ul class="list-unstyled">
						<li><a href="'.$currentIndex.'&getOrders" class="btn btn-info">Sincronizar Pedidos</a></li>
						<li>Os pedidos serão sincronizados a partir da data '.date('d-m-Y', strtotime("-30 days")).'.</li>
						<li>A sincronização dos pedidos não dará baixa no estoque dos produtos.</li>
					</ul>
				</div>';
			}
			$totalSku = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'netshoesgroup_sku');
			if($totalSku > 0 ){
				$advanced_help .= '<div class="alert alert-info col-lg-9 ">
					<p>Encontramos  '.$totalSku.' Anúncios sincronizados.</p>
					<br />
					<ul class="list-unstyled">
						<li><a href="'.$currentIndex.'&truncateTableSKU" class="btn btn-danger">Excuir Anúncios</a></li>
						<li><a href="'.$currentIndex.'&getSkus" class="btn btn-primary">Sincronizar Anúncios</a></li>
					</ul>
				</div>';
			}else{
				$advanced_help .= '<div class="alert alert-danger col-lg-9 ">
					<p>Não encontrados nenhum anúncios sincronizado.</p>
					<br />
					<ul class="list-unstyled">
						<li><a href="'.$currentIndex.'&getSkus" class="btn btn-info">Sincronizar Anúncios</a></li>
						<li>A sincronização dos anúncios não fara a associação com os produtos.</li>
					</ul>
				</div>';
			}
		}

		return array(
			'notification_helper' => $notification_helper,
			'advanced_help' => $advanced_help,
			$this->prefix.'min_comission' => (Configuration::get($this->prefix.'min_comission')?Configuration::get($this->prefix.'min_comission'):200),
			$this->prefix.'tax_comission' => (Configuration::get($this->prefix.'tax_comission')?Configuration::get($this->prefix.'tax_comission'):0.8),
			$this->prefix.'install_date' => (Configuration::get($this->prefix.'install_date')?Configuration::get($this->prefix.'install_date'):date('Y-m-d')),
			$this->prefix.'access_token' => Configuration::get($this->prefix.'access_token'),
			$this->prefix.'client_id' => Configuration::get($this->prefix.'client_id'),
			//$this->prefix.'seller' => Configuration::get($this->prefix.'seller'),
			$this->prefix.'version' => Configuration::get($this->prefix.'version')
		);
	}
	
	private function initList()
    {

    	if ((int)Tools::isSubmit('submitCategory')){
    		$id_category = (int)Tools::getValue('id_category');
    		$value = (float)Tools::getValue('value');
    		Configuration::updateValue($this->prefix.'variation_price'.$id_category,$value);
    	}
    	
    	if ((int)Tools::isSubmit('submitUpdate')){
    		$id_category = (int)Tools::getValue('id_category');
    		$categoryTax = (float)Tools::getValue('value');
    		Configuration::updateValue($this->prefix.'variation_price'.$id_category,$categoryTax);
    		// $sql = 'SELECT p.`id_product`,ng.`id_sku` FROM `'._DB_PREFIX_.'product` p ';
    		// $sql .= 'JOIN `'._DB_PREFIX_.'netshoesgroup_sku` ng ON (ng.`id_product` = p.`id_product`)';
    		// $sql .= ($id_category ? 'INNER JOIN `'._DB_PREFIX_.'category_product` c ON (c.`id_product` = p.`id_product` AND c.`id_category` = ' . $id_category .') ': '');
    		$sql = 'SELECT ng.`id_sku` FROM `'._DB_PREFIX_.'netshoesgroup_sku` AS ng
						INNER JOIN `'._DB_PREFIX_.'category_product` c ON (c.`id_product` = ng.`id_product` )
					 	WHERE ng.`variation_priority` = "c" AND c.`id_category` = '.$id_category ;

    		$products = Db::getInstance()->executeS($sql);
    		if(is_array($products) && count($products) > 0){
    			foreach ($products as &$product) {
    				
					$sku = new netshoesgroupSku($product['id_sku']);
					$price = Product::getPriceStatic($sku->id_product, true, $sku->id_attribute, 2);
					$sku->price->sellPrice = $price + ($price * $categoryTax / 100)+ $sku->price_impact;
					$sku->price->listPrice = number_format((($sku->price->listPrice < $sku->price->sellPrice)? $sku->price->sellPrice:$sku->price->listPrice),2,'.','');
					$sku->price->sellPrice = number_format($sku->price->sellPrice,2,'.','');
					$this->netshoesGroup = new client\ApiClient((Configuration::get($this->prefix.'version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
					client\Configuration::$apiKey['client_id'] = Configuration::get($this->prefix.'client_id');
					client\Configuration::$apiKey['access_token'] = Configuration::get($this->prefix.'access_token');
					client\Configuration::$apiClient = $this->netshoesGroup;
					client\Configuration::$debug = false;
					$this->skuApi = new sku\SkusApi($this->netshoesGroup);
					$error = array();
					$bodyPrice = array(
						"sellPrice"=> $sku->price->sellPrice,
						"listPrice"=> $sku->price->listPrice,
					);
					try {
						$response = $this->skuApi->updatePrice($sku->sku, $bodyPrice);
					} catch (ApiException $e) {
						$error['price'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
					}
					if(!empty($error['price'])) {
						logMessageNS(array('Erro ao atualizar preço pela listagem de categorias e porcentagem',$sku->sku,$bodyPrice,$response));
					}
					try{
						$result = $sku->update();
					} catch (Exception $e) {
						logMessageNS(array('Erro ao atualizar SKU ('.$sku->sku.') no BANCO.',$e->getMessage()),3, $this->logFile);
					}
					if(!$result){
						logMessageNS(array('Erro ao atualizar SKU ('.$sku->sku.') no BANCO.',Db::getInstance()->getMsgError()),3, $this->logFile);
					}
    			}
    		}

			Tools::redirectAdmin(AdminController::$currentIndex.'&configure=netshoesgroup&token='.$_GET['token']);
    		die();
    	}

    	$this->context->smarty->assign('list', $this->populateCategories());
    	return $this->display(__FILE__, 'views/templates/admin/listCategory.tpl');
    }

	public function hookDisplayBackOfficeHeader()
	{
	   $this->context->controller->addCss($this->_path.'css/tab.css');
	}
	
    private function populateCategories( $categories = array(), $breadcrumb = null,$return = array()){
        if(!is_array($categories) || count($categories) <1 ){
	    	$categories = Category::getNestedCategories(Category::getRootCategory()->id);
			$categories = $categories[Category::getRootCategory()->id]['children'];
		}
		foreach ($categories as $category) {
			$return[] = array(
	            'id_category' => $category['id_category'],
	            'name' => $category['name'],
	            'breadcrumb' => $breadcrumb. $category['name'],
	            'variation_price'=> Configuration::get($this->prefix.'variation_price'.$category['id_category']),
            );
			if(isset($category['children']) && is_array($category['children'])){
				$breadcrumb .= $category['name'].' > ';
                $return = $this->populateCategories($category['children'], $breadcrumb,$return);
			}
		}
		return $return;
    }
	
	
	public function hookActionUpdateQuantity($params)
	{
		$idProduct = (int)$params['id_product'];
        $idAttribute = (int)$params['id_product_attribute'];
        	
		$error = array();
		
		
		//logMessageNS(array('hookActionUpdateQuantity.',$idProduct, $idAttribute, $available_quantity));
		
		
    	$sku = netshoesgroupSku::getSkuByProduct($idProduct,$idAttribute);
		if(!$sku || !$sku->enable){
			return;
		}
		$available_quantity = ($sku->brand == 'Oakley')?0:(int)$params['quantity'];
		logMessageNS(array('hookActionUpdateQuantity.',$idProduct, $idAttribute, $available_quantity, $sku->sku));
		
		
		$this->netshoesGroup = new client\ApiClient((Configuration::get($this->prefix.'version') == 1 ? 'http://api-marketplace.netshoes.com.br/api/v1' : 'http://api-sandbox.netshoes.com.br/api/v1' ));

		client\Configuration::$apiKey['client_id'] = Configuration::get($this->prefix.'client_id');
		client\Configuration::$apiKey['access_token'] = Configuration::get($this->prefix.'access_token');
		client\Configuration::$apiClient = $this->netshoesGroup;
		client\Configuration::$debug = false;
		$this->skuApi = new sku\SkusApi($this->netshoesGroup);
//		$available_quantity = $available_quantity + ($sku->stockVariation);
		$bodyStock = array("country"=> 'BR', "available"=> $available_quantity);
		try {
			$response = $this->skuApi->updateStock($sku->sku, $bodyStock);
		} catch (ApiException $e) {
			$error['stock'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
		}
		if (!empty($error['stock'])){
			logMessageNS(array('Erro ao atualizar estoque pela API.',$sku->sku,$bodyStock,$response));
		}
		logMessageNS(array('ok ao atualizar estoque pela API.',$sku->sku,$bodyStock,$response));
		try{
			$sku->stockQuantity = $available_quantity;
			$result = $sku->update();
		} catch (Exception $e) {
			logMessageNS(array('Erro ao atualizar estoque no BANCO. SKU = '.$sku->sku,$e->getMessage()));
		}
		if(!$result){
			logMessageNS(array('Erro ao atualizar estoque no BANCO. SKU = '.$sku->sku,Db::getInstance()->getMsgError()));
		}
		return;
	}


	public function hookUpdateProduct($params)
	{
		$product = null;
		if (is_a($params['product'], 'Product') && (int)$params['product']->id > 0){
			$product = $params['product'];
		}elseif(isset($params['id_product']) && (int)$params['id_product'] > 0){
			$product = new Product($params['id_product'],true,Context::getContext()->language->id);
		}elseif(isset($params['id_product']) && (int)$params['id_product'] > 0){
			$product = new Product($params['product']['id_product'],true,Context::getContext()->language->id);
		}
		
		if(is_object($product) && (int)$product->id > 0){

			Cache::clean('StockAvailable::getQuantityAvailableByProduct_'.(int)$product->id.'*');

		    	$sql = "SELECT id_sku FROM "._DB_PREFIX_."netshoesgroup_sku WHERE id_product = '".$product->id."'";// AND id_attribute = '".$product->id_product_attribute."'";
		    	$results = Db::getInstance()->ExecuteS($sql);
			
		    	if(!is_array($results) || count($results) < 1){
		    		return;
		    	}
			$product->tax_rate = $product->getTaxesRate(NULL);
			$this->netshoesGroup = new client\ApiClient((Configuration::get($this->prefix.'version') == 1 ? 'http://api-marketplace.netshoes.com.br/api/v1' : 'http://api-sandbox.netshoes.com.br/api/v1' ));
			client\Configuration::$apiKey['client_id'] = Configuration::get($this->prefix.'client_id');
			client\Configuration::$apiKey['access_token'] = Configuration::get($this->prefix.'access_token');
			client\Configuration::$apiClient = $this->netshoesGroup;
			client\Configuration::$debug = false;
			$this->skuApi = new sku\SkusApi($this->netshoesGroup);
			$error = array();
			$categoryTax = floatval(Configuration::get($this->prefix.'variation_price'.$product->id_category_default));
			foreach ($results as $row) {
				$sku = new netshoesgroupSku($row['id_sku']);
				
				if($sku->enable)
				{
					//if($sku->variation_priority != 'l'){
						/*$sku->price->sellPrice = Product::getPriceStatic($sku->id_product, true, $sku->id_attribute, 2);
						$sku->price->listPrice = tofloat(($product->tax_rate > 0 && $product->unit_price >0)?$product->unit_price + ($product->unit_price * $product->tax_rate /100) :$sku->price->sellPrice);
						if($sku->variation_priority == 'p' && $sku->variation_price >0 ){
							$sku->price->sellPrice = $sku->price->sellPrice + ($sku->price->sellPrice * $sku->variation_price / 100)+ $sku->price_impact;
						}else{
							$sku->price->sellPrice = $sku->price->sellPrice + ($sku->price->sellPrice * $categoryTax / 100)+ $sku->price_impact;
						}
	
						$sku->price->listPrice = number_format((($sku->price->listPrice < $sku->price->sellPrice)? $sku->price->sellPrice:$sku->price->listPrice),2,'.','');
						$sku->price->sellPrice = number_format($sku->price->sellPrice,2,'.','');
						$bodyPrice = array(
							"sellPrice"=> $sku->price->sellPrice,
							"listPrice"=> $sku->price->listPrice,
						);
						try {
							$response = $this->skuApi->updatePrice($sku->sku, $bodyPrice);
						} catch (ApiException $e) {
							$error['price'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
						}
						if(!empty($error['price'])) {
							logMessageNS(array('Erro ao atualizar preço pelo hookUpdateProduct',$sku->sku,$bodyPrice,$response));
						}*/
						//$sku->stockQuantity = (StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute) ) + ($sku->stockVariation);
					$sku->stockQuantity = ($sku->brand == 'Oakley')?0:(StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute) );
					
					$sku->update();
					//}					
					$bodyStock = array("country"=> 'BR', "available"=> $sku->stockQuantity);
					if ( !$this->skuApi->updateStock($sku->sku, $bodyStock) ) {
						logMessageNS(array('Erro ao atualizar estoque pela API.',$sku->sku,$bodyStock,$response));
					}
					//logMessageNS(array('ok ao atualizar estoque pela API.',$sku->sku,$bodyStock,$response));
					$response = null;
					if(!$product->active){
						$bodyEnable = array("active" => false);
						try {
							$response = $this->skuApi->updateStatus($sku->sku, $sku->business_id, $bodyEnable);
						} catch (ApiException $e) {
							$error['enable'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
						}
						if(!empty($error['enable'])) {
							logMessageNS(array('Erro ao atualizar status pelo hookUpdateProduct',$sku->sku,$bodyEnable,$response));
						}
						//logMessageNS(array('ok atualizar status pelo hookUpdateProduct',$sku->sku,$bodyEnable,$response));
					}
					$result = null;
					try{
						$result = $sku->update();
					} catch (Exception $e) {
						logMessageNS(array('Erro ao atualizar SKU ('.$sku->sku.') no BANCO.',$e->getMessage()));
					}
					if(!$result){
						logMessageNS(array('Erro ao atualizar SKU ('.$sku->sku.') no BANCO.',Db::getInstance()->getMsgError()));
					}
				}
				elseif (!$sku->enable && $sku->situation == "RELEASED_MATCH")
				{
					if($product->active)
					{
					    $sku->enable = true;
						$sku->stockQuantity = ($sku->brand == 'Oakley')?0:(StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute) );
					
						$sku->update();
						//}					
						$bodyStock = array("country"=> 'BR', "available"=> $sku->stockQuantity);
						if ( !$this->skuApi->updateStock($sku->sku, $bodyStock) ) {
							logMessageNS(array('Erro ao atualizar estoque pela API.',$sku->sku,$bodyStock,$response));
						}
						$bodyEnable = array("active" => true);
						try {
							$response = $this->skuApi->updateStatus($sku->sku, $sku->business_id, $bodyEnable);
						} catch (ApiException $e) {
							$error['enable'] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
						}
						if(!empty($error['enable'])) {
							logMessageNS(array('Erro ao atualizar status pelo hookUpdateProduct',$sku->sku,$bodyEnable,$response));
						}
						//logMessageNS(array('ok atualizar status pelo hookUpdateProduct',$sku->sku,$bodyEnable,$response));
					}
					$result = null;
					try{
						$result = $sku->update();
					} catch (Exception $e) {
						logMessageNS(array('Erro ao atualizar SKU ('.$sku->sku.') no BANCO.',$e->getMessage()));
					}
					if(!$result){
						logMessageNS(array('Erro ao atualizar SKU ('.$sku->sku.') no BANCO.',Db::getInstance()->getMsgError()));
					}
				}
			}
		}

    	return;
	}

	private function renderStats(){
		
		$totalOrders = Db::getInstance()->getValue('SELECT COUNT(*) FROM '._DB_PREFIX_.'netshoesgroup_order');
		if($totalOrders){
			$dateStart = Tools::getValue('dateStart',date('Y-m-1'));
			$dateEnd = Tools::getValue('dateEnd',date('Y-m-t'));
			$dateTime = new DateTime( $dateStart, new DateTimeZone('UTC'));
			$dateTime->setTime(0, 0, 0);
			$sqlCountOrders = "SELECT LEFT(`order_date`, 10) as date, COUNT(*) as orders 
						FROM `"._DB_PREFIX_."netshoesgroup_order` 
						WHERE `order_date` BETWEEN '".$dateStart." 0:0:0' AND '".$dateEnd." 23:59:59' 
						AND `status` != 'canceled' 
						GROUP BY LEFT(`order_date`, 10)";
			$totalCountOrders = Db::getInstance()->executeS($sqlCountOrders);
			$sqlOrders = "SELECT LEFT(`order_date`, 10) AS date, SUM(`total_gross`) AS sale
						FROM `"._DB_PREFIX_."netshoesgroup_order` 
						WHERE `order_date` BETWEEN '".$dateStart." 0:0:0' AND '".$dateEnd." 23:59:59'  
						AND `status` != 'canceled'  
						GROUP BY LEFT(`order_date`, 10)";
			$totalSales = Db::getInstance()->executeS($sqlOrders);
			$orders = array();
			$sales = array();
			foreach ($totalSales as $row){
				$dateTime->setTimestamp(strtotime($row['date']))->setTime(9, 0, 0);
				$sales[$dateTime->getTimestamp()] = $row['sale'];
			}
			foreach ($totalCountOrders as $row){
				$dateTime->setTimestamp(strtotime($row['date']))->setTime(9, 0, 0);
				$orders[$dateTime->getTimestamp()] = $row['orders'];
			}
				
			
			$from = $dateTime->setTimestamp(strtotime($dateStart))->setTime(0, 0, 0)->getTimestamp();
			$to = min(time(), $dateTime->setTimestamp(strtotime($dateEnd))->setTime(23, 59, 59)->getTimestamp());
			
			$data = array();
			$data['sales']['total'] = 0;
			$data['sales']['id'] = 'sales';
			$data['sales']['key'] = 'Vendas';
			$data['sales']['color'] = '#1777B6';
			
			$data['orders']['total'] = 0;
			$data['orders']['id'] = 'orders';
			$data['orders']['key'] = 'Pedidos';
			$data['orders']['color'] = '#2CA121';
			
			$data['sales']['valuesFormated'] = '[';
			$data['orders']['valuesFormated'] = '[';

			$count = 0;
			for ($date = $dateTime->setTimestamp(strtotime($dateStart))->setTime(9, 0, 0); $date->getTimestamp() < $to; $date->modify( '+1 day' )) {
				$timestamp = $date->getTimestamp();
				$data['sales']['valuesFormated'] .= ($count >0 )?',':'';
				$data['orders']['valuesFormated'] .= ($count >0 )?',':'';
				$count++;
				$data['sales']['values'][$timestamp] = tofloat(isset($sales[$timestamp]) ? $sales[$timestamp] : 0);
				$data['sales']['total'] += $data['sales']['values'][$timestamp];
				$data['orders']['values'][$timestamp] = intval(isset($orders[$timestamp]) ? $orders[$timestamp] : 0);
				$data['orders']['total'] += $data['orders']['values'][$timestamp];
				
				$data['sales']['valuesFormated'] .= '{x:'.$timestamp.',y:'.$data['sales']['values'][$timestamp].'}';
				$data['orders']['valuesFormated'] .= '{x:'.$timestamp.',y:'.$data['orders']['values'][$timestamp].'}';
			}
			$data['sales']['valuesFormated'] .= ']';
			$data['orders']['valuesFormated'] .= ']';
			
		 	$this->admin_webpath = str_ireplace(_PS_CORE_DIR_, '', _PS_ADMIN_DIR_);
        	$this->admin_webpath = preg_replace('/^'.preg_quote(DIRECTORY_SEPARATOR, '/').'/', '', $this->admin_webpath);
        	$this->bo_theme = ((Validate::isLoadedObject($this->context->employee)&& $this->context->employee->bo_theme) ? $this->context->employee->bo_theme : 'default');
	        
			$this->context->controller->addJs(array(
	            _PS_JS_DIR_.'vendor/d3.v3.min.js',
	            // _PS_JS_DIR_.'date.js',
	            __PS_BASE_URI__.$this->admin_webpath.'/themes/'.$this->bo_theme.'/js/vendor/nv.d3.min.js',
	        ));
	        $this->context->controller->addCss(__PS_BASE_URI__.$this->admin_webpath.'/themes/'.$this->bo_theme.'/css/vendor/nv.d3.css');
			$formAction = $this->context->link->getAdminLink('AdminModules', false).'&token='.Tools::getAdminTokenLite('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
			$this->context->smarty->assign('data', $data);
			$this->context->smarty->assign('formAction', $formAction);
			$this->context->smarty->assign('dateStart', $dateStart);
			$this->context->smarty->assign('dateEnd', $dateEnd);
	    	return $this->display(__FILE__, 'views/templates/admin/stats.tpl');
		}
		
	}

	public function getInvoice()
	{
		if(Tools::getIsset('deletenetshoesgroup_invoice')){
			$idInvoice = Tools::getValue('id_invoice');
			if((int)$idInvoice > 0){
				if(!Db::getInstance()->delete('netshoesgroup_invoice','id_invoice = '.$idInvoice)){
					$this->html .= $this->displayError('Erro ao remover fatura '.$idInvoice);
					logMessageNS(array('erro ao remover fatura', Db::getInstance()->getMsgError()));
				}else{
					if(!Db::getInstance()->update('netshoesgroup_order', array('id_invoice'=>0), 'id_invoice = '.$idInvoice )){
						$this->html .= $this->displayError('Erro ao remover id da fatura '.$idInvoice.' nos pedidos.');
						logMessageNS(array('Erro ao remover id da fatura '.$idInvoice.' nos pedidos.', Db::getInstance()->getMsgError()));
					}else{
						$this->html .= $this->displayConfirmation($this->l('Fatura removida com sucesso.'));
						$currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&module_name='.$this->name.'&getInvoice&token='.Tools::getAdminTokenLite('AdminModules');
						Tools::redirectAdmin($currentIndex);
    					die();
					}
				}
			}
		}
		$orderBy = (Tools::getValue('netshoesgroup_invoiceOrderby')?Tools::getValue('netshoesgroup_invoiceOrderby'):'id_invoice');
		$orderWay = (Tools::getValue('netshoesgroup_invoiceOrderway')?Tools::getValue('netshoesgroup_invoiceOrderway'):'DESC');
		$where = null;
		$sql = 'SELECT * FROM '._DB_PREFIX_.'netshoesgroup_invoice ';
		$sql .= ' WHERE 1 '.$where;
		$sql .= ' ORDER BY '.$orderBy.' '.$orderWay;
		$invoices = Db::getInstance()->executeS($sql);
		$totalInvoices = Db::getInstance()->getValue('SELECT count(*) FROM '._DB_PREFIX_.'netshoesgroup_invoice WHERE 1'.$where);
		
		$fields_list = array();
		$fields_list['id_invoice'] = array(
			'title' => $this->l('ID'),
			'align' => 'center',
			'width' => 'auto',
		);
		$fields_list['sales'] = array(
			'title' => $this->l('Vendas'),
			'type' => 'price',
			'align' => 'text-right',
			'filter' => false,
			'search' => false
		);
		$fields_list['tax'] = array(
			'title' => $this->l('Total Taxa'),
			'type' => 'price',
			'align' => 'text-right',
			'filter' => false,
			'search' => false
		);
		$fields_list['date_end'] = array(
			'title' => $this->l('Data Limite'),
            'align' => 'text-right',
            'type' => 'date',
			'filter_key' => 'date_end',
			'filter' => false,
			'search' => false
		);
		$fields_list['date_add'] = array(
			'title' => $this->l('Data Cadastro'),
            'align' => 'text-right',
            'type' => 'datetime',
			'filter_key' => 'date_add',
			'filter' => false,
			'search' => false
		);
		
		$helper = new HelperList();
	    $helper->listTotal= $totalInvoices;
	    $helper->shopLinkType = '';
	     
	    $helper->simple_header = false;
	    
	    // Actions to be displayed in the "Actions" column
	    $helper->actions = array('delete');
		
	    $helper->show_toolbar = true;
	    $helper->title = 'Faturas ';
	    $helper->table = $this->name.'_invoice';
	    $helper->identifier = 'id_invoice';
	     
	    $helper->token = Tools::getAdminTokenLite('AdminModules');
	    $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&module_name='.$this->name.'&getInvoice';
	    $helper->no_link = true;
	    $helper->tpl_vars =array(
	    	'order_by'=>$this->context->cookie->{$helper->table.'Orderby'},
	    	'order_way'=>$this->context->cookie->{$helper->table.'Orderway'},
	    	);

	    $this->html .= $helper->generateList($invoices, $fields_list);
	    return;
		
	}
}

	
