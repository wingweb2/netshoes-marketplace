<?php
/**
 * Created by PhpStorm.
 * User: Backend Wing
 * Date: 03/10/2017
 * Time: 12:14
 */

use ApiMktpNetshoesV1\sku as sku;
use ApiMktpNetshoesV1\client as client;

require(dirname(__FILE__).'/../../config/config.inc.php');
require(dirname(__FILE__).'/../../init.php');
require dirname(__FILE__).'/includes/functions.php';
require dirname(__FILE__).'/classes/netshoesgroupOrder.php';
require dirname(__FILE__).'/classes/netshoesgroupSku.php';
require_once dirname(__FILE__).'/ApiMktpNetshoesV1.php';

require_once ('netshoesgroup.php');

error_reporting(E_ALL);
ini_set('display_errors', 'On');


$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
$useSSL = true;
client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
client\Configuration::$apiClient = $netshoesGroup;
client\Configuration::$debug = false;

$skusApi = new sku\SkusApi($netshoesGroup);
$error = array();
$export = array();
$tabela1 = json_decode(file_get_contents('tabela_error.json'));

$netshoes = new netshoesgroup();
foreach ($tabela1 as $sku)
{
    if(isset($sku->ps_stock))
    {
        //semerro

        $result = Db::getInstance()->getRow('SELECT id_product FROM ' . _DB_PREFIX_ . 'netshoesgroup_sku WHERE sku "' . $sku->sku . '"');
        $product = new Product($result['id_product']);
        Hook::exec('updateProduct', array('product' => $product), $netshoes->id, false, true);
        echo '<pre>';
        var_dump($sku);
        var_dump($result);
        echo '</pre>';
    }
    else
    {
        $sku_name = explode(" - ", $sku->sku);
        $idSku = $sku_name[1];
//        var_dump($sku_name);
        $id_sku = Db::getInstance()->getRow('SELECT id_sku FROM ' . _DB_PREFIX_ . 'netshoesgroup_sku WHERE sku = "' . $sku_name[1] . '"');
        $sku_obj = new netshoesgroupSku($id_sku['id_sku']);
        $bus_id = $sku_obj->business_id;
        var_dump(Db::getInstance()->getMsgError());
        if($bus_id == "Netshoes" || $bus_id == "NS")
        {
            $bu_id = "NS";
            $sku_obj->business_id = $bu_id;
        }
        if($bus_id == "Zattini" || $bus_id == "ZT")
        {
            $bu_id = "ZT";
            $sku_obj->business_id = $bu_id;
        }
        $status = $skusApi->getStatus($idSku, $bu_id);
        echo '<pre>';
        var_dump($status);
        var_dump($sku);
        echo '</pre>';
//				die();
        if(($status == null))
        {
            $sku_obj->situation = "SKU_NOT_FOUND";
        }
        else
        {
            $sku_obj->situation = $status->statusMatch;
        }
        $sku_obj->update();
        sleep(5);
    }
}
