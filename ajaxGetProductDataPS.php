<?php
require(dirname(__FILE__).'/../../config/config.inc.php');
$useSSL = true;
require(dirname(__FILE__).'/../../init.php');
require dirname(__FILE__).'/includes/functions.php';
error_reporting(E_ALL);
ini_set('display_errors', '1');
$context = Context::getContext();
$id_lang = (int)$context->language->id;
$idProduct = isset($_GET['id'])?(int)$_GET['id']:0;

$product = new Product($idProduct,true,Context::getContext()->language->id);
$return = array();
$return['html'] = null;
$return['id'] = false;
$return['error'] = false;
if(!$product->id){
	$return['id'] = false;
	$return['error'] = 'produto não encontrado.';
	$return['html'] = null;
}else{
	$attributes = $product->getAttributesResume(Context::getContext()->language->id);
	$categoryTax = floatval(Configuration::get('netshoesgroup_variation_price'.$product->id_category_default));
	$return = array(
		'id' => $product->id,
		'sellPrice'=>  number_format(Product::getPriceStatic($idProduct, true),2,'.',''),
		'listPrice' => number_format(($product->tax_rate > 0 && $product->unit_price >0)?$product->unit_price + ($product->unit_price * $product->tax_rate /100) : Product::getPriceStatic($idProduct, true),2,'.',''),
		'stockQuantity'=>StockAvailable::getQuantityAvailableByProduct($idProduct),
		'attributes' => array(),
		'categoryTax' =>$categoryTax,
	);
	
	if(!$attributes){
		$return['attributes'] = false;
		// $return['attributes'][] = array(
			// 'id'=> 0,
			// 'name'=> null,
			// 'sellPrice'=>  number_format(Product::getPriceStatic($idProduct, true),2,'.',''),
			// 'stockQuantity'=>StockAvailable::getQuantityAvailableByProduct($idProduct),
		// );
	}else{
		foreach ($attributes as $key => $row) {
			$return['attributes'][] = array(
				'id'=> $row['id_product_attribute'],
				'name'=> $row['attribute_designation'],
				'sellPrice'=>  number_format(Product::getPriceStatic($idProduct, true,$row['id_product_attribute']),2,'.',''),
				'stockQuantity'=>StockAvailable::getQuantityAvailableByProduct($idProduct,$row['id_product_attribute']),
				'ean' => $row['ean13'],
			);
		}
	}
	
	$tpl = Context::getContext()->smarty->createTemplate(dirname(__FILE__).'/views/templates/admin/sku_product_data.tpl');
	$tpl->assign('product', $return);
	$return['html'] = $tpl->fetch();
	// $return['html'] = null;
}
// echo $return['html'];
// var_dump($return['html']);
echo Tools::jsonEncode($return);