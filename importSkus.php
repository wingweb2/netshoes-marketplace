<?php
use ApiMktpNetshoesV1\sku as sku;
use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\product as product;

require(dirname(__FILE__).'/../../config/config.inc.php');
$useSSL = true;
require(dirname(__FILE__).'/../../init.php');
require dirname(__FILE__).'/includes/functions.php';
require_once dirname(__FILE__).'/ApiMktpNetshoesV1.php';
$return = array(
	'error' =>false,
	'html' =>'',
);
error_reporting(E_ALL);
ini_set('display_errors', 1);
$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
client\Configuration::$apiClient = $netshoesGroup;
$page =   ((int)Tools::getValue('page') >0 )?(int)Tools::getValue('page'):0;
$error = array();
$skusApi = new sku\SkusApi($netshoesGroup);
$productsApi = new product\ProductsApi($netshoesGroup);
try {	
	$products = $productsApi->listProducts($page, 50, array('images, stocks'));
} catch (ApiException $e) {
	$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
}

if(!isset($products['items'])){
	$return['error'] = "Oops! Algo deu errado. Por favor, tente novamente.";
	$return['response'] = $products;
}elseif(count($products['items'])<1){
	$return['error'] = "Nenhum anúncio foi encontrado.";
}elseif(count($error) == 0){
	$return['totalResults'] = count($products['items']);
	$return['productsInPage'] =  50;
	$return['totalPages'] = count($products['items']) / 50;
	$return['nextPage'] = ( count($products['items']) == 50 )?($page+1): false ;
	$error = array();
	foreach ($products['items'] as $key => $product) {
		$dataInsert = array(
			'brand' => isset($product['brand'])?$product['brand']:NULL,
			'product_type' => isset($product['product_type'])?$product['product_type']:NULL,
			'department' => isset($product['department'])?$product['department']:NULL,
			'id_netshoes' => isset($product['product_id'])?$product['product_id']:NULL,
		);
		foreach ($product['skus'] as $key => $sku) {
			$business_ids = array();
			$bu_id = 'NS';
print_r('<br><br>SKU<br><br><br>');
			print_r($sku);
			try {	
				$statusNS = $skusApi->getStatus($sku['sku'], $bu_id);
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if(count($error) == 0 && $statusNS['active'] == true){
				$business_ids[] = 'NS';
			}
			$bu_id = 'ZT';
			try {	
				$statusZT = $skusApi->getStatus($sku['sku'], $bu_id);
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if(count($error) == 0 && $statusZT['active'] == true){
				$business_ids[] = 'ZT';
			}
print_r('<br><br>STATUS NS<br><br><br>');
			print_r($statusNS);
print_r('<br><br>STATUS ZT<br><br><br>');
			print_r($statusZT);
			if($business_ids != array()) {
				try {
					$stock = $skusApi->getStock($sku['sku']);
				} catch (ApiException $e) {
					$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
				}
			} else {
				$business_ids[] = '';
			}
			try {
				$prices = $skusApi->listPrices($sku['sku']);
			} catch (ApiException $e) {
				$error[] = preg_replace("/[\[\]#|]/", "", trim($e->getMessage()));
			}
			if(count($business_ids) >= 1) {
				foreach ($business_ids as $key => $business_id) {
					$dataInsert['id_product'] = NULL;
					$dataInsert['id_attribute'] = NULL;
					$dataInsert['sku'] = isset($sku['sku'])?$sku['sku']:NULL;
					$dataInsert['color'] = ( isset($sku['color']) && $sku['flavor'] == "")?$sku['color']:NULL;
					$dataInsert['flavor'] = ( isset($sku['flavor']) && $sku['color'] == "")?$sku['flavor']:NULL;
					$dataInsert['gender'] = isset($sku['gender'])?$sku['gender']:NULL;
					$dataInsert['size'] = isset($sku['size'])?$sku['size']:NULL;
					$dataInsert['name'] = isset($sku['name'])?$sku['name']:NULL;
					$dataInsert['description'] = isset($sku['description'])?$sku['description']:NULL;
					$dataInsert['eanIsbn'] = isset($sku['ean_isbn'])?$sku['ean_isbn']:NULL;
					$dataInsert['height'] = isset($sku['height'])?$sku['height']:0;
					$dataInsert['width'] = isset($sku['width'])?$sku['width']:0;
					$dataInsert['depth'] = isset($sku['depth'])?$sku['depth']:0;
					$dataInsert['weight'] = isset($sku['weight'])?$sku['weight']:0;
					$dataInsert['stockQuantity'] = isset($stock['available'])?$stock['available']:NULL;
					if ($business_id == 'NS') {
						$dataInsert['enable'] = isset($statusNS['active'])?$statusNS['active']:false;
						$dataInsert['situation'] = ($statusNS['active'] == true)?'RELEASED_MATCH':'PENDING_MATCH';
					} elseif($business_id == 'ZT'){
						$dataInsert['enable'] = isset($statusZT['active'])?$statusZT['active']:false;
						$dataInsert['situation'] = ($statusZT['active'] == true)?'RELEASED_MATCH':'PENDING_MATCH';
					} else {
						$dataInsert['enable'] = false;
						$dataInsert['situation'] = 'PENDING_MATCH';
					}
					$dataInsert['price'] = isset($prices['price'])?$prices['price']:NULL;
					$dataInsert['images'] = isset($sku['images'])?Tools::jsonEncode($sku['images'],JSON_UNESCAPED_UNICODE):NULL;
					$dataInsert['business_id'] = $business_id;
					$dataInsert['link'] = isset($sku['links']['href'])?Tools::jsonEncode($sku['links']['href'],JSON_UNESCAPED_UNICODE):NULL;
					$dataInsert['date_add'] = date('Y-m-d H:i:s');
		  			$dataInsert['date_upd'] = date('Y-m-d H:i:s');

					if(Db::getInstance()->getValue('SELECT sku FROM '._DB_PREFIX_.'netshoesgroup_sku WHERE sku = "'.$sku['sku'].'"')){ 
			
						$insert = Db::getInstance()->update('netshoesgroup_sku',$dataInsert,'sku = "'.$sku['sku'].'"');
					}else{
print_r('<br><br><br>DATAINSERT<br><br>');print_r($dataInsert);
						$insert = Db::getInstance()->insert('netshoesgroup_sku',$dataInsert);
					}
					$insert = true;
					$return['sku'][$sku['sku']] = array(
						'insert' =>$insert,
						'data' =>$dataInsert,
						'error' =>($insert?null:Db::getInstance()->getMsgError())
					);
					$return['html'] = 'Produtos importados com sucesso!';
				}
			} else {
				$return['error'] = 'Ops, ocorreu algum problema durante a importação!';
			}
		}
	}
}
echo Tools::jsonEncode($return);
