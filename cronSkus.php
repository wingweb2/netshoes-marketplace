<?php
use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\sku as sku;
use ApiMktpNetshoesV1\product as product;
require(dirname(__FILE__).'/../../config/config.inc.php');
require(dirname(__FILE__).'/../../init.php');
require dirname(__FILE__).'/includes/functions.php';
require dirname(__FILE__).'/classes/netshoesgroupSku.php';
require_once dirname(__FILE__).'/ApiMktpNetshoesV1.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
$useSSL = true;
client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
client\Configuration::$apiClient = $netshoesGroup;
client\Configuration::$debug = false;

$skusApi = new sku\SkusApi($netshoesGroup);
$productsApi = new product\ProductsApi($netshoesGroup);

$db = Db::getInstance();
$page = isset($_GET['page'])?$_GET['page']:0;
echo $page."\r\n";
$sql = 'SELECT sku, id_sku FROM '._DB_PREFIX_.'netshoesgroup_sku where id_sku >= '.($page*100).' AND id_sku <= '.($page*100+100);
var_dump($sql)."\r\n";

if ($results = Db::getInstance()->ExecuteS($sql))
{
    $requests = 0;
    foreach ($results as $row)
    {
        $sku = new netshoesgroupSku($row['id_sku']);
        $productPS = new ProductCore($sku->id_product);
        if($sku->business_id == "Netshoes" || $sku->business_id == "NS")
        {
            $bu_id = "NS";
            $sku->business_id = $bu_id;
        }
        if($sku->business_id == "Zattini" || $sku->business_id == "ZT")
        {
            $bu_id = "ZT";
            $sku->business_id = $bu_id;
        }


        if( $skuStatus = @$skusApi->getStatus($sku->sku, $sku->business_id) )
        {
            $lastHeader = $skusApi->getApiClient()->getLastHeaderResponse();
            //logMessageNS($lastHeader);
//            $requests++;
//            if($requests >= 50)
//            {
//                logMessageNS('Request: ' . $requests . ', aguardar 30s');
//                sleep(30);
//                $requests = 0;
//            }
            if($skuStatus->statusMatch != null)
            {
                $sku->situation = $skuStatus->statusMatch;
                $sku->update();
            }
            else
            {
                if($skuStatus->active == true)
                {
                    $sku->situation = 'RELEASED_MATCH';
                    $sku->update();
                }
            }
            if($sku->situation == 'PENDING_MATCH')
            {
	            if(!is_object($sku->price))
	            {
		            $sku->price = json_decode($sku->price);
	            }
	            if($sku->variation_priority != 'l')
	            {
		            $sku->price->sellPrice = Product::getPriceStatic($sku->id_product, true, $sku->id_attribute, 2);
		            $sku->price->listPrice = tofloat(( $productPS->tax_rate > 0 && $productPS->unit_price > 0 ) ? $productPS->unit_price + ( $productPS->unit_price * $product->tax_rate / 100 ) : $sku->price->sellPrice);
		            if($sku->variation_priority == 'p' && $sku->variation_price > 0)
		            {
			            $sku->price->sellPrice = $sku->price->sellPrice + ( $sku->price->sellPrice * $sku->variation_price / 100 ) + $sku->price_impact;
		            }
		            else
		            {
			            $sku->price->sellPrice = $sku->price->sellPrice + ( $sku->price->sellPrice * $categoryTax / 100 ) + $sku->price_impact;
		            }
	            }
	            else
	            {
		            $sku->price->sellPrice = ProductCore::getPriceStatic($sku->id_product, true, $sku->id_attribute, 2);
		            $sku->price->listPrice = tofloat(( $productPS->tax_rate > 0 && $productPS->unit_price > 0 ) ? $productPS->unit_price + ( $productPS->unit_price * $productPS->tax_rate / 100 ) : $sku->price->sellPrice);
	            }
                $body = array('country' => 'BR', 'listPrice' => $sku->price->listPrice , 'sellPrice' => $sku->price->sellPrice );
//                var_dump($sku, $body);
//                die();
                $skusApi->updatePrice($sku->sku, $body);
//                $requests++;
//                if($requests >= 50)
//                {
//                    logMessageNS('Request: ' . $requests . ', aguardar 30s');
//                    sleep(30);
//                    $requests = 0;
//                }
                logMessageNS(['preço do sku '.$sku->sku.' foi atualizado', $body]);
                if($skuStatus['active'] == true)
                {
                    if($sku->stockQuantity < 0)
                        $sku->stockQuantity = 0;
		            if($sku->brand == 'Oakley' || strpos(trim($sku->name), 'Oakley') !== false)
						$sku->stockQuantity = 0;
                    $bodyStock = array('country' => 'BR', 'available' => floatval($sku->stockQuantity));
                    $skusApi->updateStock($sku->sku, $bodyStock);
//                    $requests++;
//                    if($requests >= 50)
//                    {
//                        logMessageNS('Request: ' . $requests . ', aguardar 30s');
//                        sleep(30);
//                        $requests = 0;
//                    }
                    logMessageNS('quantidade do sku '.$sku->sku.' foi atualizada');
                    $sku->enable = true;
                }
                $sku->situation = 'RELEASED_MATCH';
                $sku->update();
                /*
               else {
                   $body = array('country' => 'BR', 'listPrice' => floatval( ($sku->price->listPrice)?$sku->price->listPrice:$sku->price ), 'sellPrice' => floatval( ($sku->price->sellPrice)?$sku->price->sellPrice:$sku->price) );
                   $skusApi->updatePrice($sku->sku, $body);
                   logMessageNS('preço do sku '.$sku->sku.' foi atualizado');
               }
               */
            }
            elseif($sku->situation == 'RELEASED_MATCH')
            {

                if($skuStatus['active'] == true)
                {
                    $sku->enable = true;
                    $sku->stockQuantity = ($sku->brand == 'Oakley'  || strpos(trim($sku->name), 'Oakley') !== false)?0:intval( StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute) );
                    if($sku->stockQuantity < 0)
                        $sku->stockQuantity = 0;
                    $bodyStock = array('country' => 'BR', 'available' => floatval($sku->stockQuantity));
                    $resp = $skusApi->updateStock($sku->sku, $bodyStock);
//                    $requests++;
//                    if($requests >= 50)
//                    {
//                        logMessageNS('Request: ' . $requests . ', aguardar 30s');
//                        sleep(30);
//                        $requests = 0;
//                    }
                    logMessageNS('quantidade do sku '.$sku->sku.' foi atualizada: ' . debug($bodyStock) . ' resposta: ' . debug($resp));
//                    if($skuStock = @$skusApi->getStock($sku->sku))
//                    {
//                        if($sku->id_product)
//                        {
//                            $productStock = intval( StockAvailable::getQuantityAvailableByProduct($sku->id_product, $sku->id_attribute) );
//                            $sku->stockVariation = $skuStock['available'] - $productStock;
//                        }
//                        $sku->stockQuantity = intval($skuStock['available']);
//                    }
	                if(!is_object($sku->price))
	                {
	                	$sku->price = json_decode($sku->price);
	                }
	                if($sku->variation_priority != 'l')
	                {
		                $sku->price->sellPrice = ProductCore::getPriceStatic($sku->id_product, true, $sku->id_attribute, 2);
		                $sku->price->listPrice = tofloat(( $productPS->tax_rate > 0 && $productPS->unit_price > 0 ) ? $productPS->unit_price + ( $productPS->unit_price * $productPS->tax_rate / 100 ) : $sku->price->sellPrice);
		                if($sku->variation_priority == 'p' && $sku->variation_price > 0)
		                {
			                $sku->price->sellPrice = $sku->price->sellPrice + ( $sku->price->sellPrice * $sku->variation_price / 100 ) + $sku->price_impact;
		                }
		                else
		                {
			                $sku->price->sellPrice = $sku->price->sellPrice + ( $sku->price->sellPrice * $categoryTax / 100 ) + $sku->price_impact;
		                }
	                }
	                else
	                {
		                $sku->price->sellPrice = Product::getPriceStatic($sku->id_product, true, $sku->id_attribute, 2);
		                $sku->price->listPrice = tofloat(( $productPS->tax_rate > 0 && $productPS->unit_price > 0 ) ? $productPS->unit_price + ( $productPS->unit_price * $productPS->tax_rate / 100 ) : $sku->price->sellPrice);
	                }
	                $body = array('country' => 'BR', 'listPrice' => $sku->price->listPrice , 'sellPrice' => $sku->price->sellPrice );
	                //var_dump($sku, $body);
//	                die();
                    $skusApi->updatePrice($sku->sku, $body);
	                logMessageNS(['preço do sku '.$sku->sku.' foi atualizado', $body]);
                    if($skuPrice = @$skusApi->listPrices($sku->sku))
                    {
                        echo '<pre>';
                        var_dump($sku->sku);
                        var_dump($skuPrice);
                        echo '</pre>';
                        $sku->price->sellPrice = $skuPrice->price;

                    }
                    if(!$sku->update())
                    {
                        var_dump($sku);
                        die();
                    }
                    else
                    {
                        var_dump($sku->stockQuantity);
                        var_dump($sku->stockVariation);
                    }


                }

            }

        }
    }
    logMessage(json_encode(array('page'=>$page, 'sql'=>$sql, 'count_results'=>count($results))), 0, dirname(__FILE__).'/logs/cron-sku-'.date('Y-m-d').'.log');
    if( count($results) > 0 )
    {
        $page += 1;
        // header("Refresh:0; url=cronSkus.php?page=".$page);
        logMessage('aguardar 20 segundos para proxima pagina', 0, dirname(__FILE__).'/logs/cron-sku-'.date('Y-m-d').'.log');
        sleep(20);
        header("Location: cronSkus.php?page=".$page);
    }
}


