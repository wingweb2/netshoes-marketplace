<?php

use ApiMktpNetshoesV1\client as client;
use ApiMktpNetshoesV1\order as order;

require(dirname(__FILE__).'/../../../config/config.inc.php');
require(dirname(__FILE__).'/../../../init.php');
require dirname(__FILE__).'/../includes/functions.php';
require dirname(__FILE__).'/../classes/netshoesgroupSku.php';
require_once dirname(__FILE__).'/../ApiMktpNetshoesV1.php';

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$netshoesGroup = new client\ApiClient((Configuration::get('netshoesgroup_version') == 1?'http://api-marketplace.netshoes.com.br/api/v1':'http://api-sandbox.netshoes.com.br/api/v1'));
$useSSL = true;
client\Configuration::$apiKey['client_id'] = Configuration::get('netshoesgroup_client_id');
client\Configuration::$apiKey['access_token'] = Configuration::get('netshoesgroup_access_token');
client\Configuration::$apiClient = $netshoesGroup;
client\Configuration::$debug = false;
$apiOrder = new order\OrdersApi($netshoesGroup);

// $orders = $apiOrder->listOrders($page, 50, array("shippings", "items", "devolutionItems"), null, null, null, null);
//    public function listOrders($page, $size, $expand, $order_start_date, $order_end_date, $order_status, $order_type) {
$orders = $apiOrder->listOrders(0, 100,  array("shippings", "items", "devolutionItems"), '2017-10-01T00:00:00.000-03:00', null, 'canceled', null);
// $orders = $apiOrder->listOrders(0, 5,  array("shippings", "items", "devolutionItems"), null, null, null, null);

$itensPedidos = [];
foreach ($orders['items'] as $key => $order) {
    foreach ($order['shippings'] as $shipping) {
        foreach ($shipping['items'] as $item) {

            $itemReturn = [];
            $itemReturn['data'] = date("Y-m-d H:i:s", $order['order_date']/1000);
            $itemReturn['pedido'] = $order['number'];
            $itemReturn['sku'] = $item['sku'];
            $itemReturn['nome'] = $item['name'];
            $itemReturn['quantidade'] = $item['quantity'];
            $itemReturn['order_status'] = $order['status'];
            $itemReturn['shipping_status'] = $shipping['status'];

            $itensPedidos[] = $itemReturn;
        }
    }
} 
echo json_encode($itensPedidos);
// echo json_encode($orders);