<hr>
<div class="col-lg-6">
    {if $product.attributes}
        <div class="form-group col-lg-12">
            <label for="id_attribute" class="control-label col-lg-3 required"> <span>Combinação</span> </label>
            <div class="col-lg-8">
                <select required="" id="id_attribute" name="id_attribute" onchange="changeAttribute()">
                    <option value="" data-stockquantity="0" data-sellprice="0" >Selecione a combinação</option>
                    {foreach from=$product.attributes key=key item=attribute}
                        <option data-stockquantity="{$attribute.stockQuantity}" data-sellprice="{$attribute.sellPrice}" value="{$attribute.id}">{$attribute.name}</option>
                    {/foreach}
                </select>
            </div>
        </div>
    {else}
        <input type="hidden"  id="id_attribute" name="id_attribute" value="0"/>
    {/if}
    <div class="form-group col-lg-12">
        <label for="id_attribute" class="control-label col-lg-3 required"> <span>Estoque</span> </label>
        <div class="col-lg-8">
            <input required="" id="stockquantity" name="stockquantity" value="{$product.stockQuantity}" class="form-control">
        </div>
    </div>
</div>

<div class="col-lg-6 panel">
    <h3> Definição de preço </h3>
    <div class="row">
        <div class="form-group col-lg-4">
            <label class="control-label col-lg-4" for="tax-type-c">Categoria</label>
            <div class="col-lg-8">
                <div class="input-group">
                    <span class="input-group-addon">
                        <input type="radio" class="tax-type" name="tax-type" value="c" checked="checked" id="tax-type-c">
                    </span>
                    <input type="text" class="form-control" id="tax-c" value="{$product.categoryTax}" disabled="">
                    <span class="input-group-addon">
                        %
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group col-lg-4">
            <label class="control-label col-lg-4" for="tax-type-p">Produto</label>
            <div class="col-lg-8">
                <div class="input-group">
                    <span class="input-group-addon">
                        <input type="radio" class="tax-type" name="tax-type" value="p" id="tax-type-p">
                    </span>
                    <input type="text" class="form-control"  onkeyup="updateSellPrice(this.value);" id="tax-p" name="tax" value="0" >
                    <span class="input-group-addon">
                        %
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group col-lg-4">
            <label for="tax-type-p" class="control-label col-lg-4">Impacto Ad.</label>
            <div class="col-lg-8">
                <div class="input-group">
                    <input type="text" value="0" name="price-impact" id="price-impact" onkeyup="updateSellPrice(this.value,true);" class="form-control">
                    <span class="input-group-addon">R$</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="form-group col-lg-6">
            <label for="listPrice" class="control-label col-lg-6 required"> <span>Preço De</span> </label>
            <div class="col-lg-6 input-group">
                <input type="text" maxlength="13" required="" value="{$product.listPrice}" id="listPrice" name="listPrice" class="form-control">
                <span class="input-group-addon">R$</span>
            </div>
        </div>
        <div class="form-group col-lg-6">
            <label for="" class="control-label col-lg-6 required"> <span>Preço Por</span> </label>
            <div class="col-lg-6 input-group">
                <span class="input-group-addon">
                    <input type="radio" class="tax-type" name="tax-type" value="l" id="tax-type-l">
                </span>
                <input type="text" maxlength="13" id="sellPrice" name="sellPrice" class="form-control" data-sellprice="{$product.sellPrice}"  value="{number_format($product.sellPrice + ($product.sellPrice * $product.categoryTax /100),2,'.','')}">
                <span class="input-group-addon">R$</span>
            </div>
        </div>
    </div>
</div>
