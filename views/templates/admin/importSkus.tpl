<div id="fieldset_import_orders" class="panel panel col-lg-6 col-lg-offset-3">
    <div class="panel-heading">
        <i class="icon-cloud-download"></i>
        Importação de anúncios.
    </div>
    <div class="panel-body">
        <div class="alert alert-warning col-lg-10 col-lg-offset-1 ">
            <p>Aguarde enquanto buscamos os seus anúncios.</p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<script type="text/javascript">
    var importOrdersUrl = "{$importSkusUrl}";
    
    function importOrders(){
        console.log(importOrdersUrl);
        $.ajax({
                type: "get",
                url: importOrdersUrl,
                dataType: "json",
                success: function(response) {
                    if(!response.error){
                        var html = '<h2>Total de anúncios: '+response.totalResults+'</h2><ul>';
                        var count = 0;
                        var page = (response.nextPage - 1);
                        for (i = 1; i <= response.tatalPages; i++) {
                            count = Number(i*50);
                            if(count < response.totalResults){
                                html += '<li class="page-'+i+'">'+ String(count-50)+' | '+String(count-1)+'</li>';
                            }else{
                                html += '<li class="page-'+i+'">'+ String(count-50)+' | '+String(response.totalResults)+'</li>';
                            }
                        }
                        html += '</ul>';
                        $('#fieldset_import_orders .panel-body').append(html);
                        
                        $('#fieldset_import_orders .page-'+page).css( "text-decoration", "line-through" );
                        $('#fieldset_import_orders .page-'+page).css( "color", "#aaaaaa" );
                        
                        importOrdersNext(response.nextPage);
                    }else{
                        $('#fieldset_import_orders .panel-body').append('<div class="alert alert-danger col-lg-10 col-lg-offset-1 ">'+response.error+'</div>');
                    }
                },
                error: function(response) {
                      $('#fieldset_import_orders .panel-body').append('<div class="alert alert-danger col-lg-10 col-lg-offset-1 "> Ocorreu um erro ao importar os SKUs </div>');
                }
            });
    }
    importOrders();
    function importOrdersNext(page){
        if(page){
            console.log(importOrdersUrl+'?page='+page);
            $.ajax({
                type: "get",
                url: importOrdersUrl+'?page='+page,
                dataType: "json",
                success: function(response) {
                    if(!response.error){
                        $('#fieldset_import_orders .page-'+page).css( "text-decoration", "line-through" );
                        $('#fieldset_import_orders .page-'+page).css( "color", "#aaaaaa" );
                        importOrdersNext(response.nextPage);
                    }else{
                        $('#fieldset_import_orders .panel-body').append('<div class="alert alert-danger col-lg-10 col-lg-offset-1 ">'+response.error+'</div>');
                    }
                }
            });
        }else{
            $('#fieldset_import_orders .panel-body').append('<div class="alert alert-success col-lg-10 col-lg-offset-1">Anúncios importados com sucesso.</div>');
            setTimeout(function() {
                window.location.href = "index.php?controller={$smarty.get.controller}&token={$smarty.get.token}&configure=netshoesgroup&module_name=netshoesgroup";
            }, 2000);
        }
    }
</script>