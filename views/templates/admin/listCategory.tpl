<div class="panel col-lg-12">
        <h3>Lista de Categorias | Valores serão Atualizados conforme a categoria padrão do produto</h3>
    <style>
        @media (max-width: 992px) {
                    .table-responsive-row td:nth-of-type(1):before {
                content: "Id";
            }
                    .table-responsive-row td:nth-of-type(2):before {
                content: "Name";
            }
                    .table-responsive-row td:nth-of-type(3):before {
                content: "Breadcrumb";
            }
                    .table-responsive-row td:nth-of-type(4):before {
                content: "Valor";
            }
        }
    </style>
    <div class="table-responsive-row clearfix">
        <table class="table mlint_categories">
            <thead>
                <tr class="nodrag nodrop">
                    <th class="">
                        <span class="title_box">
                            Id
                        </span>
                    </th>
                    <th class="">
                        <span class="title_box">
                            Name
                        </span>
                    </th>
                    <th class="">
                        <span class="title_box">
                            Breadcrumb
                        </span>
                    </th>
                    <th class="">
                        <span class="title_box">
                            Variação de preço (%)
                        </span>
                    </th>
                    <th> </th>
                </tr>
            </thead>
            <tbody>
                {foreach from=$list key=key item=row}
                <input type="hidden" name="submitUpdate" value="1">
                <tr class="{if (! $key%2 ) }odd{/if}">
                    <form action="index.php?controller=AdminModules&configure=netshoesgroup&token={getAdminToken tab='AdminModules'}" method="post">
                        <input type="hidden" name="id_category" value="{$row.id_category}">
                        <td class="pointer">{$row.id_category}</td>
                        <td class="pointer">{$row.name}</td>
                        <td class="pointer">{$row.breadcrumb}</td>
                        <td class="pointer"><input type="number" name="variation_price" value="{$row.variation_price}" step="any"/></td>
                        <td class="text-right">
                            <button class="btn btn-default" name="submitCategory" value="1" type="submit">
                                <i class="icon-save"></i> Salvar
                            </button>
                            <button class="btn btn-default submitUpdate" name="submitUpdate" value="1" type="submit">
                                <i class="icon-refresh"></i> Atualizar valores
                            </button>
                        </td>
                    </form>
                </tr>
                {/foreach}
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-lg-6">
        </div>
    </div>
</div>