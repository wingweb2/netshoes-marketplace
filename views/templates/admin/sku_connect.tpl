<div class="panel">
    <form method="post" class="form-horizontal"  name="submitExport" action="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}&id_sku={$product.id}&action=connect">
        <h3>Associar Anúncio - {$product.name} - EAN - {$product.ean}</h3>
        <div class="form-group">
            <label class="control-label col-lg-2" for="productFilter">Produto</label>
            <div class="input-group col-lg-5">      
                <input type="hidden" id="id_product" name="id_product" value="" />
                <input type="text" id="productFilter" class="input-xlarge" name="productFilter" value="{$product.ean}" autofocus/>
                <span class="input-group-addon"><i class="icon-search"></i></span>
            </div>
        </div>
        <div class="row" id="content-product">
            <div class="alert alert-warning">
                <ul class="list-unstyled">
                    <li>Selecione o produto para dar continuidade.</li>
                 </ul>
            </div>
        </div>
        <div class="panel-footer">
            <button class="btn btn-default pull-right" name="submitConnect" id="form_submit_btn" value="1" type="submit" disabled>
                <i class="process-icon-save"></i> Salvar
            </button>
            <a onclick="window.history.back()" class="btn btn-default" href="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}" > <i class="process-icon-cancel"></i> Cancelar </a>
        </div>
    </form>
</div>

<script type="text/javascript">
    {literal}
    $('#productFilter')
        .autocomplete(
                '../modules/netshoesgroup/ajaxGetProductPS.php', {
                minChars: 2,
                width: 500,
                selectFirst: false,
                scroll: false,
                dataType: 'json',
                formatItem: function(data, i, max, value, term) {
                    return value;
                },
                parse: function(data) {
                    var mytab = new Array();
                    for (var i = 0; i < data.length; i++)
                        mytab[mytab.length] = { data: data[i], value: data[i].name };
                    return mytab;
                },
                extraParams: {
                    //controller: '',
                    token: "{$smarty.get.token}",
                    productFilter: 1
                }
            }
        )
        .result(function(event, data, formatted) {
            document.getElementById("id_product").value = data.product_id;
            document.getElementById("productFilter").value = data.name;
            $.ajax({
                type: "get",
                url: "../modules/netshoesgroup/ajaxGetProductDataPS.php?id=" + data.product_id,
                dataType: "json",
                success: function(response) {
                    if(!response.error){
                        $('#content-product').html(response.html);
                        $('#form_submit_btn').attr('disabled',false);
                    }else{
                        $('#content-product').html('<div class="alert alert-danger"><ul class="list-unstyled"><li>'+response.error+'</li></ul></div>');
                    }
                }
            });
        });
        
    //simula um evento para fazer uma busca no autocomplete pelo EAN do produto
    var e = $.Event("keydown", { keyCode: 38}); //"keydown" if that's what you're doing
    $("#productFilter").trigger(e);
    
    $(document).on('change', '#id_attribute', function() {
        var elOption = $(this).find(":selected");
        $('#stockquantity').val(elOption.data('stockquantity'));
        $('#sellPrice').data('sellprice',elOption.data('sellprice'));
        if($('.tax-type:checked').val() == 'p'){
            updateSellPrice($('#tax-p').val());
        }else{
            updateSellPrice($('#tax-c').val());
        }
    });
    

    function updateSellPrice(tax){
        tax = parseFloat(tax);
        if(!isNaN(tax)) {
            var sellPriceElement = $('#sellPrice');
            var sellPrice = Number(sellPriceElement.data('sellprice'));
            newSellPrice = sellPrice + (sellPrice * tax / 100);
            sellPriceElement.val(newSellPrice.toFixed(2));
        }
    }
    $(document).on('click', '.tax-type', function() {
        var element = $(this);
        var type = element.val();
        if (element.is(':checked')) {
            if(type == 'p'){
                $('#tax-p').attr('readonly',false);
                updateSellPrice($('#tax-p').val());
            }else{
                $('#tax-p').attr('readonly',true);
                $('#tax-p').val(0);
                updateSellPrice($('#tax-c').val());
            }
        }
    });

    {/literal}
</script>