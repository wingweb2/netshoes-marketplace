<div class="clearfix"></div>
<div id="dashboard">
    <section id="dashtrends" class="panel widget allow_push">
        <header class="panel-heading">
            <i class="icon-bar-chart"></i> {l s='Painel' mod='netshoesgroup'}
            <span class="panel-heading-action">
                <a class="list-toolbar-btn" href="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}&configure=netshoesgroup&module_name=netshoesgroup&getInvoice" title="{l s='Faturas' mod='netshoesgroup'}">
                    <i class="process-icon-file-text icon-file-text"></i>
                </a>
            </span>
        </header>
        <div id="dashtrends_toolbar" class="row">
            <dl class="col-xs-6 col-lg-4 label-tooltip" id="sales_trends" onclick="selectDashtrendsChart(this, 'sales');" data-toggle="tooltip" data-original-title="{l s='A soma da receita gerado no intervalo de datas por pedidos considerados validados.' mod='netshoesgroup'}" data-placement="bottom">
                    <dt>{l s='Vendas' mod='netshoesgroup'}</dt>
                    <dd class="data_value size_l"><span id="sales_score">{displayPrice price=$data.sales.total }</span></dd>
            </dl>
            <dl class="col-xs-6 col-lg-4 label-tooltip" id="orders_trends" onclick="selectDashtrendsChart(this, 'orders');" data-toggle="tooltip" data-original-title="{l s='Número total de pedidos válidos realizados no intervalo das datas.' mod='netshoesgroup'}" data-placement="bottom">
                    <dt>{l s='Pedidos' mod='netshoesgroup'}</dt>
                    <dd class="data_value size_l"><span id="orders_score">{$data.orders.total }</span></dd>
            </dl>
            <dl class="col-xs-12 col-lg-4" id="form-date" style="cursor: inherit;">
                <form class="defaultForm form-inline" style="margin: 16px 0px;" action="{$formAction}" method="post">
                  <div class="form-group">
                    <label class="sr-only" for="dateStart">Name</label>
                    <div class="input-group">
                      <input type="text" class="form-control datepicker" name="dateStart" id="dateStart" placeholder="De" value="{$dateStart}">
                      <span class="input-group-addon">
                        <i class="icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="sr-only" for="dateEnd">Name</label>
                    <div class="input-group">
                      <input type="text" class="form-control datepicker" name="dateEnd" id="dateEnd" placeholder="Para" value="{$dateEnd}">
                      <span class="input-group-addon">
                        <i class="icon-calendar"></i>
                      </span>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary">Pesquisar</button>
                </form>
            </dl>
        </div>
        <div id="dash_trends_chart1" class="chart with-transitions">
            <svg></svg>
        </div>
    </section>
</div>
<div class="clearfix"></div>
<script>
var data_sales  = {$data.sales.valuesFormated};
var data_orders  = {$data.orders.valuesFormated};

{literal}
var dashtrends_chart;
var currency_format = 3;
var currency_sign = 'R$';
var currency_blank = 1;
var priceDisplayPrecision = 2;
var data = [{
        values: data_sales,
        key: 'Vendas',
        color: '#1777B6',
        id:'sales'
        },{
        values: data_orders,
        key: 'Pedidos',
        color: '#2CA121',
        id:'orders'
        }];

function selectDashtrendsChart(element, type)
{
    $('#dashtrends_toolbar dl').removeClass('active');
    current_charts = new Array();
    $.each(data, function(index, value) {
        if (value.id == type || value.id == type + '_compare')
        {
            if (value.id == type)
            {
                $(element).siblings().css('background-color', 'none').removeClass('active');
                $(element).css('background-color', data[index].color).addClass('active');
            }
            current_charts.push(data[index]); 
            value.disabled = false;
        }
    });
    
    dashtrends_chart = nv.models.lineChart()
        .useInteractiveGuideline(true)
        ;
    dashtrends_chart.xAxis
        .axisLabel('Data')
        .tickFormat(function(d) {
            var date = new Date(d*1000);
            console.log(d,date);
            return date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();
          }); 
    dashtrends_chart.yAxis.tickFormat(d3.format('.f'));
    if (type == 'sales')
        dashtrends_chart.yAxis.tickFormat(function(d) {
            return 'R$ '+parseFloat(d);
            // return formatCurrency(parseFloat(d));
        });

    d3.select('#dash_trends_chart1 svg')
        .datum(current_charts)
        .call(dashtrends_chart);
}
$(document).ready(function(){
    nv.addGraph(function() {
      var chart = nv.models.lineChart()
        .useInteractiveGuideline(true)
        ;
    
      chart.xAxis
        .axisLabel('Data')
        .tickFormat(function(d) {
            var date = new Date(d*1000);
            return date.getFullYear()+'-'+date.getMonth()+'-'+date.getDate();
          });
    
      chart.yAxis
        .axisLabel('Valor')
        .tickFormat(function(d) {
            return formatCurrency(parseFloat(d), currency_format, currency_sign, currency_blank);
        })
        ;
    
        console.log(data['sales']);
        dashtrends_chart = chart;
      return chart;
    });


    selectDashtrendsChart($('#sales_trends'), 'sales');

    // $("dl").tooltip();
});
{/literal}
</script>
