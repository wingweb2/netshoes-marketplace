<div class="panel">
    <form method="post" class="form-horizontal"  name="submitExport" action="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}&netshoesgroupSKU&id_sku={$product.id}">
        <h3>Associar Anúncio - {$product.name} - EAN - {$product.ean}</h3>
        <div class="form-group">
            <label class="control-label col-lg-2" for="productFilter">Produto</label>
            <div class="input-group col-lg-5">      
                <input type="hidden" id="id_product" name="id_product" value="{if ($product.id_product >0)}{$product.id_product}{/if}" />
                <input type="text" id="productFilter" class="input-xlarge" name="productFilter" value="{$product.ean}" autofocus/>
                <span class="input-group-addon"><i class="icon-search"></i></span>
            </div>
        </div>
        <div class="row" id="content-product">
            <div class="alert alert-warning">
                <ul class="list-unstyled">
                    <li>Selecione o produto para dar continuidade.</li>
                 </ul>
            </div>
        </div>
        <div class="panel-footer">
            <button class="btn btn-default pull-right" name="submitConnectSKU" id="form_submit_btn" value="1" type="submit" >
                <i class="process-icon-save"></i> Salvar
            </button>
            <button class="btn btn-default pull-right" name="submitRemoveRelation" id="form_submit_btn" value="1" type="submit" >
                <i class="process-icon-cancel"></i> Remover 
            </button>
            <a onclick="window.history.back()" class="btn btn-default" href="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}" > <i class="process-icon-cancel"></i> Cancelar </a>
        </div>
    </form>
</div>

<script type="text/javascript">
    $('#productFilter')
        .autocomplete(
                '../modules/netshoesgroup/ajaxGetProductPS.php', {
                minChars: 2,
                width: 500,
                selectFirst: false,
                scroll: false,
                dataType: 'json',
                formatItem: function(data, i, max, value, term) {
                    return value;
                },
                parse: function(data) {
                    var mytab = new Array();
                    for (var i = 0; i < data.length; i++)
                        mytab[mytab.length] = { data: data[i], value: data[i].name };
                    return mytab;
                },
                extraParams: {
                    //controller: '',
                    token: "{$smarty.get.token}",
                    productFilter: 1
                }
            }
        )
        .result(function(event, data, formatted) {
            document.getElementById("id_product").value = data.product_id;
            document.getElementById("productFilter").value = data.name;
            getProductData(data.product_id);
        });
        
    
    $(document).on('change', '#id_attribute', function() {
        changeAttribute();
    });
    
    function changeAttribute(){
        var elOption = $('#id_attribute').find(":selected");
        $('#stockquantity').val(elOption.data('stockquantity'));
        $('#sellPrice').data('sellprice',elOption.data('sellprice'));
        if($('.tax-type:checked').val() == 'p'){
            updateSellPrice($('#tax-p').val());
        }else if($('.tax-type:checked').val() == 'c'){
            updateSellPrice($('#tax-c').val());
        }
        
    }
    
    function getProductData(id_prodcut, id_attribute, variation_type, variation_price,price_impact){
        id_attribute = parseFloat(id_attribute);
        if(isNaN(id_attribute)) id_attribute = 0;
        $.ajax({
            type: "get",
            url: "../modules/netshoesgroup/ajaxGetProductDataPS.php?id=" + id_prodcut + "&id_attribute="+id_attribute,
            dataType: "json",
            success: function(response) {
                if(!response.error){
                    $('#content-product').html(response.html);
                    $('#form_submit_btn').attr('disabled',false);
                    $('#content-product #id_attribute').val(id_attribute);
                    if (id_attribute > 0)
                        changeAttribute();
                    $('#content-product #price-impact').val(price_impact);
                    if (variation_type == 'p'){
                        $('#content-product #tax-type-p').prop("checked", true);
                        $('#content-product #tax-p').val(variation_price);
                        $('#content-product #tax-p').attr('readonly',false);
                        updateSellPrice($('#content-product #tax-p').val());
                    }else if(variation_type == 'l'){
                        var sellPriceElement = $('#sellPrice');
                        sellPriceElement.val(Number($('#price-sell-price').val()));
                        sellPriceElement.attr('readonly',false);
                        $('#price-impact').attr('readonly',true);
                        $('#content-product #tax-type-l').prop("checked", true);
                    }
                    return true;
                }else{
                    $('#content-product').html('<div class="alert alert-danger"><ul class="list-unstyled"><li>'+response.error+'</li></ul></div>');
                    return false;
                }
            }
        });
    }

    function updateSellPrice(tax, impact){
        var impactValue = parseFloat($('#price-impact').val());
        if(isNaN(impactValue)) impactValue = 0;
        if(impact){
            if($('.tax-type:checked').val() == 'p'){
                tax = $('#tax-p').val();
            } else if($('.tax-type:checked').val() == 'c'){
                tax = $('#tax-c').val();
            }
            
        }
        tax = parseFloat(tax);
        if(!isNaN(tax)) {
            var sellPriceElement = $('#sellPrice');
            var sellPrice = Number(sellPriceElement.data('sellprice'));
            newSellPrice = sellPrice + (sellPrice * tax / 100) + impactValue;
            sellPriceElement.val(newSellPrice.toFixed(2));
        }
    }
    $(document).on('click', '.tax-type', function() {
        var element = $(this);
        var type = element.val();
        if (element.is(':checked')) {
            if(type == 'p'){
                $('#tax-p').attr('readonly',false);
                updateSellPrice($('#tax-p').val());
                $('#sellPrice').attr('readonly',true);
                $('#price-impact').attr('readonly',false);
            }else if(type == 'c'){
                $('#tax-p').attr('readonly',true);
                $('#tax-p').val(0);
                updateSellPrice($('#tax-c').val());
                $('#sellPrice').attr('readonly',true);
                $('#price-impact').attr('readonly',false);
            }else{
                $('#price-impact').attr('readonly',true);
                $('#content-product #tax-p').attr('readonly',true);
                var sellPriceElement = $('#sellPrice');
                sellPriceElement.val(Number($('#price-sell-price').val()));
                sellPriceElement.attr('readonly',false);
            }
        }
    });
    
    {if ($product.id_product >0)}
        getProductData({$product.id_product},{$product.id_attribute},"{$product.variation_priority}",{$product.variation_price},{$product.price_impact})
    {else}
        //simula um evento para fazer uma busca no autocomplete pelo EAN do produto
        var e = $.Event("keydown", { keyCode: 38}); //"keydown" if that's what you're doing
        $("#productFilter").trigger(e);
    {/if}
</script>