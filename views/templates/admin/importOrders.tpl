<div id="fieldset_import_orders" class="panel panel col-lg-6 col-lg-offset-3">
    <div class="panel-heading">
        <i class="icon-cloud-download"></i>
        Importação de pedidos.
    </div>
    <div class="panel-body">
        <div class="alert alert-warning col-lg-10 col-lg-offset-1 ">
            <p>Aguarde enquanto buscamos os seus pedidos.</p>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<script type="text/javascript">
    var importOrdersUrl = "{$importOrdersUrl}";
    
    function importOrders(){
        console.log(importOrdersUrl);
        $.ajax({
                type: "get",
                url: importOrdersUrl,
                dataType: "json",
                success: function(response) {
                    if(!response.error){
                        var html = '<h2>Total de pedidos: '+response.totalResults+'</h2><ul>';
                        var count = 0;
                        console.log(response);
                        var page = (response.nextPage);
                        console.log(page);
                        for (i = 1; i <= response.totalPages; i++) {
                            count = Number(i*50);
                            if(count < response.totalResults){
                                html += '<li class="page-'+i+'">'+ String(count-50)+' | '+String(count-1)+'</li>';
                            }else{
                                html += '<li class="page-'+i+'">'+ String(count-50)+' | '+String(response.totalResults)+'</li>';
                            }
                        }
                        html += '</ul>';
                        $('#fieldset_import_orders .panel-body').append(html);
                        
                        $('#fieldset_import_orders .page-'+page).css( "text-decoration", "line-through" );
                        $('#fieldset_import_orders .page-'+page).css( "color", "#aaaaaa" );
                        
                        importOrdersNext(response.nextPage);
                    }else{
                        $('#fieldset_import_orders .panel-body').append('<div class="alert alert-danger col-lg-10 col-lg-offset-1 ">'+response.error+'</div>');
                    }
                }
            });
    }
    importOrders();
    function importOrdersNext(page){
        if(page){
            console.log(importOrdersUrl+'?page='+page);
            $.ajax({
                type: "get",
                url: importOrdersUrl+'?page='+page,
                dataType: "json",
                success: function(response) {
                    if(!response.error){
                        $('#fieldset_import_orders .page-'+page).css( "text-decoration", "line-through" );
                        $('#fieldset_import_orders .page-'+page).css( "color", "#aaaaaa" );
                        importOrdersNext(response.nextPage);
                    }else{
                        $('#fieldset_import_orders .panel-body').append('<div class="alert alert-danger col-lg-10 col-lg-offset-1 ">'+response.error+'</div>');
                    }
                }
            });
        }else{
            $('#fieldset_import_orders .panel-body').append('<div class="alert alert-success col-lg-10 col-lg-offset-1">Pedidos importados com sucesso.</div>');
            setTimeout(function() {
                window.location.href = "index.php?controller={$smarty.get.controller}&token={$smarty.get.token}&configure=netshoesgroup&module_name=netshoesgroup";
            }, 2000);
        }
    }
</script>