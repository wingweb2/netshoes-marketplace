<div class="panel">
    {*<pre>{$product|var_dump}</pre>*}
    {if isset($product.addVariation)}
    <form method="post" class="form-horizontal" name="submitAddSku" action="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}&id_product={$product.id}&action=addVariation&product_id={$product.product_id}&productId={Tools::getValue('product_id')}">
        <input type="hidden" name="business_id" value="{Tools::getValue('business_id')}">
    {else}
    <form method="post" class="form-horizontal" name="submitExport" action="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}&id_product={$product.id}&action=exportProduct">
        <input type="hidden" name="business_id" value="{$product.business_id}">
    {/if}

        <h3>Exportar Produto - {$product.name}{if isset($product.addVariation)} - Adicionar Variações{/if}</h3>
        <div class="col-lg-12 panel">
            <div class="panel-heading"> Definições do produto </div>
            <div class="row">
                <div class="form-group col-lg-4">
                    <label class="control-label col-lg-4 required" for="brand"> <span>Marca </span> </label>
                    <div class="col-lg-8">
                         <select required="" id="brand" name="brand" >
                            {foreach from=$product.brands key=key item=brand}
                                <option value="{$brand.name}" {($brand.name == $product.brand)?'selected':''} > {$brand.name} </option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group col-lg-4">
                    <label class="control-label col-lg-4 required" for="department"> <span>Departamento </span> </label>
                    <div class="col-lg-8">
                         <select required="" id="department" name="department" onchange="getProductTypes(this);">
                            {foreach from=$product.departments key=key item=department}
                                <option value="{$department.name}" id="{$department.code}" >{$department.name} </option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group col-lg-4">
                    <label class="control-label col-lg-4 required" for="product_type"> <span>Tipo de Produto: </span> </label>
                    <div class="col-lg-8">
                         <select required="" id="product_type" name="product_type" >
                        </select>
                    </div>
                </div>
            </div>
        </div>

        {assign var='i' value=count($product.attributes)}
        {foreach from=$product.attributes key=key item=attribute}
            <input type="hidden" name="id_product_attribute[{$attribute.id}]" value="{$attribute.id}" />
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group col-md-2">
                        <img width="150" src="{$attribute.image}">
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="name-{$product.id}-{$attribute.id}" class="control-label col-lg-4 required"> <span>Nome</span> </label>
                        <div class="col-lg-8">
                            <input type="text" maxlength="100" required="" value="{$product.name}" id="name-{$product.id}-{$attribute.id}" name="name[{$attribute.id}]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label for="ean-{$product.id}-{$attribute.id}" class="control-label col-lg-4 required"> <span>EAN</span> </label>
                        <div class="col-lg-8">
                            <input type="text" maxlength="13" required="" value="{$attribute.ean}" id="ean-{$product.id}-{$attribute.id}" name="ean[{$attribute.id}]" class="form-control">
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-4 required" for="sku-{$product.id}-{$attribute.id}"> <span>SKU</span> </label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="sku[{$attribute.id}]" id="sku-{$product.id}-{$attribute.id}" value="{$product.reference}-{$attribute.id}" onkeyup="validateID(this)" required maxlength="18"/>
                        </div>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="control-label col-lg-4 required" for="stockQuantity-{$product.id}-{$attribute.id}"> <span>Estoque</span> </label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="stockQuantity[{$attribute.id}]" id="stockQuantity-{$product.id}-{$attribute.id}" value="{$attribute.stockQuantity}" required maxlength="13"/>
                        </div>
                    </div>
                     <div class="form-group col-lg-6">
                        <label class="control-label col-lg-4 required" for="color-{$attribute.id}"> <span>Cor: </span> </label>
                        <div class="col-lg-8">
                             <select required="" id="color-{$attribute.id}" name="color[{$attribute.id}]" onchange="flavorColorChange(this, 'flavor', {$attribute.id})">
                                <option value="" ></option>
				
                                {foreach from=$attribute.colors key=key item=color}
					
                                    <option value="{$color}" >{$color} </option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                     <div class="form-group col-lg-6">
                        <label class="control-label col-lg-4 required" for="flavor-{$attribute.id}"> <span>Sabor: </span> </label>
                        <div class="col-lg-8">
                             <select required="" id="flavor-{$attribute.id}" name="flavor[{$attribute.id}]" onchange="flavorColorChange(this, 'color', {$attribute.id})">
                               <option value="" ></option>
                                {foreach from=$attribute.flavors key=key item=flavor}
                                    <option value="{$flavor}" >{$flavor} </option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                     <div class="form-group col-lg-6">
                        <label class="control-label col-lg-4 required" for="size-{$attribute.id}"> <span>Tamanho: </span> </label>
                        <div class="col-lg-8">
                             <select required="" id="size-{$attribute.id}" name="size[{$attribute.id}]" >
                               <option value="" ></option>
                                {foreach from=$attribute.sizes key=key item=size}
                                    <option value="{$size}" >{$size} </option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                     <div class="form-group col-lg-6">
                        <label class="control-label col-lg-4 required" for="gender-{$attribute.id}"> <span>Gênero: </span> </label>
                        <div class="col-lg-8">
                             <select required="" id="gender-{$attribute.id}" name="gender[{$attribute.id}]" >
                               <option value="" ></option>
                                {foreach from=$attribute.genders key=key item=gender}
                                    <option value="{$gender.name}" >{$gender.name} </option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 panel">
                    <div class="panel-heading"> Definição de preço </div>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-6" for="tax-type-c-{$product.id}-{$attribute.id}">Categoria</label>
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input type="radio" class="tax-type" name="tax-type[{$attribute.id}]" value="c" checked="checked" id="tax-type-c-{$product.id}-{$attribute.id}" data-id="{$product.id}-{$attribute.id}">
                                    </span>
                                    <input type="text" class="form-control" id="tax-c-{$product.id}-{$attribute.id}" value="{$product.categoryTax}" disabled="">
                                    <span class="input-group-addon">
                                        %
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-6">
                            <label class="control-label col-lg-6" for="tax-type-p-{$product.id}-{$attribute.id}">Produto</label>
                            <div class="col-lg-6">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input type="radio" class="tax-type" name="tax-type[{$attribute.id}]" value="p" id="tax-type-p-{$product.id}-{$attribute.id}" data-id="{$product.id}-{$attribute.id}">
                                    </span>
                                    <input type="text" class="form-control"  onkeyup="updateSellPrice(this.value,'{$product.id}-{$attribute.id}');" id="tax-p-{$product.id}-{$attribute.id}" name="tax[{$attribute.id}]" value="0" readonly="">
                                    <span class="input-group-addon">
                                        %
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-6">
                            <label for="listPrice-{$product.id}-{$attribute.id}" class="control-label col-lg-6 required"> <span>Preço De</span> </label>
                            <div class="col-lg-6 input-group">
                                <input type="text" maxlength="13" required="" value="{$product.listPrice}" id="listPrice-{$product.id}-{$attribute.id}" name="listPrice[{$attribute.id}]" class="form-control">
                                <span class="input-group-addon">R$</span>
                            </div>
                        </div>
                        <div class="form-group col-lg-6">
                            <label for="" class="control-label col-lg-6 required"> <span>Preço Por</span> </label>
                            <div class="col-lg-6 input-group">
                                <input type="text"  maxlength="13" id="sellPrice-{$product.id}-{$attribute.id}" name="sellPrice[{$attribute.id}]" class="form-control" data-sellprice="{$attribute.sellPrice}" data-value="{number_format($attribute.sellPrice + ($attribute.sellPrice * $product.categoryTax /100),2,'.','')}" value="{number_format($attribute.sellPrice + ($attribute.sellPrice * $product.categoryTax /100),2,'.','')}">
                                <span class="input-group-addon">R$</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {if ($key != ($i - 1))}<hr>{/if}
        {/foreach}
        <div class="panel-footer">
            {if isset($product.addVariation)}
                <button class="btn btn-default pull-right" name="submitAddSku" id="form_submit_btn" value="1" type="submit">
                    <i class="process-icon-save"></i> Salvar
                </button>
            {else}
            <button class="btn btn-default pull-right" name="submitExport" id="form_submit_btn" value="1" type="submit">
                <i class="process-icon-save"></i> Salvar
            </button>
            {/if}
            <a onclick="window.history.back()" class="btn btn-default" href="index.php?controller={$smarty.get.controller}&token={$smarty.get.token}"> <i class="process-icon-cancel"></i> Cancelar </a>
        </div>
    </form>
</div>
<script type="text/javascript">
    function validateID(element) {
        var valor = $(element).val().replace(/[^a-zA-Z0-9-_.]+/g,'');
        $(element).val(valor);
    }
    function updateSellPrice(tax,id){
        tax = parseFloat(tax);
        if(!isNaN(tax)) {
            var sellPriceElement = $('#sellPrice-'+id);
            var sellPrice = Number(sellPriceElement.data('sellprice'));
            newSellPrice = sellPrice + (sellPrice * tax / 100);
            sellPriceElement.val(newSellPrice.toFixed(2));
            console.log($('#sellPrice-'+id).data('sellprice'));
        }
    }
    
    $(document).ready(function () {
        $('.tax-type').click(function () {
            var element = $(this);
            var type = element.val();
            if (element.is(':checked')) {
                var id = element.data('id');
                if(type == 'p'){
                    $('#tax-p-'+id).attr('readonly',false);
                    updateSellPrice($('#tax-p-'+id).val(),id);
                }else{
                    $('#tax-p-'+id).attr('readonly',true);
                    $('#tax-p-'+id).val(0);
                    updateSellPrice($('#tax-c-'+id).val(),id);
                }
            }
        });
        var options = $('select#brand option');
        var arr = options.map(function(_, o) {
            return {
                t: $(o).text(),
                v: o.value
            };
        }).get();
        arr.sort(function(o1, o2)
        {
            return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
        });
        options.each(function(i, o) {
            o.value = arr[i].v;
            $(o).text(arr[i].t);
        });
        var options = $('select#department option');
        var arr = options.map(function(_, o) {
            return {
                t: $(o).text(),
                v: o.value
            };
        }).get();
        arr.sort(function(o1, o2)
        {
            return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
        });
        options.each(function(i, o) {
            o.value = arr[i].v;
            $(o).text(arr[i].t);
        });

    });
    function flavorColorChange(elem, type, attribute_id){
       if(elem.value != '') {
          document.getElementById(type+'-'+attribute_id).disabled = true;
       } else {
          document.getElementById(type+'-'+attribute_id).disabled = false;
       }
    }
    function getProductTypes(elem){
            var getProductTypeUrl = "{$product.getProductTypeUrl}";
	    $('#product_type').empty();
            var code = elem[elem.selectedIndex].id;
            $.ajax({
                    type: "get",
                    data: { department_code: code},
                    url: getProductTypeUrl,
                    dataType: "json",
                    success: function(response) {
                         for(var key in response) {
                           $('#product_type').append('<option value="'+response[key]['name']+'"> '+response[key]['name']+'</option>');
                        }
                        var options = $('select#product_type option');
                        var arr = options.map(function(_, o) {
                            return {
                                t: $(o).text(),
                                v: o.value
                            };
                        }).get();
                        arr.sort(function(o1, o2)
                        {
                            return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
                        });
                        options.each(function(i, o) {
                            o.value = arr[i].v;
                            $(o).text(arr[i].t);
                        });
                    }
                });

        }
 
</script>
</script>
