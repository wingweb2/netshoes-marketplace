<div class="panel kpi-container hidden-print">
    <div class="row">
        <div class="col-xs-6 col-sm-3  box-stats color3" >
            <div class="kpi-content">
                <i class="icon-calendar-empty"></i>
                <span class="title">{l s='Data'}</span>
                <span class="value">{$order->order_date}</span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 box-stats color4" >
            <div class="kpi-content">
                <i class="icon-money"></i>
                <span class="title">{l s='Total'}</span>
                <span class="value">{displayPrice price=$order->total_gross }</span>
            </div>
        </div>
        <div class="col-xs-6 col-sm-3 box-stats color1" >
            <a href="#start_products">
                <div class="kpi-content">
                    <i class="icon-book"></i>
                    <span class="title">{l s='Produtos'}</span>
                    <span class="value">{sizeof($order->products)}</span>
                </div>
            </a>
        </div>
        <div class="col-xs-6 col-sm-3 box-stats color2" >
            <a href="{$link->getAdminLink('NetshoesGroupOrders')|escape:'html':'UTF-8'}&amp;id_order={$order->id_order}&amp;synchronize=1">
                <div class="kpi-content">
                    <i class="icon-refresh"></i>
                    <span class="title">{l s='Pedido'} - {$order->id_netshoes}</span>
                    <span class="value">{l s='Sincronizar'}</span>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel" style="float: left; width: 100%">
            <div class="panel-heading">
                <i class="icon-credit-card"></i>
                {l s='Pedido'}
                <span class="badge">{$order->id_netshoes}</span>
            </div>
            <div class="col-md-6 text-center hidden-print" style="">
                <span class="badge" style="font-size: 35px; margin-bottom: 35px">{l s="#"}{$order->id_netshoes}</span>
                <!-- Tab nav -->
                <ul class="nav nav-tabs" id="tabOrder">
                    <li class="active">
                        <a href="#status">
                            <i class="icon-time"></i>
                            {l s='Status'}
                        </a>
                    </li>
                </ul>
                <!-- Tab content -->
                <div class="tab-content panel text-left">
                    <!-- Tab status -->
                    <div class="tab-pane active" id="status">
                        <h4 class="visible-print">{l s='Status'}</h4>
                        <!-- History of status -->
                        <div class="table-responsive">
                            <p style="margin-bottom: 20px; text-align: center">
                                <span class="label" style="font-size: 20px; background-color: {$order->statusLabelColor};color:{if Tools::getBrightness($order->statusLabelColor) < 128}white{else}#383838{/if}">{$order->statusLabel|stripslashes}</span>
                            </p>
                            <div class="text-center">
                                <div class="btn-group">
                                    {assign var="paymentClass" value=null}
                                    {assign var="invoiceClass" value=null}
                                    {assign var="shippingClass" value=null}
                                    {assign var="homeClass" value=null}
                                    {assign var="paymentText" value=null}
                                    {assign var="invoiceText" value=null}
                                    {assign var="shippingText" value=null}
                                    {assign var="homeText" value=null}
                                    {if $order->status == 'Canceled'}
                                        {$paymentClass = 'btn-danger'}
                                        {$paymentText = 'Pedido Cancelado'}

                                {*{elseif $order->status == 'NOT_APPROVED'}
                                        {$paymentClass = 'btn-danger'}
                                        {$paymentText = 'Pagamento Vencido'} *}
                                    {elseif $order->status == 'Created'}
                                        {$paymentClass = 'btn-warning'}
                                        {$paymentText = 'Aguardando Pagamento'}
                              {*{elseif $order->status == 'UNAVAILABLE'}
                                        {$paymentClass = 'btn-danger'}
                                        {$paymentText = 'Indis. de Estoque'}
                                        {$paymentClass = 'btn-danger'}
                                        {$paymentText = 'danger'} *}
                                   {elseif $order->status == 'Approved'}
                                        {$paymentClass = 'btn-success'}
                                        {$paymentText = 'Pagamento Aprovado'}
                           {*   {elseif $order->status == 'PROCESSING'}
                                        {$paymentClass = 'btn-success'}
                                        {$paymentText = 'Pagamento Aprovado'}
                                        {$invoiceClass = 'btn-warning'}
                                        {$invoiceText = 'Aguardando nota fiscal'} *}
                            	   {elseif $order->status == 'Invoiced'}
                                        {$paymentClass = 'btn-success'}
                                        {$paymentText = 'Pagamento Aprovado'}
                                        {$invoiceClass = 'btn-success'}
                                        {$invoiceText = 'Nota Fiscal Emitida'}
                                        {$shippingClass = 'btn-warning'}
                                        {$shippingText = 'Aguardando Envio'}

                              {*{elseif $order->status == 'SHIPMENT_EXCEPTION'}
                                        {$paymentClass = 'btn-success'}
                                        {$paymentText = 'Pagamento Aprovado'}
                                        {$invoiceClass = 'btn-success'}
                                        {$invoiceText = 'Nota Fiscal Emitida'}
                                        {$shippingClass = 'btn-info'}
                                        {$shippingText = 'Exceção de Transporte'}*}
                                   {elseif $order->status == 'Shipped'}
                                        {$paymentClass = 'btn-success'}
                                        {$paymentText = 'Pagamento Aprovado'}
                                        {$invoiceClass = 'btn-success'}
                                        {$invoiceText = 'Nota Fiscal Emitida'}
                                        {$shippingClass = 'btn-success'}
                                        {$shippingText = 'Pedido Enviado'}
                                        {$homeClass = 'btn-warning'}
                                        {$homeText = 'Aguardando entrega'}
                                    {elseif $order->status == 'Delivered'}
                                        {$paymentClass = 'btn-success'}
                                        {$paymentText = 'Pagamento Aprovado'}
                                        {$invoiceClass = 'btn-success'}
                                        {$invoiceText = 'Nota Fiscal Emitida'}
                                        {$shippingClass = 'btn-success'}
                                        {$shippingText = 'Pedido Enviado'}
                                        {$homeClass = 'btn-success'}
                                        {$homeText = 'Entrega Realizada'}
                                    {/if}
                                    <button class="btn help-tooltip {$paymentClass}" type="button" data-toggle="tooltip" data-placement="top" title="{$paymentText}"><i class="icon-credit-card" style="font-size: 40px"></i></button>
                                    <button class="btn help-tooltip {$invoiceClass}" type="button" data-toggle="tooltip" data-placement="top" title="{$invoiceText}"><i class="icon-file-text"  style="font-size: 40px"></i></button>
                                    <button class="btn help-tooltip {$shippingClass}" type="button" data-toggle="tooltip" data-placement="top" title="{$shippingText}"><i class="icon-truck"  style="font-size: 40px"></i></button>
                                    <button class="btn help-tooltip {$homeClass}" type="button" data-toggle="tooltip" data-placement="top" title="{$homeText}"><i class="icon-home"  style="font-size: 40px" ></i></button>
                                </div>
                            </div>
                            {if $order->status == 'Approved'}
                                  <form style="margin: 20px;" class="defaultForm form-horizontal panel" action="{$link->getAdminLink('NetshoesGroupOrders')|escape:'html':'UTF-8'}&amp;viewnetshoesgroup_order&amp;id_order={$order->id}" method="post">
                                    <h3><i class="icon-file-text"  ></i> Nota Fiscal</h3>
                                    <div class="form-wrapper">
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 required"> Número: </label>
                                            <div class="col-lg-9 ">
                                                <input id="number" class="" type="text" name="number" value="" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 required"> Número de série: </label>
                                            <div class="col-lg-9 ">
                                                <input id="line" class="" type="text" name="line" value="" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 required"> Data Nota Fiscal: </label>
                                            <div class="col-lg-9 ">
                                                <input id="issueDate" class="datepicker" type="text" name="issueDate" value="" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 required"> Chave : </label>
                                            <div class="col-lg-9 ">
                                                <input id="key" class="" type="text" name="key" value="" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3"> Documento XML da DANFE: </label>
                                            <div class="col-lg-9 ">
                                                <textarea id="danfeXml" name="danfeXml" value="" > </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group panel-footer">
                                        <button id="module_form_submit_btn" class="btn btn-default pull-right" name="sendInvoice" value="1" type="submit">
                                            <i class="process-icon-save"> </i>
                                            Salvar
                                        </button>
                                    </div>
                                </form>
                            {elseif $order->status == 'Invoiced'}
                                <form style="margin: 20px;" class="defaultForm form-horizontal panel" action="{$link->getAdminLink('NetshoesGroupOrders')|escape:'html':'UTF-8'}&amp;viewnetshoesgroup_order&amp;id_order={$order->id}" method="post">
                                    <h3><i class="icon-truck"></i> Transporte</h3>
                                    <div class="form-wrapper">
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 required"> Cod. Rastreamento: </label>
                                            <div class="col-lg-9 ">
                                                <input id="trackingProtocol" class="" type="text" name="trackingProtocol" value="" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 required"> Data estimada da entrega: </label>
                                            <div class="col-lg-9 ">
                                                <input id="estimatedDelivery" class="datepicker" type="text" name="estimatedDelivery" value="" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 required">  Data que o produto foi entregue a transportadora: </label>
                                            <div class="col-lg-9 ">
                                                <input id="deliveredCarrierDate" class="datepicker" type="text" name="deliveredCarrierDate" value="" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 required"> Nome da transportadora: </label>
                                            <div class="col-lg-9 ">
                                                <input id="carrierName " class="" type="text" name="carrierName " value="" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-3"> Url rastreamento do pedido: </label>
                                            <div class="col-lg-9 ">
                                                <input id="trackingUrl " class="" type="text" name="trackingUrl " value="" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group panel-footer">
                                        <button id="module_form_submit_btn" class="btn btn-default pull-right" name="sendShipped" value="1" type="submit">
                                            <i class="process-icon-save"></i>
                                            Salvar
                                        </button>
                                    </div>
                                </form>
                            {elseif $order->status == 'Shipped'}
                                <form style="margin: 20px;" class="defaultForm form-horizontal panel" action="{$link->getAdminLink('NetshoesGroupOrders')|escape:'html':'UTF-8'}&amp;viewnetshoesgroup_order&amp;id_order={$order->id}" method="post">
                                    <h3><i class="icon-truck"></i> Transporte</h3>
                                    <div class="form-wrapper">
                                       <!-- <div class="form-group">
                                            <label class="control-label col-lg-3 required"> Exceções Ocorridas: </label>
                                            <div class="col-lg-9 ">
                                                <textarea id="observation" name="observation" value="" required="required"></textarea>
                                            </div>
                                        </div>-->
                                        <div class="form-group">
                                            <label class="control-label col-lg-3 required"> Data da Ocorrência: </label>
                                            <div class="col-lg-9 ">
                                                <input id="occurrenceDate" class="datepicker" type="text" name="occurrenceDate" value="{date('Y-m-d H:i:s')}" required="required">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group panel-footer">
                                       <!-- <button id="module_form_submit_btn" class="btn btn-default pull-right" name="sendShipmentException" value="1" type="submit">
                                            <i class="process-icon-save"></i>
                                            Salvar
                                        </button>-->
                                        <a class="btn btn-default pull-right" href="{$link->getAdminLink('NetshoesGroupOrders')|escape:'html':'UTF-8'}&amp;viewnetshoesgroup_order&amp;id_order={$order->id}&amp;Delivered=1">
                                             <i class="process-icon-truck icon-truck"></i> Entregue
                                        </a>
                                    </div>
                                </form>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <!-- Orders Actions -->
                <div class="well hidden-print">
                    <a class="btn btn-default" href="javascript:window.print()">
                        <i class="icon-print"></i>
                        {l s='Imprimir pedido de compra'}
                    </a>
                    &nbsp;
                </div>
                <!-- Tab nav -->
                <ul class="nav nav-tabs" id="myTab">
                    <li class="active">
                        <a href="#shipping">
                            <i class="icon-truck "></i>
                            {l s='Shipping'}
                        </a>
                    </li>
                </ul>
                <!-- Tab content -->
                <div class="tab-content panel">
                    <!-- Tab shipping -->
                    <div class="tab-pane active" id="shipping">
                        <h4 class="visible-print">{l s='Frete'}</h4>
                        <div class="form-horizontal">
                            <div class="table-responsive">
                                <table class="table" id="shipping_table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <span class="title_box ">{l s='Transportadora'}</span>
                                            </th>
                                            <th class="text-right">
                                                <span class="title_box ">{l s='Custo de envio'}</span>
                                            </th>
                                            <th class="text-right">
                                                <span class="title_box ">{l s='Data estimada entrega'}</span>
                                            </th>
                                            <th class="text-right">
                                                <span class="title_box ">{l s='Número de rastreamento'}</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        {if $order->shipping->transport != null}
                                            <td>{if isset($order->shipping->transport->delivery_service) }{$order->shipping->transport->delivery_service}{/if}&nbsp;</td>
                                        {else}
                                            <td>Transportadora não foi encontrada &nbsp;</td>
                                        {/if}
                                        <td class="text-right">
                                            {displayPrice price=$order->total_freight}
                                        </td>
                                        <td class="text-right">
                                            {if isset($order->shipping->shipping_estimate)}{dateFormat date=$order->shipping->shipping_estimate full=true}{/if}
                                        </td>
                                       <td class="actions text-right">
                                            <span id="shipping_number_show">&nbsp;
						{if isset($order->shipping_info->trackingProtoco)}
							{$order->shipping_info->trackingProtocol}
						{elseif $order->shipping->transport->tracking_number != null}
							{$order->shipping->transport->tracking_number}
						{else}
							Rastreamento não foi encontrado &nbsp;
						{/if}
					    </span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <!-- Payments block -->
            <div id="formAddPaymentPanel" class="panel">
                <div class="panel-heading">
                    <i class="icon-money"></i>
                    {l s="Pagamento"}
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th><span class="title_box ">{l s='ID Transação'}</span></th>
                                <th><span class="title_box ">{l s='Valor'}</span></th>
                            </tr>
                        </thead>
                        <tbody>
                           <!--  {if (isset($order->payment_methods) && count($order->payment_methods) > 0 )}
                                {foreach from=$order->payment_methods item=payment}
                                <tr>
                                    <td>
                                        {if $payment->id == 'DEBIT_CARD'} 
                                            Cartão de Débito
                                        {elseif $payment->id == 'CREDIT_CARD'} 
                                            Cartão de Crédito
                                        {elseif $payment->id == 'BOLETO'} 
                                            Boleto
                                        {elseif $payment->id == 'DEBIT_ACCOUNT'} 
                                            Débito em conta
                                        {elseif $payment->id == 'VOUCHER'} 
                                            Voucher/Vale
                                         {else} 
                                            {$payment->id|escape:'html':'UTF-8'}
                                        {/if}
                                    </td>
                                    <td>{displayPrice price=$payment->value}</td>
                                </tr>
                                {/foreach}
                            {/if} -->
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- Customer informations -->
            <div class="panel">
                <div class="panel-heading">
                    <i class="icon-user"></i>
                    {l s='Cliente'}
                    <span class="badge">
                            {$order->customer->customer_name}
                    </span>
                </div>
                <div class="col-xs-6">
                    <dl class="well list-detail">
                        <dt>{l s='Nome'}</dt>
                            <dd><i class="icon-user"></i> {$order->customer->customer_name}</dd>
                        <dt>Documento</dt>
                            <dd><i class="icon-paperclip"></i> {$order->customer->document}</dd>
                        <dt>{l s='Telefone'}</dt>
                            <dd>
                                <i class="icon-phone"></i> {$order->customer->cell_phone}
                            </dd>
                    </dl>
                </div>
                <!-- Tab nav -->
                <div class="col-xs-6">
                    <ul class="nav nav-tabs" id="tabAddresses">
                        <li class="active">
                            <a href="#addressShipping">
                                <i class="icon-truck"></i>
                                {l s='Endereço do envio'}
                            </a>
                        </li>
                        <li>
                            <a href="#addressInvoice">
                                <i class="icon-file-text"></i>
                                {l s='Endereço de cobrança'}
                            </a>
                        </li>
                    </ul>
                    <!-- Tab content -->
                    <div class="tab-content panel">
                        <!-- Tab status -->
                        <div class="tab-pane  in active" id="addressShipping">
                            <!-- Addresses -->
                            <h4 class="visible-print">{l s='Endereço do envio'}</h4>
                            <div class="well">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <strong>Nome: </strong>{$order->customer->customer_name}<br>
                                        {if $order->customer->address != null}
                                            <strong>Rua: </strong>{$order->customer->address->street}<br>
                                            <strong>Número: </strong>{$order->customer->address->number}<br>
                                            {if isset($order->customer->address->neighborhood)}<strong>Bairro: </strong>{$order->customer->address->neighborhood}<br>{/if}
                                            {if isset($order->customer->address->reference) && $order->customer->address->reference != ''}
                                            <strong>Referência: </strong>{$order->customer->address->reference}<br>{/if}

                                             {if isset($order->customer->address->complement) && $order->customer->address->complement != ''}
                                             <strong>Complemento: </strong>{$order->customer->address->complement}<br>{/if}
                                            <strong>CEP: </strong>{$order->customer->address->postal_code}<br>
                                            <strong>Cidade: </strong>{$order->customer->address->city}<br>
                                            <strong>Estado: </strong>{$order->customer->address->state}<br>
                                        {elseif $order->customer->address != null}
                                            <strong>Rua: </strong>{$order->customer->address->street}<br>
                                            <strong>Número: </strong>{$order->customer->address->number}<br>
                                            {if isset($order->customer->address->neighborhood)}<strong>Bairro: </strong>{$order->customer->address->neighborhood}<br>{/if}
                                            <strong>CEP: </strong>{$order->customer->address->postal_code}<br>
                                            <strong>Cidade: </strong>{$order->customer->address->city}<br>
                                            <strong>Estado: </strong>{$order->customer->address->state}<br>

                                        {else}
                                            <br><br><h2><strong>Não foram encontrados endereços para esse pedido</strong></h2></br>
                                        {/if}
                                    </div>
                                    <div class="col-sm-6 hidden-print">
                                        <div id="map-delivery-canvas" style="height: 190px"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <script>
                    $('#tabAddresses a').click(function (e) {
                        e.preventDefault()
                        $(this).tab('show')
                    })
                </script>
            </div>
        </div>
    </div>
</div>
</div>
<div id="start_products">
    <div class="col-lg-12">
        <div class="panel">
            <div class="panel-heading">
                <i class="icon-shopping-cart"></i>
                {l s='Produtos'} <span class="badge">{$order->products|@count}</span>
            </div>
            <div class="table-responsive">
                <table class="table" id="orderProducts">
                    <thead>
                        <tr>
			    <th><span class="title_box ">{l s='Produto'}</span></th>
                            <th><span class="title_box ">{l s='Preço Un.'}</span></th>
                            <th class="text-center"><span class="title_box ">{l s='Qtd.'}</span></th>
                            <th><span class="title_box ">{l s='Total'}</span></th>
                        </tr>
                    </thead>
                    <tbody>
                    {assign var=totalProducts value=0}
                    {foreach from=$order->products item=sku key=k}
                        {$totalProducts = $totalProducts+($sku->gross_unit_value * $sku->quantity)}
                        <tr class="product-line-row">

                            <td>
				{if $sku->img}<img style="width: 80px;" src="{$sku->img->url}"> {/if} {$sku->name}
			    </td>
			    <td>
                                <span class="product_price_show">{displayPrice price=$sku->gross_unit_value}</span>
                            </td>
                            <td class="productQuantity text-center">
                                <span class="product_quantity_show{if (int)$sku->quantity > 1} badge{/if}">{$sku->quantity}</span>
                            </td>
                            <td class="total_product">
                                {displayPrice price=(Tools::ps_round($sku->gross_unit_value, 2) * $sku->quantity)}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
            <div class="row  row-margin-top">
                <div class="col-xs-6">
                </div>
                <div class="col-xs-6">
                    <div class="panel panel-total">
                        <div class="table-responsive">
                            <table class="table">
                                <tr id="total_products">
                                    <td class="text-right">{l s='Produtos:'}</td>
                                    <td class="amount text-right">
                                        {displayPrice price=($totalProducts) }
                                    </td>
                                </tr>
                                <tr id="total_shipping">
                                    <td class="text-right">{l s='Frete'}</td>
                                    <td class="amount text-right" >
                                        {displayPrice price=$order->total_freight}
                                    </td>
                                </tr>
                                <tr id="total_discount">
                                    <td class="text-right">{l s='Desconto'}</td>
                                    <td class="amount text-right" >
                                        {displayPrice price=$order->total_discount}
                                    </td>
                                </tr>
                                <!--<tr id="total_interest">
                                    <td class="text-right">{l s='Juros'}</td>
                                    <td class="amount text-right" >
                                        {displayPrice price=$order->total_interest}
                                    </td>
                                </tr>-->
                                <tr id="total_order">
                                    <td class="text-right"><strong>{l s='Total Bruto'}</strong></td>
                                    <td class="amount text-right">
                                        <strong>{displayPrice price=$order->total_gross}</strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script type="text/javascript">
    var geocoder = new google.maps.Geocoder();
    var delivery_map;

    $(document).ready(function()
    {
        $(".textarea-autosize").autosize();
        geocoder.geocode({
            address: '{$order->customer->address->street},{$order->customer->address->postal_code},{$order->customer->address->city},{$order->customer->address->state}'
            }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK)
            {
                delivery_map = new google.maps.Map(document.getElementById('map-delivery-canvas'), {
                    zoom: 10,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    center: results[0].geometry.location
                });
                var delivery_marker = new google.maps.Marker({
                    map: delivery_map,
                    position: results[0].geometry.location,
                    url: 'http://maps.google.com?q={$order->customer->address->street|urlencode}-{$order->customer->address->number|urlencode},{$order->customer->address->postal_code|urlencode},{$order->customer->address->city|urlencode},{$order->customer->address->state|urlencode}'
                });
                google.maps.event.addListener(delivery_marker, 'click', function() {
                    window.open(delivery_marker.url);
                });
            }
        });


    });

    // Fix wrong maps center when map is hidden
    $('#tabAddresses').click(function(){
        x = delivery_map.getZoom();
        c = delivery_map.getCenter();
        google.maps.event.trigger(delivery_map, 'resize');
        delivery_map.setZoom(x);
        delivery_map.setCenter(c);

        x = invoice_map.getZoom();
        c = invoice_map.getCenter();
        google.maps.event.trigger(invoice_map, 'resize');
        invoice_map.setZoom(x);
        invoice_map.setCenter(c);
    });
    $(document).ready(function() {
        if ($.timepicker) {
            $.timepicker.regional['']['timeText'] = "Tempo";
            $.timepicker.regional['']['hourText'] = "Hora";
            $.timepicker.regional['']['minuteText'] = "Minuto";
            $.timepicker.regional['']['secondText'] = "Segundo";
            $.timepicker.regional['']['currentText'] = "Agora";
            $.timepicker.regional['']['closeText'] = "concluído";
            $.extend($.timepicker._defaults, $.timepicker.regional['']);
        } /* if ($.timepicker) */
  
        if ($(".datepicker").length > 0){
            
            $(".datepicker").datetimepicker({
                prevText: '',
                nextText: '',
                dateFormat: 'yy-mm-dd',
                timeFormat: "hh:mm:ss"
            });
        }
    });
</script>
