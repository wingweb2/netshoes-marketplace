<div class="panel">
    <h3 class="panel-title">
        Comissão sobre vendas
    </h3>
    <div class="panel-body">
        <div class="progress comission-bar">
            <div class="progress-bar progress-bar-warning" style="width: 100%">
                <span class="sr-only">{displayPrice price=$total_estimate }</span>
                <div class="comission-earned will-earn">
                    <small>Projeção de faturamento</small>{displayPrice price=$total_estimate }
                </div>
                <div class="comission-payed will-pay">
                    <small>Projeção de comissão (0.8%)</small>{displayPrice price=$comission_estimate }
                </div>
            </div>
            <div class="progress-bar progress-bar-success" style="width: {$current_percent}%">
                <span class="sr-only">{displayPrice price=$total_amount }</span>
                <div class="comission-earned already-earn">
                    <small>Faturado</small>{displayPrice price=$total_amount }
                </div>
                <div class="comission-payed already-pay">
                    {if $comission > $min_comission}
                        <small>Comissão ({$tax_comission}%)</small>{displayPrice price=$comission }
                    {else}
                        <small>Valor mínimo</small>{displayPrice price=$min_comission }
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.progress.comission-bar {
    position: relative;
    overflow: visible;
    -moz-border-radius: 50px;
    -webkit-border-radius: 50px;
    border-radius: 50px;
}
.progress.comission-bar .progress-bar {
    position: absolute;
    left: 0;
    top: 0;
    -moz-border-radius: 50px;
    -webkit-border-radius: 50px;
    border-radius: 50px;
}
.progress.comission-bar .progress-bar.progress-bar-success {
    opacity: 1;
    background: #89B828;
}
.progress.comission-bar .progress-bar.progress-bar-warning {
    background: rgba(180, 60, 247, 0.5);
    -moz-transition: all 0.15s ease;
    -o-transition: all 0.15s ease;
    -webkit-transition: all 0.15s ease;
    transition: all 0.15s ease;
}
.progress.comission-bar .progress-bar.progress-bar-warning:hover {
    background: #B43CF7;
}
.progress.comission-bar .progress-bar.progress-bar-warning .will-earn, .progress.comission-bar .progress-bar.progress-bar-warning .will-pay {
    color: #B43CF7;
}
.progress.comission-bar .progress-bar .comission-earned,
.progress.comission-bar .progress-bar .comission-payed {
    position: absolute;
    right: 10px;
    color: #89B828;
    font-weight: bold;
    font-size: 15px;
}
.progress.comission-bar .progress-bar .comission-earned small,
.progress.comission-bar .progress-bar .comission-payed small {
    color: #aaa;
    font-weight: normal;
    font-size: 10px;
    margin-right: 5px;
}
.progress.comission-bar .progress-bar .comission-earned {
    position: absolute;
    bottom: -25px;
}
.progress.comission-bar .progress-bar .comission-earned:before {
    position: absolute;
    top: -19px;
    right: 0;
    background: #ffffff;
    content: "";
    width: 10px;
    height: 10px;
    z-index: 1;
    -moz-border-radius: 100%;
    -webkit-border-radius: 100%;
    border-radius: 100%;
}
.progress.comission-bar .progress-bar .comission-payed {
    position: absolute;
    top: -25px;
}
</style>

